It's been nearly a year to the day since I last sent a version of this
newsletter. To say a lot has happened since last October would be like
saying that R.E.M. is my favorite band. Obvious to a fault.

This first issue will skew a bit shorter so we can all get into the
groove of things without being confronted by some wall of text.

Over the next few weeks, I'd like to fill y'all in on some of the things
that have happened in the intervening months since my last missive. The
biggest change has likely been our move to Owensboro, Kentucky. I'm
looking out over our little farm here with a baby pig walking under my
chair, dogs barking in the yard, and horses nickering out in the
pasture. It's an amazing privilege to have this refuge amidst the chaos.

<div>

------------------------------------------------------------------------

</div>

## Avenue Q is better than QAnon

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/d6eab214-884c-4902-9cb4-4282440bc2d9_1643x1090.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fd6eab214-884c-4902-9cb4-4282440bc2d9_1643x1090.jpeg){.image-link
.image2 .image2-966-1456}

I'll be honest and let you know that I haven't paid much attention to
this whole QAnon thing. I just assumed it would all pass. How much do we
talk about the Tea Party anymore? This seemed to have even less staying
power than that spurt of madness. Unfortunately, as this New York
article tells us, the followers of QAnon are now running for office and
having more success than one might suspect.

[Read about QAnon in
Washington](https://nymag.com/intelligencer/2020/09/qanon-republican-party-congress.html){.button
.primary}

## Wave on Wave on Wave

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/c7746e08-1880-4c11-a003-81d4224e39aa_600x337.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fc7746e08-1880-4c11-a003-81d4224e39aa_600x337.jpeg){.image-link
.image2 .image2-337-600}

It's probably a bit too early to get into the actual Halloween music.
(Halloween seems to have gotten almost as popular as Christmas?) But
surely, we can watch some scary movies and listen to some dark and
brooding music. I've been on a synthwave kick that's fueled as much by
nostalgia as actual musical interest. Once you start down the synthwave
path, then you're into darkwave, horrorwave, retrowave, outrun,
vaporwave, and lots of other little subgenres. Do some exploration but
the Astral Throb playlists on Youtube are a good start. They've just
published this great mix to coincide with the spookiest of seasons.

[Listen to
INVASION](https://www.youtube.com/watch?v=-SYDEsBIbec){.button .primary}

## A Complicated Revolution

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/e693ed5b-2e27-40a5-b59d-f99515110a98_617x347.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fe693ed5b-2e27-40a5-b59d-f99515110a98_617x347.jpeg){.image-link
.image2 .image2-347-617}

###### `NMdUnsl, CC BY-SA 4.0, via Wikimedia Commons`

My mother and stepfather lived in Chile for several years back in the
late 90s. I visited and it was a stunningly beautiful country. The
people I met were warm and friendly. In the last few years, there have
been rumblings of real issues across the country but specifically in
Santiago where the disparity between rich and poor is as drastic as our
own San Francisco. This article by Daniel Alarcón gives some insight
into the roots of the growing revolution and how the coronavirus has
affected the people's and the government's response.

[Read Chile at the
Barricades](https://www.newyorker.com/magazine/2020/10/12/chile-at-the-barricades){.button
.primary}

## I Would Pay It

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/a5fe1353-f722-4214-96f3-fa8144b1d776_800x496.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fa5fe1353-f722-4214-96f3-fa8144b1d776_800x496.jpeg){.image-link
.image2 .image2-496-800}

I've loved Shakespeare's work since I first read it in high school in
Mr. Saunder's class. I can appreciate any adaptation, any reference, any
allusion. Nothing is off-limits when it comes to interpreting the
stories, styles, and characters. It follows that if I had \$10 million
to through at some art, I'd be on the list to bid on this First Folio
that just sold at auction. It does beg the question if the new owner
will actually read it. This particular copy was sold by Mills College
which had owned it since 1977. I'd love to know where it had been before
that and why the college decided to sell now.

[Figure out if it\'s worth
it](https://cnn.com/style/article/shakespeare-first-folio-auction/index.html){.button
.primary}

## Plant a Tree, Save a Species

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/73f69986-c007-47d6-bdd6-8e0a4f8023cc_754x465.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F73f69986-c007-47d6-bdd6-8e0a4f8023cc_754x465.jpeg){.image-link
.image2 .image2-465-754}

###### `U.S. Army Corps of Engineers from USA, CC BY 2.0, via Wikimedia Commons`

I likely have recommended Richard Powers's book, *The Overstory*, in
this newsletter before. If not, you should definitely read it. It's
fiction but it's shot through with real science and history about trees.
A cornerstone piece of the book is related to the decimation of the
American Chestnut tree by blight which began around the turn of the 20th
century. The wholesale destruction of the American Chestnut caused
entire ecosystems to fall or shift including the extinction of several
species which relied solely on the American Chestnut for food or shelter
or both. There is a movement to plant more American Chestnuts from the
seeds of the few that have survived. You can plant an American Chestnut
by buying a seedling from a company in Maine.

[Plant an American
Chestnut](https://www.fedcoseeds.com/trees/american-chestnut-seedling-452){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

Have a great weekend.

Your guide,

Alex

## Stockholm Library

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/483d31c8-ab27-4afc-9ea1-23e6b461411d_3790x2983.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F483d31c8-ab27-4afc-9ea1-23e6b461411d_3790x2983.jpeg){.image-link
.image2 .image2-1146-1456}

###### `By Alex Ezell`
