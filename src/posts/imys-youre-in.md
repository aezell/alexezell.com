::: {.captioned-image-container}
![The I Miss Your Smell writing bunker. Location:
undisclosed.](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/0d0405a8-4f45-4153-8b96-ae818be7cb8d_1650x1272.jpeg)
:::

Hi y'all,

I have some housekeeping updates and news about I Miss Your Smell.

Due to popular demand (it was exactly one email), I've now enabled some
community features like comments and such for all the newsletters. You
won't even notice unless you want to go looking for them.

A few more of you recently mentioned wanting to "tip" me or pay for a
subscription. Hence, I've added some options for paid subscriptions.
I'll be looking into ways to incorporate some notion of "tipping" but I
haven't figured it out how to do it in an easy, unobtrusive way yet.

Regardless, **the weekly newsletter that you all signed up for will
always be free**. Paid subscribers will, in addition to the weekly
newsletter, receive the occasional story, essay, or special delivery
(like the yoga story). Likely, this will be around 1-2 extra emails per
week. It'll always be with the same voice and discerning eye. Paid
subscribers will also have access to the full archive of IMYS so you can
find that link you misplaced.

All that said, if you are receiving this, **I've comped you a free,
forever paid subscription**. You'll get everything free for all time. If
you don't want that and you're interested in just the main weekly
newsletter, let me know. I can reset you back to the Free account.

As always, thanks for your attention and trust.

Your guide,

Alex
