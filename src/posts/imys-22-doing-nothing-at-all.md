I take a lot of magazine subscriptions. Some are informative. Some are
entertainment. Most are somewhere in between. It seems like an
old-fashioned affectation in the same way that I still drive to a
bookstore and buy a book off a shelf. Sure, I like to read with ink on
paper, that's part of it. But, the larger part is that a magazine is
like a little portable time machine. Hear me out.

Books get heavy. But, a magazine is light. It's flexible. You can roll
it up, fold it back, tear out some pages. You can take a magazine
anywhere you like. So, it's easy to always have at hand. This
portability is key if you need to escape at a moment's notice.

Magazine articles are short. Certainly shorter than a novel. Sometimes,
they get long if you are reading Harper's or the New Yorker. But,
generally, you can knock one out in less than 15 minutes. Given the
breadth of topics possible, imagine being transported to another time
and place for 15 minutes no matter where you are.

Now, we have a thing that costs a few dollars, can take us anywhere our
minds want to go at any time we want to go there. We can carry it with
us and if we lose it, it's OK. We can use it to swat a fly, sweep up
some crumbs, or as a coaster. We can give it to someone or we can tear
out the pages and just give them that since we aren't done reading that
article about salmon farms. Hurray for the magazine.

I know what you're all thinking. "I just read those same articles on my
phone or my iPad." Sure, you could but here's the *coup de grace*: a
magazine demands nothing of your attention but the article you are
reading. It won't buzz or flash or popup or ring. It won't get a little
red dot with a number in it reminding you of work. It won't run out of
batteries. It won't make you insane.

Take a subscription to a magazine and find the joy in that periodic
piece of mail that enlightens, entertains, and connects.

[Share](https://imissyoursmell.substack.com/p/imys-22-doing-nothing-at-all?&utm_source=substack&utm_medium=email&utm_content=share&action=share){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

## Dinner is Ready

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/55d01a64-e04f-4b18-be44-098d4d562684_4060x3248.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F55d01a64-e04f-4b18-be44-098d4d562684_4060x3248.jpeg){.image-link
.image2 .image2-880-1100}

###### By Ella Olsson from Stockholm, Sweden - Farm Vegetables, [CC BY 2.0](https://commons.wikimedia.org/w/index.php?curid=83080866)

All of our harvest seasons have ended. The land is spinning down into
its quiet rest for winter. Yet, we still need all the bounty the land
provides. We still have to eat. I love to eat. Emergence Magazine's new
issue is all about food. They've included one of my favorite bits of
writing about eating and that's Wendell Berry's "The Pleasures of
Eating." We've talked about Wendell here before. If you aren't familiar
with his writing, this is a great introduction. As Alice Waters points
out in her introduction, the connection he makes between the land, the
farmer, and ultimately, the eater is powerful. I also appreciate the
recognition that we can take pleasure from this. So much of our food
culture now is guilt. Guilt about sugar, or gluten, or carbs, or meat,
or whatever. Berry is tackling some of the same challenges but he's
reframing not as guilt or sin to be avoided or absolved but as actions
we can take and through that effort find joy.

[Read Berry\'s \"The Pleasures of
Eating\"](https://emergencemagazine.org/story/the-pleasures-of-eating/){.button
.primary}

## Rest, Relax, Repose

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/147b1e62-1669-4852-8700-1d17d7471f64_5902x3821.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F147b1e62-1669-4852-8700-1d17d7471f64_5902x3821.jpeg){.image-link
.image2 .image2-712-1100}

###### By Ввласенко - Own work, [CC BY-SA 3.0](https://commons.wikimedia.org/w/index.php?curid=73508117)

I'm a daydreamer. I often find myself staring out the window that sits
just behind my desk. I sometimes chide myself on being distracted or
unfocused. But, this article from Aeon would rather I consider these
moments of being idle as necessary and valuable. I get it. There's
nothing I like more than to walk outside and spend 10 minutes just
sitting in the grass in front of my house. I'm sure my neighbors are
wondering what I'm doing. The answer is nothing. I'm doing exactly
nothing. I'm reminded of the question I always got about long-distance
running, "What do you think about for hours?" On my best days, I thought
about nothing. My head was as clear as it could be. Sure, physically, I
was active, but mentally, I was inert. There's a great sense of release
in that.

[Read about being
lazy](https://aeon.co/ideas/to-make-laziness-work-for-you-put-some-effort-into-it){.button
.primary}

## Embrace the Id

::: {#youtube2-yVs-veVcOTM .youtube-wrap attrs="{\"videoId\":\"yVs-veVcOTM\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=yVs-veVcOTM), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

If the song wasn't compelling enough, the video for The Front Bottoms
(best name ever?) song, "Vacation Town," captures the battle we all
fight with ourselves. Just how much of this shit will we put up with
until we just do the things we love and let the rest sort themselves
out? As the winter bears down, I want to escape to escape to my own
vacation town surrounded by books, good food, good music, and good
friends.

[Listen to \"Vacation
Town\"](https://www.youtube.com/watch?v=yVs-veVcOTM){.button .primary}

## Staring Into the Abyss

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/cbbc92aa-70d3-4ba9-987b-b6200ad9fbb5_4608x3456.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fcbbc92aa-70d3-4ba9-987b-b6200ad9fbb5_4608x3456.jpeg){.image-link
.image2 .image2-825-1100}

###### By [Sebastian Dooris](https://www.flickr.com/photos/sebastiandooris/32053374988/), [CC0](https://commons.wikimedia.org/w/index.php?curid=74617141)

Catherine Ingram is a spiritual teacher, writer, and journalist who
examines issues of consciousness and activism. It makes sense that she'd
be in tune with our current climate crisis. In many ways, the crisis
extends from our disconnect with the shared consciousness which unites
all living things. Not only have we severed that connection but we seem
to actively fight its attempts at a new embrace. It does feel like an
onrushing disaster. In this long meditation, Ingram provides some
framing for the worst-case scenario as it might play out for us:
extinction. It's clear-eyed and sobering and somehow finds its way to
hopefulness. The ideas reach far and wide but center on our capacity for
creation and love. It can't be all bad if we have those two things in
hand.

[Read Ingram on Facing
Extinction](https://www.catherineingram.com/facingextinction/){.button
.primary}

## Lovable Loser

::: {#youtube2-HUJmajB7O_s .youtube-wrap attrs="{\"videoId\":\"HUJmajB7O_s\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=HUJmajB7O_s), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Full Disclosure: This American Aquarium song is full of talk-singing. I
know that's a real challenge for many folks. Still, the lyrics here are
wry and maybe sardonic. It's a loser's lament that ends with the
acceptance that we make our own way in the world. It's catchy and
self-effacing with a nice little banjo lick. The best line, "We love you
and we know that you tried." Oof.

[Listen to \"Losing Side of
25\"](https://www.youtube.com/watch?v=HUJmajB7O_s){.button .primary}

## Hot or Not

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/af759b65-2463-4869-8dfe-be757db6faa2_5472x3648.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Faf759b65-2463-4869-8dfe-be757db6faa2_5472x3648.jpeg){.image-link
.image2 .image2-733-1100}

###### By Anthony Quintano from Honolulu, HI, United States - Mark Zuckerberg F8 2018 Keynote, [CC BY 2.0](https://commons.wikimedia.org/w/index.php?curid=70827307)

I sometimes play a thought experiment game with friends. Name three
human inventions that will likely destroy our species. Some people want
to go with nuclear fission. It's a little too easy. I've always chosen
the combustion engine first. So many horrible things in our world
descend from the ability to get goods and people from one place to
another cheaply and easily. And, yeah, massive carbon emissions.
Recently, I've wondered if Facebook might not be climbing up my list. It
seems I'm not alone as Ol' Zuckerberg was pelted with tough questions in
a Congressional hearing this week. In some ways, this kind of attention
is a self-perpetuating disaster. Every mention maybe draws another user
even when that mention is about false advertising, financial
malfeasance, horrible working conditions, or other detestable practices.
The company continues to exist because people continue to use it and
they continue to make money. If you don't like how Facebook does what it
does, just stop using it. It is not required for survival.

[Read about Zuckerberg\'s
hearing](https://www.nytimes.com/2019/10/23/technology/facebook-zuckerberg-libra-congress.html){.button
.primary}

## To the Blitz Cave

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/81da8fa4-665c-4f00-bcf7-38910f7d5855_6000x4000.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F81da8fa4-665c-4f00-bcf7-38910f7d5855_6000x4000.jpeg){.image-link
.image2 .image2-733-1100}

###### By Andrew Gray - Own work, [CC BY-SA 3.0](https://commons.wikimedia.org/w/index.php?curid=53956464)

It's likely well-known that during the Blitz of London in World War II,
underground Tube stations were refitted for use as offices, bunkers, and
safe refuges from the German planes buzzing above. This piece in
Lapham's Quarterly indicates how much of this was planned far earlier
than I would have expected. It's interesting that years before the war
began, as early as 1929, planners were identifying these underground
warrens and putting together a network of them to use should the be
needed. Of course, they were needed. I'm struck especially that London
had been bombed during the First World War and so some of those people
knew the fear and disruption that kind of attack could bring about. I
suppose none of them believed it had been the "war to end all wars."

[Read about London\'s underground
bunkers](https://www.laphamsquarterly.org/roundtable/mind-secret-bunker){.button
.primary}

## R.E.M. Song of the Week

::: {#youtube2-Go9t68mQOl0 .youtube-wrap attrs="{\"videoId\":\"Go9t68mQOl0\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=Go9t68mQOl0), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

The band has always had a penchant for covering music that they like. In
the case of "Crazy" off the *Dead Letter Office* compilation, they
covered another Athens-based band, Pylon. To my ears, this cover is
fairly straight and true to the original but it softens some of the
angularity and anxiety in the original in favor of a more loping feel.
It's a great song by a great band covered by an equally great band.
Can't miss.

[Listen to
\"Crazy\"](https://www.youtube.com/watch?v=Go9t68mQOl0){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

Have a great weekend and seek a bit of discomfort.

Your guide,

Alex

## Chicago River

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/54260852-315f-4c63-adad-659b57a0e688_1934x1484.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F54260852-315f-4c63-adad-659b57a0e688_1934x1484.jpeg){.image-link
.image2 .image2-844-1100}

###### By Alex Ezell
