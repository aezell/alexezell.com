### Season's Greetings!

What an odd phrase this is. It's nice that it's inclusive and just
covers all the bases. But, in that inclusivity, it's also quite
non-specific. If I said this to you as the sun rose on the Spring
Solstice, it would work. If I said this to at the height of the Summer
sun, it would work. Very non-committal. Would not use again. C-

Anyway, thanks for joining me this week. I thought I'd share a missive
full of nothing but my favorite versions of my favorite Christmas songs
with a brief explanation of why I like them. Less think piece-y than
usual which might be a good thing. I've added them all to a playlist
along with others I love which you can find at the bottom for various
streaming services, if available.

#### Merry Christmas!

Alex

<div>

------------------------------------------------------------------------

</div>

## El Vez - Feliz Navidad

::: {#youtube2-5ePmZKk-A5E .youtube-wrap attrs="{\"videoId\":\"5ePmZKk-A5E\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=5ePmZKk-A5E), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

There's something completely unhinged about this version of the José
Feliciano classic. First, El Vez is a magnetic performer here in his
sort of Elvis meets Freddie Mercury vibe. Second, the breakdown the band
does in the second half song is strong and in some vibes extends into an
incredible jam. [There is an entire El Vez Christmas album called Merry
Mex-mas.](https://www.discogs.com/El-Vez-Merry-MeX-mas/release/2583402)

## James Brown - Santa Claus Go Straight to the Ghetto

::: {#youtube2-cz48PR__uSo .youtube-wrap attrs="{\"videoId\":\"cz48PR__uSo\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=cz48PR__uSo), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

I don't have to tell y'all about James Brown. This song was written by a
few of the usual songwriters in his band named Alfred Ellis, Charles
Bobbitt, and Hank Ballard (who gets a call out in the song). Brown's
album *A Soulful Christmas* was released in 1968. I appreciate the
social plea in the song combined with a seemingly restrained performance
from Brown. It kicks off an album of 100% original Christmas songs which
is impressive in its own right.

## Bing Crosby - Mele Kalikimaka

::: {#youtube2-ob4LT_gUSFQ .youtube-wrap attrs="{\"videoId\":\"ob4LT_gUSFQ\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=ob4LT_gUSFQ), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Bing Crosby is a staple of any Christmas music roundup. This particular
song is great because it combines that Crosby lackadaisical, crooner
style with a strange 1950s obsession with "exotica" (which grew in
popularity as soldiers returned home to the US from the South Pacific).
Lots of small GI starter homes in this country had basements kitted out
with tiki carvings and other appropriated buts of Polynesian culture.
This song came in right at the beginning of that fad. Interestingly, the
phrase "mele kalikimaka" is [reverse-engineered into Hawaiian from
English](https://en.wikipedia.org/wiki/Mele_Kalikimaka#Origin_of_the_phrase).

## The Moonglows - Hey Santa Claus

::: {#youtube2-aETPuygYpMM .youtube-wrap attrs="{\"videoId\":\"aETPuygYpMM\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=aETPuygYpMM), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

The Moonglows were a 1950s doo-wop group that combined some powerful R&B
elements not normally heard in doo-wop. That's cool but I really love
this song because it shows up in National Lampoon's *Christmas Vacation*
which is my favorite Christmas movie.

## Bahamas - Christmas Must Be Tonight

::: {#youtube2-GX3zOZ92iu4 .youtube-wrap attrs="{\"videoId\":\"GX3zOZ92iu4\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=GX3zOZ92iu4), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

I'm a sucker for a strong original Christmas tune. Bahamas brings that
in spades here. Telling the nativity story against the backdrop of that
usual great Bahamas guitar tone catches my ear every time.

## Manchester Orchestra - Have Yourself A Merry Little Christmas

::: {.bandcamp-wrap attrs="{\"url\":\"https://manchesterorchestramusic.bandcamp.com/track/have-yourself-a-merry-little-christmas\",\"title\":\"Have Yourself a Merry Little Christmas, by manchester orchestra\",\"description\":\"from the album Christmas Songs Vol. 1\",\"thumbnail_url\":\"https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/169c5713-032e-4479-a7ea-c8a74b41e169_700x700.jpeg\",\"author\":\"manchester orchestra\",\"embed_url\":\"https://bandcamp.com/EmbeddedPlayer/size=large/bgcol=ffffff/linkcol=333333/tracklist=false/artwork=small/track=838993808/transparent=true/\",\"is_album\":false}"}
::: {.iframe}
::: {#player .inline_player .initialstate .size_large .smallartwork}
::: {#artarea}
[](#){#artlink}

::: {#art .item}
:::

::: {#big_play_button .item}
::: {#big_play_icon}
:::
:::

[[ ]{.icon}](#){.logo}
:::

::: {#nonartarea}
::: {#merch .item}
:::

::: {#infolayer}
[](#){.artlink}

::: {.art .item}
:::

::: {.info}
[[ ]{.icon}](#){.logo} [[ ]{.icon}](#){#logoalt2}

::: {#maintext .item .linktext}
[](#){#maintextlink}
:::

::: {#linkarea .item}
[ [download](#){#buyordownloadlink} ]{#buyordownload} [
[share](#){#sharelink} ]{#share}
:::

::: {#subtext .item .linktext}
[](#){#subtextlink}
:::

::: {#albumtrackartistrow}
::: {#album .item}
:::

::: {#trackname .item}
[](#){#tracknamelink}
:::

::: {#artist .item}
:::
:::

::: {#play .embeddedplaybutton .item}
:::

::: {#currenttitlerow}
::: {#currenttitle .item}
[]{#currenttitle_tracknum} []{#currenttitle_title .fgtext}
:::

::: {#currenttimeblock}
::: {#currenttime .item}
:::

::: {#totaltime .item}
:::
:::
:::

::: {#timelinecontainer}
::: {#timeline .item}
::: {.progbar_empty}
::: {#progbar_fill .progbar_fill}
::: {#progbar_fill_played .progbar_fill .played}
:::
:::

::: {#progbar_thumb .thumb}
:::
:::
:::
:::

::: {#prevnext}
::: {#prev .prevbutton .item .big}
::: {.icon}
:::
:::

::: {#next .nextbutton .item .big}
::: {.icon}
:::
:::
:::
:::
:::

::: {#tracklist .item}
::: {#tracklist_scroller}
:::
:::

::: {#linkareaalt .item}
[ [download](#){#buyordownloadlinkalt} ]{#buyordownloadalt} [
[share](#){#sharelinkalt} ]{#sharealt}
:::

[[ ]{.icon}](#){#logoalt}

::: {style="display: none"}
:::
:::

::: {#sharedialog title="Share"}
[Embed this track[[small]{.small .bc-ui2}[medium]{.medium
.bc-ui2}[large]{.large
.bc-ui2}]{.icons}](https://manchesterorchestramusic.bandcamp.com/track/have-yourself-a-merry-little-christmas?action=embed)

Email:

::: {.share-embed-container}
::: {.downloadShare}
-   
-   
-   
:::
:::
:::

::: {#tinyplayer .tinyplayer .item}
[](#){#infolink}
:::

::: {#badtralbumerror .error}
Sorry, this track or album is not available.
:::

::: {#badbrowsererror .error}
Sorry, this player does not support your browser. Your browser must
either support native HTML audio or have the Flash plugin installed.
:::
:::
:::
:::

Last week, Manchester Orchestra surprise-released a Christmas EP on
[their Patreon](https://www.patreon.com/ManchesterOrchestra). If you
like the band, I recommended subscribing. This version of a sentimental,
maudlin Christmas song is wonderful. When the lead singer, Andy Hull,
hit the bit about our being together "if the fates allow," I was in
tears. That being together means a lot more this year than it might have
before.

## O Come, O Come Emmanuel - The Punch Brothers

::: {#youtube2-s097Ek3YcuM .youtube-wrap attrs="{\"videoId\":\"s097Ek3YcuM\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=s097Ek3YcuM), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Chris Thile has proven himself to be not only an incredible musician but
also a deft arbiter of taste on his radio show, Live from Here, which
replaced A Prairie Home Companion. Before all of that, he put together
the band, The Punch Brothers, and lucky for us, they covered this oddly
anxious version of the staple carol. Thile's singing here is a nicely
casual and laid-back counterpoint to the energetic, near frenetic
playing.

## The Pogues feat. Kirsty MacColl - Fairytale of New York

::: {#youtube2-j9jbdgZidu8 .youtube-wrap attrs="{\"videoId\":\"j9jbdgZidu8\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=j9jbdgZidu8), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

This is a controversial song because of some epithets used in the
lyrics. I believe that the characters in the song are true to their
admittedly vile character and use those words as true to their
character. None of us can know the heart of songwriter Shane MacGowan.
This is not a happy song. It's a deeply sad song. As I read the articles
about the meth arrests and opioid overdoses here at the confluence of
the Christmas holidays with the pall of the pandemic, the song is sadly
fitting.

## Bruce Springsteen - Santa Claus Is Comin' To Town

::: {#youtube2-76WFkKp8Tjs .youtube-wrap attrs="{\"videoId\":\"76WFkKp8Tjs\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=76WFkKp8Tjs), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Let's end on a note of pure, unadulterated joy. Sure, we've lost the Big
Man but the ebullient playing and singing on this Springsteen version of
the classic is a balm for whatever ails you. You can find some other
great live versions of this song on Youtube like this version from a
[1978 show in Houston](https://www.youtube.com/watch?v=RcV8INh0d7g).

<div>

------------------------------------------------------------------------

</div>

Those are some my very favorite Christmas songs. I hope you enjoy them!

[Listen on
Spotify](https://open.spotify.com/playlist/4MpaiHubRgJUO1lkR1dGNV?si=BYqM9C7mTDCuXlvEKDlGIw){.button
.primary}

[Listen on Youtube
Music](https://music.youtube.com/playlist?list=PLkXuyb39QNAdt9FhEjMu-eyG5UuhiZohi){.button
.primary}

[Listen on Qobuz](https://open.qobuz.com/playlist/5408081){.button
.primary}

::: {.captioned-image-container}
![Christmas
2010](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/b2e00549-0182-4491-905e-14125352bbd2_1506x2108.jpeg)
:::
