# I miss your smell

### A weekly newsletter from Alex Ezell

Journeys lay before us and we fear their start much more than their end.
It\'s a glaring blindness that holds us back from the multitude of
futures we might conquer. Take the next step with me.\
\
**The Grim Dark Follows**\
I recently tucked into the last book of Joe Abercrombie\'s *First Law
Trilogy* called [Last Argument of
Kings](https://www.goodreads.com/book/show/3235477-last-argument-of-kings).
Abercrombie\'s books are astonishingly exciting and well-written. As a
primary example of the
\"[grimdark](https://en.wikipedia.org/wiki/Grimdark)\" genre, these
novels combine some of the fantasy tropes with hyper-violent action and
character development that extends from the noir tradition. If you scoff
at the clichéd, near-utopian worlds of most common fantasy, you may find
Abercrombie\'s work a fresh take on what fantasy can be.\
\
**Stunning Admissions**\
I\'m still digesting [this Bloomberg piece detailing Andrés Sepúlveda\'s
admissions that he rigged
elections](http://www.bloomberg.com/features/2016-how-to-hack-an-election/)
across South America for years. And we are supposed to worry about voter
fraud. This makes it clear that these kinds of things happen on a level
far way from the voters themselves except in their manipulation and
betrayal.\
\
**The Music Lives On**\
Sad news out of Athens, GA this week that Ross Shapiro died. Shapiro was
a founding member of The Glands who had a brief but highly influential
career. Shapiro was also an owner of Schoolkids Records (closed in 2011)
in Athens which I frequented in my undergraduate days. I have a specific
memory of buying tickets to a Five Eight show at the store. Like all
great record stores, it was idiosyncratic, opinionated, and
mind-opening. I imagine it was a solid reflection of Shapiro. Check out
[The Glands doing \"Livin\' Was
Easy.](https://www.youtube.com/watch?v=IRcv9MHfY2A)\"\
\
**Capitalism Runs Amok**\
We all know that China is a communist country but they have a burgeoning
stock market that is reminiscent of the market in the US around the turn
of the 20th century. Like that market, there is a lot of volatility from
a lack of regulation and information made all the more problematic by
more recent inventions such as the hedge fund and high-speed, automated
trading. It follows then that the first true [hedge fund king of China
would rise and fall most
spectacularly](http://www.nytimes.com/2016/04/03/magazine/the-fall-of-chinas-hedge-fund-king.html).
Alex Palmer\'s article tells a cautionary tale with echoes in
Showtime\'s *Billions* TV show and the vast forces at work in markets
whose size and complexity we can\'t possibly comprehend.\
\
**Enemies of Enemies are Friends**\
Noel Gallagher [committed to booing the
DMA\'s](http://themusic.com.au/news/all/2015/03/04/oasis-noel-gallagher-im-going-to-boo-dmas-when-i-see-them/)
when he hears them. While the band wears their influences on their
sleeves and Oasis is clearly among them, Gallagher\'s distaste is
misplaced. [Their new album, Hills
End](https://open.spotify.com/album/5UmpYG6wOrGTgCexdQZLOA), peaks with
[the song \"Delete\"](https://www.youtube.com/watch?v=FhkhuarqDGE) but
there are other gems throughout the record. It seems there\'s an
intriguing Australian band every year or so and I think the DMA\'s may
take that mantle this year.\
\
**Check Into Idiocy**\
The check cashing industry, which was started in Cleveland, TN by Allan
Jones, has long been derided for its predatory practices including
exorbitant fees, usurious interest rates, and policies that take
advantage of the disadvantaged communities in which they are often
found. This week, [the worlds of software development, institutional
bigotry in the police force, and the check cashing industry
collided](https://storify.com/rodneysampson/adayinthelife) to embarrass
and potentially endanger a young man trying to better himself. There are
clearly two sides to every story but its hard to imagine what might have
happened that justifies the actions taken by store employees and police
in this story. It\'s a small story that possibly illustrates some of the
greatest ills our society faces.\
\
**A Break-Up Song**\
Robert Ellis is a Texas-based singer-songwriter whose [2014 album \"The
Lights from the Chemical
Plant\"](https://open.spotify.com/album/32kFjsz0DC3HWAftZuvVpc) combines
several genres with Ellis\'s uniquely plaintive and melodic voice. Ellis
is back with [new music with this single
\"Drivin\'\"](http://www.npr.org/2016/03/25/471302109/songs-we-love-robert-ellis-drivin)
which will be featured on his eponymously titled new album. His
signature sound is still intact but it\'s possibly there\'s a bit more
irony or light-heartedness on this new record.\
\
**Mega Rock**\
My son has recently gotten deep into the Mega Man comic books based off
the video game that debuted in 1987 on the NES. He loves the characters,
the action, and the bold, bright colors. So, I was excited to share [The
Protomen\'s album \"Act II: The Father of
Death\"](https://open.spotify.com/album/5dVmbXxmkROv6smdOfTcyJ) which is
a concept album loosely telling the backstory of Dr. Wily came to power.
It\'s a prequel to their [\"Act I: The
Protomen\"](https://open.spotify.com/album/3qHPyK7e96uprnWQWzVOnB) which
has a more raw sound. Act II shows off a vast array of influences and
some wonderful production while telling a compelling story of the
rivalry between Dr. Wily and Thomas Light. [Check out \"The Good
Doctor\" for a taste](https://www.youtube.com/watch?v=vgGYwXYt3J0) of
what Act II is about.\
\
**Beauty Reigns**\
Michael Stipe was joined by Paul Cantelone to deliver [a version of
\"Man Who Sold the World\"](https://youtu.be/hF2ed7ouU3o) as a simple,
yet stunning tribute to David Bowie. Stipe\'s beard would not answer
questions after the performance.\
\
**R.E.M. Song of the Week**\
From one of R.E.M.\'s earliest recorded works, the Chronic Town EP, this
week [we check out \"Carnival of Sorts
(Boxcars)\"](https://www.youtube.com/watch?v=Tv4nuD7q2uQ) which was one
of the first R.E.M. songs I heard when my brother Rob played the tape
for me.\
\
\--\
\
Please share this email with friends. Let\'s turn this into a community
sharing a culture of interesting music and news and creativity.
