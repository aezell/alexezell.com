I finished a book this weekend titled *The Light Brigade* by Kameron
Hurley. She writes what I would call *weird* science fiction in a
similar vein to Jeff Vandermeer but with more connection to space opera
and military sci-fi. I know that likely turned off quite a lot of you.
But, trust me, she is an author to pay attention to. Here's why.

In this book, a soldier is turned into light to be beamed to other parts
of the world or other worlds. However, this light beam technology is new
and sometimes things don't work out. Limbs misplaced, brains scrambled,
complete incorporeality. It's body horror but that's just where she's
getting started. In her previous novel, *The Stars are Legion*, the
world-sized space ships were made of organic material, all oozing and
pulsing flesh.

That's cool. But, what I want to talk more about is that the soldier in
*The Light Brigade* never knows when or where or if they will arrive at
their destination during these "light drops." They feel the process
begin and then something will happen on the other end. It's largely
oblivion in between until \<spoiler alert\> it isn't.

I'm intrigued by this idea of closing your eyes, nodding your head, and
disappearing from the world until you reappear with no sense of what
happened, if anything, in between. It reminds me of sleep.

Each night (or maybe a lockdown nap, if I'm honest), I close my eyes and
lower my shoulders and I fall asleep. I awake the next morning. My eyes
open. Then, as I consider the day ahead, my shoulders rise up toward my
ears. I swing my legs off the side of the bed and huff myself to
standing. I assume that the world keeps rotating and that things
continue to occur but I can't be sure.

In my life, I've reached for this oblivion in any number of ways.
Focused dedication to learning (cramming for an exam). Performing on
stage live. Alcohol. Meditation. Extreme endurance exercise. The 12
hours of sleep I got last night. Taking a long shower. All of these are
attempts to touch the beauty not of the liminal edge of oblivion but of
the release that lies deep within. I suppose a therapist might say this
is a death wish.

I don't see it that way. I think of it as a place where we can be truly
free, if only for a moment. When we sleep, we are free to dream of
anything great or small, good or evil. It's a place of infinities and
while we often think of oblivion being a state of nothingness, it really
means that we are simply unaware. Oblivion then might be proof that the
world does exist and continue to progress even as we stand outside of
the awareness of that existence.

There is power in that perspective of watching, assessing, learning, and
then applying what we've learned. I ran about 45 miles in 12 hours one
time. It's the longest race I've ever run. After about 30 miles, I
wasn't sure what time it was, what day it was, or even why I continued
to run in a circle. It was simply motion for motion's sake. At some
point, I figured out how I should solve a problem at work. I decided on
what was going to happen to the characters in the novel I'm writing. I
wrote entire poems. I hummed unknown melodies. I saw the arc of a
relationship that I'd struggled with for some time. I saw my complicity
in its demise. In short, I was above the world, out of body if you will,
seeing things from some height that absolved my from judgment.

Simpler than that, how many times have you had a great idea in the
shower? How many times has a walk "cleared your head" and allowed you to
move past whatever was in your way? A good night's sleep is a salve for
even the most stricken of hearts.

It seems to me that in this state of oblivion, we have access to a level
of perception and connection that simply isn't present when our
awareness is fully in the room. Hurley's character experiences this
oblivion in a rather horrific way but eventually they discover the power
I just described. This new-found power carries them to a resolution they
could never have seen with eyes wide open.

I believe that the main thing keeping us from finding this oblivion is
that most of us think we are too busy. We rush around doing the things
we think are important and focusing on the immediate. All of that is the
barrier that keeps us from accessing this infinite oblivion. Even when
we attempt to meditate, take a nature bath, or chemically enhance, our
fear of obliviousness always keeps us on the waking side of the abyss.

Are we brave enough to pierce that veil and pull ourselves out of time
and place for a few moments, observe the rotation of the Earth, make a
few decisions, and then reenter the world ready to act?

<div>

------------------------------------------------------------------------

</div>

## Down at the Local

This piece about the last pub in Stonesfield, England is a local story
writ large. The White Horse Tavern is the last pub in Stonesfield. There
used to be seven pubs there. It's the core of a community of 1500 people
that will simply disappear of the locals can't raise the money to buy
it. Despite having some historical designation, the pub could still be
sold but its owner to be turned into homes or something else. As more
and more people fill larger and larger cities, smaller communities like
this are becoming more and more just the places where people live.
Increasingly, they want to live in privacy and not interact with their
neighbors at all. The flagging sense of community died long before taxes
and more liberal alcohol sales made pubs a dying species.

[Read about The White Horse
Tavern](https://www.nytimes.com/2020/11/28/world/europe/england-.html){.button
.primary}

## A Man of His Time

The new biography of Graham Greene by Richard Greene (no relation)
focuses on the supposed wildness of Greene. His love of alcohol, danger,
and women (maybe not in that order) seems to make up the myth of the
author. Richard Greene's biography really wants to sell that story above
all the other things that might Graham Greene worth considering. This
Guardian review of the biography points out those flaws but also points
us toward some of the things we should consider worthwhile about Greene.

[Read a review of Russian
Roulette](https://www.theguardian.com/books/2020/nov/28/russian-roulette-the-life-and-times-of-graham-greene-review-addicted-to-danger){.button
.primary}

## The Other Me

::: {#youtube2-zDPwz0BTTo4 .youtube-wrap attrs="{\"videoId\":\"zDPwz0BTTo4\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=zDPwz0BTTo4), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

A friend recently sent me the song, "The Real Me," by Shooter Jennings.
In it, Jennings describes how he becomes a completely different person
when he drinks. He is certainly describing an experience that I had back
when I was drinking. But, I think it's a little too judgmental to call
it, "The Real Me." Instead, I like Jason Isbell's description of this in
"Live Oak" where it's more about "the other me." That is, one is isn't
real or fake but both are true at the same time.

[Listen to Live
Oak](https://www.youtube.com/watch?v=NwDF9Hn7Ak0){.button .primary}

## Mechanical Doping

It used to be that running was considered the easiest sport to get
started. After all, you just put on some shoes (or not) and head out the
door. That's likely still true. But, as someone with running shoes for
different kinds of trail or weather or length of run or simply color
preference, on top of a vest to carry food and water, a special hat, a
buff, winter gloves, a fancy watch, I can tell you that we've made
running a capitalist sport. It makes sense then that Nike and the other
shoemakers would be in an arms race around shoes. As a fan of distance
running, I'm also pretty amazed to see how times have come down. Sure,
the shoes help but it's still a shitload of work to get there. As a kid,
we used to talk about who had the faster shoes. It probably didn't make
sense back then but now it can.

[Read about Nike\'s fast
shoes](https://www.youtube.com/watch?v=NwDF9Hn7Ak0){.button .primary}

## A Love Story

::: {#youtube2--Y8AoL3Uszo .youtube-wrap attrs="{\"videoId\":\"-Y8AoL3Uszo\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=-Y8AoL3Uszo), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

I believe I have shared some of Lukas Nelson's songs in this space
before. I recently came across this cover he's done of Dire Straits's
"Romeo and Juliet." It's a song I really love and this version not only
does it justice but Nelson makes it his own along with a sturdy band.
Covers are always hit or miss and I think this one hits the bullseye.

[Listen to Romeo and Juliet](https://youtu.be/-Y8AoL3Uszo){.button
.primary}

## Get Lost

Susanna Clarke's new novel *Piranesi* is on my reading list. I really
enjoyed her *Jonathan Strange & Mr Norrell* and what I've heard about
this new book has me intrigued. I was interested then to read this piece
by Cameron Laux about how the novel is a sort of labyrinth and how the
labyrinthine might play a part in our culture. I'm reminded of what I
wrote above about finding oblivion and it seems to me that a labyrinth
is one method to achieve that dissociated state. Many of the Episcopal
churches I've been in featured labyrinths somewhere on their grounds.
This is not something we are newly discovering but maybe just
rediscovering.

[Read about
labyrinths](https://www.bbc.com/culture/article/20201118-the-mysterious-appeal-of-a-labyrinth){.button
.primary}

## I Feel It

::: {#youtube2-vyT1iHP28q0 .youtube-wrap attrs="{\"videoId\":\"vyT1iHP28q0\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=vyT1iHP28q0), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Just taking a look at my listening history in any of the places I listen
to music and you'll find a healthy does of emo, new and old. Back in
2005, I was teaching high school. I often played music in between
classes or doing planning periods. Kids are drawn to music like moths to
a flame. They would come into the classroom and say things, "You like
this?!" or "Have you heard their other album?" I don't think they
thought I was cool but it was a way for us to connect. So, maybe there's
some part of me that's a millenial high school student still listening
to emo. Anyway, Teenage Halloween is a hell of an emo band.

[Listen to the full
album](https://www.youtube.com/watch?v=vyT1iHP28q0){.button .primary}

<div>

------------------------------------------------------------------------

</div>

Thanks for joining me this week. It was snowing here earlier. Maybe next
week, I'll do a Christmas Music special edition?

Your guide,

Alex

## Stay Focused

::: {.captioned-image-container}
![By Alex
Ezell](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/d0f134f0-8681-49fe-b775-99bff2db8f3f_1920x1282.jpeg)
:::

## Big Eats

::: {.captioned-image-container}
![Fresh Air Barbecue by Alex
Ezell](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/2cdfabd2-747e-445f-aaa4-2e7b8dbd38d8_3264x2448.jpeg)
:::
