I've become that insufferable friend this week. I've only talked about
two things: the new exercise program I'm on and the book I'm working on.
Truly, maddeningly self-centered. I've written elsewhere about the
selfishness of exercise, specifically long-distance running.

It can also be true for artistic endeavors. Many of them require a
significant amount of individual effort away from the eyes or ears of
others until the work is done. But, the thing which gets created is
often designed specifically to become a part of some community and to
affect that community in some way. It's an interesting paradox.

I picture a musician toiling away on a melody or the writers fiddling
with just the write way to end that sentence or the sculptor shaving off
a millimeter from the edge or the carpenter fitting a tenon joint just
so. Then, all of that is turned over, released into the world to fend
for itself. What a brave thing. What an absolutely fearless act. It
seems radically vulnerable.

I hope that each of you finds that song, poem, story, sketch, quilt,
shelf, or whatever that you can release into the world. I know it's in
there. Dig it out.

<div>

------------------------------------------------------------------------

</div>

## Did You Try Frying It?

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/1eb2af41-d016-410a-8638-538ce1e3f14f_2592x1944.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F1eb2af41-d016-410a-8638-538ce1e3f14f_2592x1944.jpeg){.image-link
.image2 .image2-825-1100}

###### By David Monniaux - Cassava Root - Own work, [CC BY-SA 3.0](https://commons.wikimedia.org/w/index.php?curid=298810)

Humans eat pretty much anything. Sure, we lay that epithet on goats all
the time. But, really, we are the ultimate omnivores. That's because
we've discovered ways of making the inedible actually quite tasty. [This
BBC article dives into the topic of how this kind of learning actually
happens](https://www.bbc.com/news/business-48859333). Before things like
mass spectrometers and understanding of biochemistry, someone had to
discover how to make certain plants safe to eat. I'm reminded of a
conversation I was in recently about how certain plants might have
co-evolved alongside humans because their affect on our psyches was
beneficial. Specifically, the idea that plants, or fungi, with
psychotropic properties might have benefited from human consumption,
protection, or even cultivation thereby making that psychotropic
property an evolutionary benefit. The plants this article discusses seem
to succeed in different way. They are, in fact, poisonous to humans yet
we've learned how to inoculate or eradicate those poisons through
processes like drying, cooking, or careful selection. Animals won't eat
them, but humans will cultivate them. That's a unique way around the
problem.

[How did we learn to eat
that](https://www.bbc.com/news/business-48859333){.button .primary}

## It Could Have Been Easy

::: {#youtube2-k2QjEgOtCAA .youtube-wrap attrs="{\"videoId\":\"k2QjEgOtCAA\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=k2QjEgOtCAA), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Mandolin Orange is a band from NC that plays what I guess would be
called folk music. It's informed by bluegrass, folk, and string band
music but the lyrics and song structures are distinctly modern. I find a
deep sense of place and time in their songs and in their playing. In
[their recent Tiny Desk
Concert](https://www.youtube.com/watch?v=k2QjEgOtCAA), the two main
members of the band, Emily Frantz and Andrew Marlin, make the harmonies
and playing seem absolutely effortless. It's that ease and grace that I
find so inviting. The set ends up with one of my favorite songs of
theirs, "Wildfire." I appreciate the connection it makes between the
South I grew up in and the South that could have been.

[Watch the Tiny Desk
Concert](https://www.youtube.com/watch?v=k2QjEgOtCAA){.button .primary}

## The Elders

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/585d50e4-c772-4af2-96ed-12e708cb4be5_3000x2418.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F585d50e4-c772-4af2-96ed-12e708cb4be5_3000x2418.jpeg){.image-link
.image2 .image2-887-1100}

###### By [Provincial Archives of Alberta](https://www.flickr.com/photos/alberta_archives/29302774172/) - [No restrictions](https://commons.wikimedia.org/w/index.php?curid=52887497)

Many of us grew up seeing the natural world as a spectacle or maybe even
just a backdrop for our human endeavors. Nature existed in our lives to
provide wonder in the best case or simply resources for extraction in
the worst. As I've become more attuned and interested in how I live
within or without nature, my conception has shifted as I'm sure most
readers have noted. It's with excitement then that I read about [a newer
field of dendrology, the study of trees, which focuses on measuring the
changes in trees over
time](https://www.newyorker.com/science/elements/a-day-in-the-life-of-a-tree)
or even in human real-time. What intrigues me is that it could put to
rest this cultural notion of trees being inert or lifeless. Even in a
world where trees lose their leaves and flower or simply grow, many of
us have a sense that they are motionless. This new data and the research
around it gives us a sense that even for a being for whom time has a
different meaning, within the scope of a day, there is constant change.
Imagine if we began to understand why that change happened. As this
article tries to paint, how is the tree reacting to its environment in
the moment? That's a fairly radical idea for me. It doesn't take days or
weeks for a tree to react. It could be mere minutes. Now, add up all
those minutes of experiencing the world over the life of a tree that
lives 400 years. What do you think that tree might know that you don't?

[Read A Day in The
Life](https://www.newyorker.com/science/elements/a-day-in-the-life-of-a-tree){.button
.primary}

## Can't Help But Dance

::: {#youtube2-k64i8G6wBw4 .youtube-wrap attrs="{\"videoId\":\"k64i8G6wBw4\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=k64i8G6wBw4), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Imagine combining the talents of a Jamaican dancehall rapper with an
astoundingly prolific Ghanaian multi-instrumentalist and producer. Sure,
you're going to get some incredible Afro beats and some fast flows.
You're also going to get a lot of energy. So much energy that you can't
help but move. The lyrics are simple but strike at that heart of much of
the best music which is about pulling people together and empowering
them toward change and hope. All of that is what you'll get with [K.O.G.
& The Zongo Brigade](https://zongobrigade.bandcamp.com/). In this video,
the animation is also engaging and has the same frenetic energy of the
music. [Other songs are more
reggae-influenced](https://www.youtube.com/watch?v=vA1OWtfS3Sc) and slow
down with more dub-like production. Incidentally, I was introduced to
this band on the [Qobuz](https://www.qobuz.com/us-en/discover) service.
It's a streaming music service focused on providing high-quality
streams. It works just like Spotify but does away with algorithms and
social features in favor of human curation and a broader focus on music
other than Western pop.

[Dance with K.O.G. & The Zongo
Brigade](https://www.youtube.com/watch?v=k64i8G6wBw4){.button .primary}

## Drop and Give Me 20

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/00b0b9c5-6e36-4c8f-b83f-39397ee81e7a_5720x4610.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F00b0b9c5-6e36-4c8f-b83f-39397ee81e7a_5720x4610.jpeg){.image-link
.image2 .image2-887-1100}

###### By doe-oakridge - High School Basketball team Letterman\'s team initiation Oak Ridge, [Public Domain](https://commons.wikimedia.org/w/index.php?curid=69228687)

I was in a fraternity in college. There was hazing. Was it cruel? Maybe.
Was it unnecessary? Certainly. I'd like to think that I didn't engage
doing the hazing with the same zeal with which it was done to me. I
don't know if that's true. There's a karmic blemish around all of that
for sure. I've never given much thought to the repercussions that
hazing, as both recipient and practitioner, might have later in my life.
That is, [until I read this, rather long, piece
in](https://nplusonemag.com/issue-34/essays/special-journey-to-our-bottom-line/)
*[n+1](https://nplusonemag.com/issue-34/essays/special-journey-to-our-bottom-line/)*
[magazine](https://nplusonemag.com/issue-34/essays/special-journey-to-our-bottom-line/).
The essay gives a harrowing account of recent hazing incidents, some of
which ended in death. It then goes on to extrapolate how the hazing that
occurs at West Point might have affected how those same officers and
military leaders engaged in counterinsurgency tactics bordering on and
including torture. I'll admit that I find the connections strained and
tenuous but the psychological connection is fascinating to think about.
At a personal level, it did make me think about how those experiences in
my formative years as a young adult might have affected and may still
affect how I react to situations as an older adult. It had never
occurred to me that any of that might have left an imprint on me but it
seems so reasonable now. It makes me wonder about how we, as a society,
condone some behaviors in early adulthood as "harmless" without truly
understanding the outcomes they generate.

[Read about Hazing and
Counterinsurgency](https://nplusonemag.com/issue-34/essays/special-journey-to-our-bottom-line/){.button
.primary}

## Something a Bit More Comfortable

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/f18f699f-c56a-4b9a-8841-6575a6ac6ab6_3648x2736.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Ff18f699f-c56a-4b9a-8841-6575a6ac6ab6_3648x2736.jpeg){.image-link
.image2 .image2-825-1100}

###### By QuidoX, [CC BY-SA 3.0](https://commons.wikimedia.org/w/index.php?curid=58545134)

While we tend to think of America as a land of freedom, we are decidedly
Puritanical when it comes to nudity, especially non-sexual nudity. In
fact, I don't think the idea of non-sexual nudity exists in our culture.
If someone is nude, that is itself a sexual event or we sexualize it
regardless of the intent. It's why we have panics about pictures of
bathing babies on Facebook. [Look, then, to the Germans where a culture
of nudism
flourishes](https://www.nytimes.com/2019/08/31/world/europe/germany-nudism.html).
Most interesting to me in this article is the bit about the Communists
of East Germany trying to eradicate nudist practices and gathering
places. When they couldn't, they then celebrated it as an example of how
free and happy life was under Communist rule. Genius.

[Look at Nude
Germans](https://www.nytimes.com/2019/08/31/world/europe/germany-nudism.html){.button
.primary}

R.E.M. Song of the Week

::: {#youtube2-Y2RsHiRXnCg .youtube-wrap attrs="{\"videoId\":\"Y2RsHiRXnCg\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=Y2RsHiRXnCg), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

The phrase "Automatic for the People" is the catch phrase created by the
owner of the soul food restaurant, Weaver D's, in Athens, GA. It's still
there and you can go eat there. The greens and cornbread are amazing. It
was a kind nod to the band's hometown when they named the album this
way. There was a quietness to this album that the band would rebel
against when they released *Monster* a couple of years later. But, I
appreciate the string arrangements and space they give to a song like
"[Sweetness Follows](https://www.youtube.com/watch?v=Y2RsHiRXnCg)." The
song is ultimately hopeful though it feels like a dirge. Certainly, it
begins with a line about burial and death and that sense of loss is
there throughout. However, the title lies plaintively at the end. There
can be redemption.

[Listen to the
Sweetness](https://www.youtube.com/watch?v=Y2RsHiRXnCg){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

Thanks for joining me again this week. I'd be grateful if you shared
this on your social media channels and maybe asked your friends to sign
up. Just give them this link: <https://imissyoursmell.substack.com>

Your guide,

Alex

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/e9f616c3-c38f-4073-8f4b-62d3f5ecaf16_3264x2448.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fe9f616c3-c38f-4073-8f4b-62d3f5ecaf16_3264x2448.jpeg){.image-link
.image2 .image2-825-1100}

###### By Alex Ezell, View from Dug Gap near Dalton, GA
