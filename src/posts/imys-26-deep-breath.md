It feels better to write this newsletter on a Sunday, especially after a
week like the one we've just had here in the US. Sure, it's only about
24 hours since the election was called and there are lawsuits and
protests and eruptions of anger and frustration. Unfortunately, those
eruptions don't appear cathartic. As the left was stricken and saddened
by the results in 2016, the right seems to be angered and retrenched by
these results of 2020. The president-elect calls for reconciliation and
rediscovery of this American experiment but maybe we are one toe over
the line.

I can't help but think if we were just [one toke over the
line](https://www.youtube.com/watch?v=L9HXClusp_E) instead, we'd all be
able to get through this a lot easier. The decriminalization of drugs
continues to gain ground across the country. With liberalization laws
passing in several states, Oregon steps way out front by decriminalizing
the possession of all drugs. The new law acknowledges the fact that
addiction is a health matter and not a criminal matter. I hope to see
other states follow suit. This is also a key pillar of the "defund the
police" movement. Let's take all that money spent on body armor, pepper
spray, and assault vehicles and instead spend it on helping sick people
get better.

I count myself lucky to have had my addiction be one that's far more
socially acceptable. Alcoholism is as much a plot point or setup for a
joke as it is something I would have been put in jail for. Especially,
as a white male, the only thing that drinking might have gotten me in
trouble for was drunk driving. Meanwhile, if I had developed an
addiction to the painkillers I was prescribed after two collarbone
surgeries, I likely would have found myself shopping on the black market
with all the dangers that entails.

Instead, with alcohol, the state can benefit from the money spent on
taxes. The bars can continue to generate tax dollars and low-pay service
sector jobs that rich people seem to think are what we are all looking
for. I do fear that the legalization of marijuana (and potentially other
drugs) will lead us into the trap we've made with alcohol and nicotine.
Capitalism has a cruel hand when it comes to caution. That said, those
problems seem far more surmountable than do the ones of opiate
addiction, overdose deaths, drug market street violence, and a scarcity
of medical care.

Further, I'm excited about the increase in openness to studying the
benefits that psychotropic drugs can have for chronic mental illness
such as schizophrenia, PTSD, depression, and bipolar disorder. This
renewed interest means we might find in nature what we long ago
dismissed as shamanism, depravity, and a danger to social order. Maybe
now more than ever before, we can agree that the status quo is nothing
but rapacious destruction.

Perhaps we can tear down this world and build a new one in its place.

[Share I Miss Your
Smell](https://imissyoursmell.substack.com/?utm_source=substack&utm_medium=email&utm_content=share&action=share){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

## Just Kidding

::: {.captioned-image-container}
![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/1e19e4f2-6a6e-4c66-befe-84f60b6f4260_1200x628.jpeg)
:::

When I was studying and teaching literature, there was always a nagging
voice in the back of my head that told me it was all nonsense. That is,
all the symbols and metaphors and similes to which I attributed such
meaning and art might just be a winking joke. I still wonder how much of
the art we find in literature, painting, or music is accidental. There
is certainly a story behind the knights fighting snails in these
medieval manuscripts. Too bad, we don't know what it is.

[Read about Knights fighting
Snails](https://blogs.bl.uk/digitisedmanuscripts/2013/09/knight-v-snail.html){.button
.primary}

## Stankalicious

::: {#youtube2-QqFY8wSutu8 .youtube-wrap attrs="{\"videoId\":\"QqFY8wSutu8\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=QqFY8wSutu8), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Outkast has just released a deluxe, remastered version of their album,
*Stankonia*. It's the 20th anniversary and the album holds up incredibly
well. The beats are masterful and fun. The lyrics are perhaps a bit of
their time though they still rise above almost all current music in
their playfulness and occasional subterfuge. What stands out is the
tension between the more traditional rap structures and flow from Big
Boi and the sing-song, almost pop, sounds that Andre 3000 was moving to.
I guess that\'s what ultimately doomed the duo but here, it helps to
sharpen each perspective and propel the whole album to classic status.

[Listen to
Stankonia](https://www.youtube.com/watch?v=QqFY8wSutu8&list=PLAAvjySCSH-F6fLwWpZzWEZ5iHv7dZMPv){.button
.primary}

## Sharp as a Blade

::: {.captioned-image-container}
![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/3cfd3446-9acf-4bd5-885e-35d5c4075b52_1600x1067.jpeg)
:::

Wesley Snipes has been out of the limelight for many years, with good
reason. He spent a few years in prison for tax evasion. (They always get
you on the taxes.) That came after his films become unintentional
comedies that weren't earning back the way they should. Still, Snipes
got to where he was because he is magnetic, intense, and deeply weird.
It\'s fitting that his potential comeback will be starring in *Coming to
America 2* with Eddie Murphy. I was part of a group that did a few
sketch comedy bits based on the original. Shoulders of giants. Let\'s
hope that Snipes has better accountants now and that some time away has
renewed his focus. Whether it\'s comedy or drama or action, Snipes is
impossible to ignore.

[Read about Wesley Snipes
revival](https://www.theguardian.com/film/2020/nov/02/wesley-snipes-on-art-excellence-and-life-after-prison-i-hope-i-came-out-a-better-person){.button
.primary}

## Don't Stand So Close to Me

::: {.captioned-image-container}
![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/97f3e3e0-9d82-4070-a5fd-6bdde783d4f3_1072x715.jpeg)
:::

I've written in this space often about the power of nature, specifically
forests, to heal and energize us. In a similar fashion, human touch also
has deep power to affect our wellbeing. However, we live in a world,
even before the pandemic, which increasingly eschewed touch in nearly
all of the places where we gather. For good reasons, uninvited touching
of others is no longer acceptable, yet, it begs the question of what
this isolation might mean to us. I grew up in the Episcopal Church and
the bit of the mass where everyone would shake hands and say, "Peace be
with you," was always my favorite. There was something transgressive
about getting past the fancy clothes and the solemn seriousness of the
situation and touching other people. Will we enter an era of constant
distance or will we react and do quite the opposite?

[Read about the power of
touch](https://aeon.co/essays/touch-is-a-language-we-cannot-afford-to-forget){.button
.primary}

## Staying Alive

::: {.captioned-image-container}
![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/001016ff-83bf-428e-8a7f-107dd6131c64_1280x721.jpeg)
:::

George Romero passed away a few years back after leaving an indelible
mark on popular culture with his zombie films. Before his death, he
began work on a novel that would dig deeper into the social commentary
and satire at the heart of the zombie genre. With the blessing of the
Romero estate, Daniel Kraus has completed the novel titled, *The Living
Dead*. It is everything it's cracked up to be. The satire and eye-poking
is only outdone by the gore. Halloween is over but this book is worth
devoting some time to before the US holidays bear down on us. You'll be
rewarded with over-the-top violence and gore with an undercurrent of
dark humor and smirking finger-wagging.

[Check out The Living
Dead](https://us.macmillan.com/books/9781250305121){.button .primary}

<div>

------------------------------------------------------------------------

</div>

That's it for this week. Try to look away from the cable news and get
prepared for oncoming winter.

Your guide,

Alex

::: {.captioned-image-container}
![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/11646482-746d-4db8-b90f-88ac27334ade_3024x4032.jpeg)
:::

###### `Ginger greets the day. by Alex Ezell`
