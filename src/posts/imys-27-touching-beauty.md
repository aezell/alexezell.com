"He saw the beauty in it and he reached out and touched it."

That's what Dave Chappelle said about one of his sons trying out standup
comedy. When he said it, he glanced just above head height and raised
his palm on a lightly extended arm. It was a gesture of desire and
trepidation. His gaze wasn't toward the heavens. It was somewhere just
above where he currently sat. It was a perfect encapsulation of how each
of us could reach for beauty.

It's important too that the word here is "touch." It's not experience.
It's not own. It's not master. It's not practice. It is merely to touch.
That word reminds us of the gossamer fragility of beauty. Handle it too
roughly and it can disappear.

For some of us, there's a deeply ingrained Protestant work ethic that
tends to eschew beauty except in the sense of production or output. We
even have words for it like craftsmanship which separate that kind of
effort from something that is Art with a capital A. I do think there is
a through line with all these concepts which is that of beauty. I
believe that beauty is what Dave Chappelle was talking about touching.

Chappelle describes his child seeing the beauty "of this thing I do"
which is his work as a comedian, specifically as a standup comedian. He
acknowledges that his work is art and that it is in and of itself not
necessarily beautiful. The beauty lies in the act of sharing that art.
It's the incredible binding of energy between a performer and an an
audience. It exists in a moment yet it lingers in our blood.

Think of the performances you've seen and how you felt for hours or days
after. There's a reason people say things like, "That concert changed my
life." I believe that it literally changes our lives. We are different
people when we walk through the exits. Our molecules have shifted. We
are bereft of these kinds of experiences in our current lockdown world.
It is a great loss.

That's a lot of musing about art and how we experience it on a path to
beauty. It's heady stuff. What hit me about the comment and the moment
in the show is what it meant to me as a parent.

Chappelle was tapping into that primordial motivation parents have to
see our children find beauty. We talk about the biological imperative to
perpetuate our DNA and, sure, that's the mechanism of procreation. What
that ignores is how we get from our birth to the birth of our
grandchildren. That arc is full of infinite narratives where we as
parents fail or succeed at our children's ability to reach out and touch
beauty.

In many ways, parents aren't fully responsible. There's a great big
world out there which isn't always so focused on beauty. This is another
topic that Chappelle touches on when he describes the event where his
son tried standup. It was a community event to raise money. That
community bit is important. Chappelle has put himself and his child into
the midst of a community of others also reaching to touch beauty whether
it be their performance or the act of helping others.

We tend to wallow in the image of the lonely artist creating in fit of
near madness. What it could look like instead is a community where each
of us is supported in our attempts to touch beauty. Maybe it's in
cooking the perfect pizza. Maybe it's in writing a book that your
neighbors all read. Maybe it's in buying something made by that woman at
the Christmas Market.

Reaching out to touch beauty is a vulnerable thing to do. Imagine it
physically. With our arm and our gaze raised, our balance is uncentered,
our soft belly meat exposed, our intentions laid bare. We are sharing of
ourselves in the hopes that our community will treat the beauty inside
with love and respect.

What beauty have you reach out and touched today?

[Watch me try
standup](https://drive.google.com/file/d/1fkDPOzW__XS95bUf-fdgWxBpyA-15Q28uQ/view?usp=sharing){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

This newsletter is nearly up to 100 subscribers. I've been working on a
small special thing to share with those of you have been here this far.
It will require your mailing address. I promise to not share this info
or do anything with it other than send you something one time.

[Get mail](https://forms.gle/Uja4qMXtNcs6wfnp7){.button .primary}

<div>

------------------------------------------------------------------------

</div>

## Dave and David

::: {.captioned-image-container}
![Dave and David in a still from their
interview.](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/b5a96e35-42ec-444c-9ee8-cc7cf23bcc61_1200x675.jpeg)
:::

David Letterman's interview series on Netflix has lots of wonderful
moments and some that are fluffier than I might like to see Letterman
doing. Still, the recently released episode with Dave Chappelle is solid
gold.

[Watch Dave and David](https://www.netflix.com/watch/81228413){.button
.primary}

## Rebel Music

::: {.captioned-image-container}
![King Tubby working magic in the
studio](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/db97aabb-cf55-4c4c-83de-6e7b2396e48d_750x536.jpeg)
:::

I don't know what to make of a white kid from south Georgia who loves
reggae and dub music. I battle with the notion of cultural
appropriation. I grapple with the notion that a lot of the music is
directly indicting behaviors of my ancestors (and me if we're honest). I
have no right to this music. I bristle at the scenes of my youth where
reggae was just music to listen to at the beach or get high to. I still
remain transfixed by the rhythms, the energy, and the rebellion. I feel
connection to the studio experimentation of the dub wizards. The music
connects at a level I simply can't process. It's what great music does.
I can't wait to see Steve McQueen's film, "Lover's Rock," later this
year.

[See McQueens\' intro to the New
Review](https://www.theguardian.com/culture/2020/nov/15/steve-mcqueen-its-rebel-music-that-moves-me-small-axe-lovers-rock){.button
.primary}

## Gift Idea for Book Nerds

::: {.captioned-image-container}
![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/bb83c967-dc6f-4116-a886-3a678c5eb8f2_847x590.png)
:::

The New York Review of Books is a wonderful magazine that is mostly
focused on long-form book reviews. I've shared many articles from it in
this newsletter. Around the Christmas holidays each year, they do a sale
of themed boxes of their books which are just an incredible deal. I
recommend checking them out even if you just want to buy a box for
yourself. If you get really jazzed, you can subscribe to the annual plan
which will surprise you with a random book each month. If you have a
book lover in your family but can never pick out the right book, this is
a great option.

[Get a box of
books](https://www.nyrb.com/collections/nyrb-book-boxes){.button
.primary}

## Ancient Horses, Ancient Challenges

::: {.captioned-image-container}
![Yawning or
warning?](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/38397235-498e-4548-bb94-0d0b45b5ceae_900x900.jpeg)
:::

My wife is a passionate horse person. She trains horses, teaches
lessons, and helps horses stay healthy and happy. It is amazing to watch
her coach not just the horses but the people, too. I often ask her about
the history of horsemanship so we were excited to read about this new
research that shows a more specific timeline of horsemanship in China.
Specifically, the research bridges the gap between when horses were
mostly pulling things like wagons, carts, and plows to when humans
started riding them. One wonders at the pressures that moved humans to
try this decidedly dangerous activity.

[Read about ancient horses](https://nyti.ms/2K8mhwx){.button .primary}

## The Air We Breathe

::: {.captioned-image-container}
![Provided by
NASA](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/ccee2421-9eb3-454a-adde-937c7b68f8b1_438x294.jpeg)
:::

I now live along the Ohio River. One of the most polluted towns in the
US is about an hour away on the other side of the river. There are
industrial plants all along the river. I worry about what that might
mean for long-term health. Increasingly, freedom from these sorts of
worries is class and race adjacent. We all know that landfills, sewage
plants, and industrial plants have often been located in poorer and
mostly non-white areas. Boyce Upholt's essay, "The Meaning of Air"
examines these issues and how they intersect the increasing notion of
protecting wilderness which is often engaged in by well-to-do whites
with little input from those folks already dealing with poisons in
paradise.

[Read Boyce Upholt\'s \"The Meaning of
Air\"](https://emergencemagazine.org/story/the-meaning-of-air/){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

Have a great week. Let me know if you touch any beauty.

Your guide,

Alex

::: {.captioned-image-container}
![Hamlet was
cold](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/abe18cef-83e4-4782-97f2-427e0b23f173_2448x1829.jpeg)
:::
