I've always wanted to be able to write an email, tweet, letter, or blog
post that started like this: "I've been quiet these last few weeks
because I recently got a contract for \[this thing I created\] and now I
can finally share the good news." But, this is not that email.

Instead, this email comes to you after a few missed weeks due to mental
health issues and insane travel schedules. I was in Fort Lauderdale
Beach for a week for work and just returned from my 25th high school
reunion in Rome, GA. Both of those trips required lots of socializing
and face time. I can do it. I might even be good at it. But, the toll it
takes is tremendous. It leaves no space for whatever it is that lets me
write this newsletter or make progress on my other pursuits.

At the reunion, I encountered an interesting phenomenon. Many people
started a conversation with some version of, "Well, you already know
what's up with me from Facebook." In my case, that's not true, because I
don't have a Facebook account. But, I'm standing there in a group so I
smile and nod. Then, no one talks. We all look to the high corners of
the room. I'm overly forward so I say, "I don't know anything about what
you've been doing. Tell me."

The conversation would begin. Within five minutes, there was laughter.
There was commiserating. There was agreement and disagreement. In short,
it was connection. The real kind that simply can't be replaced by
Facebook, Twitter, an email, or maybe even a letter. I've been connected
to these humans for twenty five years. It only takes a single vibration
to bring us to a shared place. We human beings need to be in a physical
space with other humans to find out what makes us all tick.

Humans have rituals and traditions that reinforce this. Religion thrives
on it. The vast and varied food cultures of this planet exist to nurture
it. Yet, we build our houses on larger pieces of land which we forbid
our neighbors to enter. We build fences, or walls. We wear earbuds to
insulate us. We have all our needs and desires shipped to us in brown
trucks. We continually isolate.

We are left lonely in a world of constant connection. It is the deepest
paradox of our humanity and maybe the one that causes us the most harm.

Now, let's share in a bit of the world.

<div>

------------------------------------------------------------------------

</div>

## There Is So Much More

::: {#youtube2-j_iFqz87TXQ .youtube-wrap attrs="{\"videoId\":\"j_iFqz87TXQ\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=j_iFqz87TXQ), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

The song "There Is So Much More" by Brett Dennen has been around for a
while, 2006 to be exact. When I first heard it, I was recovering from a
stint in rehab and fighting all the demons that you meet there. It's
over-simplified to say that the song put my thoughts to words. Instead,
it captured a part of me that maybe was afraid to be voiced. A lot of
that time is fuzzy in my mind. It's the blessing of forgetting. This
song has stood the test of time and still provides solace in the sense
that it makes me feel less alone.

[Listen to \"There Is So Much
More\"](https://www.youtube.com/watch?v=j_iFqz87TXQ){.button .primary}

## The Challenge of Difference

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/c91364b8-cfb6-4b81-a3e7-225d7ec8f22c_444x255.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fc91364b8-cfb6-4b81-a3e7-225d7ec8f22c_444x255.jpeg){.image-link
.image2 .image2-255-444}

###### By Charles Frederick Holder - Along the Florida Reef, [Public Domain](https://commons.wikimedia.org/w/index.php?curid=42388704)

We all tend to give the animals around us human motivations. Youtube is
full of videos of dogs "saving" people or other animals, cats frowning,
bears waving kindly, dolphins playing games, and more. We want there to
be some connection between us and the natural world that is fully
understood. How do we then make sense of the octopus? It seems
intelligent. It seems to remember us. It seems to like some of us more
than others. Yet, anatomically, it is so radically different from us,
from a dog, even from an earthworm, that it defies any explanation we've
been able to come up with. I find some comfort in the idea that there's
a part of nature that seems unknowable, some portion of our Earth and
its inhabitants that we humans maybe can't and won't lord dominion over.

[Read about the alien
octopus](https://lithub.com/the-octopus-an-alien-among-us/){.button
.primary}

## Best of the Century

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/432ccd8b-26b6-4428-8194-f16b566d058b_3619x2423.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F432ccd8b-26b6-4428-8194-f16b566d058b_3619x2423.jpeg){.image-link
.image2 .image2-736-1100}

###### By Jess Mann, [CC BY-SA 4.0](https://commons.wikimedia.org/w/index.php?curid=44967783)

Best of lists are the lifeblood of the Internet. I remember sometime
around 1995, one of the longest threads I ever saw on Usenet was titled,
"The Definitive List of the Best Novels." As you might imagine, not one
of the replies was congratulating the author on how well they'd whittled
down all of human storytelling to a short list. And yet, we continue to
try. The Guardian weighed in recently with their "100 Best Books of the
21st Century" list. Using the word century sounds amazing but we are
really only talking about 18 years from 2001 to the present. I won't
spoil any of the picks but it is a thought-provoking list.

[Count how many you\'ve
read](https://www.theguardian.com/books/2019/sep/21/best-books-of-the-21st-century){.button
.primary}

## Boiling a Frog

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/e30b451e-246f-4f52-b94d-cdcc51980f87_4450x3508.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fe30b451e-246f-4f52-b94d-cdcc51980f87_4450x3508.jpeg){.image-link
.image2 .image2-867-1100}

###### By Unknown - [Konzernarchiv der Georg Fischer AG, Schaffhausen](https://archives.georgfischer.com/objects/15437), [CC BY-SA 4.0](https://commons.wikimedia.org/w/index.php?curid=59722718)

The world is full of ruminations about work. Look back at the Industrial
Revolution and the art that was generated to both celebrate its onrush
of capital and seeming progress and to denigrate its lack of concern for
the humans that worked within the machine. It feels like we may be at a
similar point of inflection around AI, Machine Learning, and Automation.
This New Yorker piece by Anna Wiener isn't really about that. Instead,
it describes the human cost of the run up to that shift in our culture.
It lays out, at human level, what this bonanza of money and optimism
around the "tech industry" has wrought.

[Read about Anna Wiener\'s tech
work](https://www.newyorker.com/magazine/2019/09/30/four-years-in-startups){.button
.primary}

## The Weight of Grief

::: {#youtube2-ULlPZidWThw .youtube-wrap attrs="{\"videoId\":\"ULlPZidWThw\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=ULlPZidWThw), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

I recently watched the film,
"[Evelyn](https://www.netflix.com/title/80216928)," which is a
documentary by acclaimed war photographer Orlando von Einsiedel about
the suicide of his younger brother. It shows the family, primarily the
surviving siblings going for a long walk to finally talk about what
happened to their brother, Evelyn. It is heart-wrenchingly sad and I
recommend you be prepared should you watch. But, you should watch it.
It's a stunning film about family, grief, and the power of human
connection to elevate us to a higher self. The above video is a song
from the film written and performed by Orlando's sister Gwennie who also
appears in the film.

[Watch the trailer for
\"Evelyn\"](https://www.youtube.com/watch?v=n_CPWU1REaM){.button
.primary}

## The Bath as Portal

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/0881ee35-3d09-4953-a571-15521cf54025_5304x3600.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F0881ee35-3d09-4953-a571-15521cf54025_5304x3600.jpeg){.image-link
.image2 .image2-747-1100}

###### By Diego Delso, [CC BY-SA 4.0](https://commons.wikimedia.org/w/index.php?curid=35323216)

I'm not a bath taker. Never have been. When I was a kid, I used to fill
the bath up and sit on the toilet for the approximate time a bath would
take. I'd wet my hair and then emerge from the bathroom proclaiming how
nice my bath was. The water would flow down the drain hopefully taking
the sin of my lie with it. But, some people in the world demand a bath.
They luxuriate in the time it takes, the womb-like embrace, and the
silence that it often engenders. In this article, Mikaella Clements
takes a look at how baths have been used in recent literature.

[Soak in the literature of
baths](https://www.the-tls.co.uk/articles/public/the-slow-clean-baths/){.button
.primary}

## Hello? Are you there?

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/16f15efa-6e26-478d-9578-8e3ea378253d_2816x2112.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F16f15efa-6e26-478d-9578-8e3ea378253d_2816x2112.jpeg){.image-link
.image2 .image2-825-1100}

###### By Robert Stinnett from Boonville, MO, USA - [Operator, Can You Help Me Place this Call?, CC BY 2.0](https://commons.wikimedia.org/w/index.php?curid=69141389)

I've never enjoyed talking on the telephone. It seems like a lot of
work. The current generation probably doesn't even know that their
phones can call people. Everything is text-based. I'm OK with this. I
type fairly well and express myself with the written word in a way I'm
comfortable with. This Atlantic article by Amanda Mull makes the case
that we should start talking on the phone again. In the world of
communications, they talk about low-context interactions and
high-context. Face-to-face, in the same room, communication is the
highest of context. Some distance after that is a video call. After
that, a voice call. Falling so far behind that its context would take
decades to reach us through time and space is asynchronous text
communication, like Slack or even texting. So, I get the idea that we
might all benefit from a little more context and the healthy
confrontation it creates. That's how understanding is achieved. Don't
think about it too hard. Call your mom.

[Read about the joy of phone
calls](https://www.theatlantic.com/health/archive/2019/09/ring-ring-ring/598129/){.button
.primary}

## R.E.M. Song of the Week

::: {#youtube2-eN1f4AFgiAc .youtube-wrap attrs="{\"videoId\":\"eN1f4AFgiAc\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=eN1f4AFgiAc), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Sure, I could play you Michael Stipe's new solo single. But, that's not
an R.E.M. song, so I'll refrain. Instead, let's look at the band's last
album, *Collapse Into Now.* The song, "Oh My Heart," collapses a lot of
R.E.M.'s strengths into a single song. There's the acoustic-based
instrumentation from *Out of Time*. There's the political, timely lyrics
of latter Stipe (this song being about Katrina's havoc on New Orleans).
There's the arpeggiated guitar work of classic Peter Buck. There's the
perfect backup vocals by Mike Mills. The only thing missing might be
that post-punk edge from the earliest records. Maybe there's a maturity
in that.

[Listen to \"Oh My
Heart\"](https://www.youtube.com/watch?v=eN1f4AFgiAc){.button .primary}

<div>

------------------------------------------------------------------------

</div>

I appreciate your coming back to this with me. I'm happy to share this
with you.

Your guide,

Alex\

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/e6c0cc95-261d-4162-b7cc-1465fb5d6c18_4032x3024.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fe6c0cc95-261d-4162-b7cc-1465fb5d6c18_4032x3024.jpeg){.image-link
.image2 .image2-825-1100}

###### A Banyan Tree - Hugh Taylor Burch State Park - Ft. Lauderdale Beach, FL
