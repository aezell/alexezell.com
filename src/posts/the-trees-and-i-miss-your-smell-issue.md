# I miss your smell

#### A weekly newsletter from Alex Ezell about that which may discover us

\
Spring is in the air though it looks like we\'ll have snow in my area
this weekend. Like all the Springs before, this one reminds me of
redemption, rebirth, and the ability of humans to overcome.\
\
**Is It Irish Yet?**\
Yesterday was St. Patrick\'s Day, so let\'s start off with a
[Celtic-influenced song from Corner Boy called Moming,
Morning](https://www.youtube.com/watch?v=T8VJ6hFfQ8A). I appreciate the
American folk influence paired with the Celtic arrangement and
pop-styled lyrics. I heard this song on Larry Kirwan\'s [\"Celtic
Crush\"](https://www.facebook.com/Celtic-Crush-346561287150/) show on
SiriusXM. Larry is one of the founding members of Black 47 who also
combined various influences into an angrier Celtic stew on songs like
[\"Funky Céili\"](https://open.spotify.com/track/0AhVtK03RPWGalbHJ9J4ds)
and [\"Danny Boy\"](https://www.youtube.com/watch?v=WDNPKtiTOho) with
Kirwan bringing distinctly NY punk influences into the band.\
\
**There Is No Peace With Cancer**\
I don\'t know the statistics but I bet everyone has had a brush with
cancer with themselves, family, or friends. Cancer is a pernicious
disease. [R.A. Villanueva\'s poem, \"When
Doves,\"](https://www.poets.org/poetsorg/poem/when-doves) is stunning in
the allusions it makes to medieval frescoes, Prince songs, funeral
rites, and the cosmic unseen. I\'m reminded as well of [Jason Isbell\'s
gut-punch of a song,
\"Elephant,\"](https://www.youtube.com/watch?v=LHJhyrrUTgc) which tells
a story of cancer that never shies away from reality.\
\
**Escape to the North**\
Humanity\'s ability to overcome occasionally seems unlimited. A [new
article in the NY Times Magazine about the seemingly inept,
ill-prepared, and ill-met adventurers of the Plaisted Polar
Expedition](http://www.nytimes.com/2016/03/20/magazine/an-insurance-salesman-and-a-doctor-walk-into-a-bar-and-end-up-at-the-north-pole.html)
shows us that something about humans make us able to overcome even our
self-imposed limitations. These men made history when seemingly all was
arrayed against them.\
\
**Store All The Cat Pictures**\
Dropbox is a huge part of many people\'s lives storing their family
photos, important financial documents, and probably millions of cat
pictures. The engineering feat involved in [Dropbox moving off of
Amazon\'s AWS
infrastructure](http://www.wired.com/2016/03/epic-story-dropboxs-exodus-amazon-cloud-empire)
is, in my opinion, understated in this article. Some part of me does
struggle with the amount of human intelligence and effort poured into
doing something just a bit better than has been done before. It seems
like that energy might be better focused on true innovation.\
\
**The Lies We Tell**\
I recently finished [David Brin\'s
novel](https://www.goodreads.com/book/show/2190668.The_Postman) *[The
Postman](https://www.goodreads.com/book/show/2190668.The_Postman)* which
was made into a terrible Kevin Costner movie in the early 90s. The book
wants to be about the nature of good and evil in humanity. It wants you
to think about whether we are inherently good or inherently bad and what
maybe flips that switch in some of us. What\'s more interesting in the
book is its playing with the nature of Truth. While the main character
muses a bit on the topic, Brin seems content to wallow about in
political meanderings instead of truly tackling the ethics of lies that
do good. [Sam Harris\'s
book](https://www.goodreads.com/book/show/12379144-lying)
*[Lying](https://www.goodreads.com/book/show/12379144-lying)* wants us
to eschew white lies to make our lives better. Brin\'s book has the
opportunity to make us confront the giant lie that inspires altruism and
selflessness. Those are the lies we should all tackle with more fervor.\
\
**Adios and Vaya Con Dios**\
I\'ve been fired. I\'ve been laid off. I\'ve quit. I\'ve fired people.
When I read [Zach Holman\'s recent piece on Firing
People](https://zachholman.com/talk/firing-people), I recognized several
mistakes I made in all the situations I mention above. Though his piece,
by his own admission, smacks of the privilege of being an accomplished
technical worker in Silicon Valley, I was struck by his descriptions of
the roles of each participant in this situation. There seems to be a way
to have compassion and yet accomplish the goals of the business but
it\'s a perilously thin ridge to traverse.\
\
**Two Songs with Bass**\
I\'ve always loved [Morphine\'s album \"Cure for
Pain\"](https://open.spotify.com/album/0FocpOyE7RnaNsiaEhSbp6) and it
was even more precious when I read they labeled their music as \"low
rock.\" With a sound anchored by the two-string bass of Sandman, [their
song \"I\'m Free Now\"](https://www.youtube.com/watch?v=kM043nMsVFg)
might have the most self-effacing lyrics ever used in such a sax-heavy
tune. My buddy, Matt, tells me that Flea from Red Hot Chili Peppers is
playing sixteenth notes in the bass line for [their song \"Parallel
Universe\"](https://open.spotify.com/track/1Se0r96r0gnqg67kJPmESc) but
all I can think about is how tired his hands must be after four and half
minutes of it. I\'m not typically an RHCP listener but this song stops
me in my tracks every time.\
\
**Beauty Reigns**\
[k.d. lang\'s
\"Ingenue\"](https://open.spotify.com/album/1cHVBJY0QDg1ss4jsNXOvS) came
out in 1992 when I was a sophomore in high school. I remember being at
home that spring break and my Dad putting the CD into the stereo and
playing [\"Constant
Craving.\"](https://www.youtube.com/watch?v=oXqPjx94YMg) It made no
impression except that I knew it was on the radio a lot. Over the years,
her more western-influenced work would cross my path but still I thought
mostly that she had a fascinating voice but the tunes weren\'t for me.
Years later, I come across this new project combining Neko Case, k.d.
lang, and Laura Veirs as case/lang/veirs and their [newly-released song
\"Atomic
Number\"](https://open.spotify.com/track/36BvxxHlY6JPjmvbp43CMc) which
I\'ve listened to at least 20 times in the last few days. The vocal
combinations and the string arrangements underscored by that wandering
guitar line reel my right in.\
\
**R.E.M. Song of the Week**\
Off what might be the most ignored R.E.M. album, \"Reveal,\" [this
week\'s song, \"I\'ve Been
High\"](https://open.spotify.com/track/44gAfS0UvdMDVzXOLLGm5m) is a rich
evocation of want and loss.\
\
\--\
\
Encourage friends and family to sign up so we can share the love. Just
forward the email to them and see if they dig it. Have a great weekend!\
\
You can access an archive of all these newsletters here:
[http://tinyletter.com/imissyoursmell/archive​](http://tinyletter.com/imissyoursmell/archive)
