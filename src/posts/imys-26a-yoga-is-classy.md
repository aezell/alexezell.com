::: {.captioned-image-container}
![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/f75d662e-104a-48ca-a53d-318fdffb8331_860x524.jpeg)
:::

If it's alright with y'all, I thought I'd share a little story:

I'm a yoga newbie. Today was my third visit to the yoga studio. I'm a
270 pound man wearing a torn Manchester Orchestra t-shirt and too short
running shorts. I start pouring sweat 10 minutes into this business and
I'm a complete mess after 30 minutes. It feels good though.

There was this petite, older lady next me. She's always quite prim and
modest and she is incredibly strong at yoga. We were doing some thing
called the "[tree pose](https://en.wikipedia.org/wiki/Vriksasana)" and
she fell into me having lost her balance. It was fine. She probably
weighs as much as my leg. She was solidly Midwest embarrassed.

About 10 minutes later we were in the middle of a series of tortures
called a [vinyasa](https://en.wikipedia.org/wiki/Viny%C4%81sa). As she
exhaled into [cobra pose](https://en.wikipedia.org/wiki/Bhujangasana),
she farted very loudly. I'm new to this world of yoga so I look over not
knowing how to react. Her eyes were as big as saucers. She was frozen
but I caught her eye. I just shrugged with my face and got back into the
[crow pose](https://en.wikipedia.org/wiki/Bakasana) (or tried to).

At the end of class, she came up to me to apologize for the dozenth
time. She said, "I hope I didn't scare you away." I told her, "Ma'am, I
grew up with three brothers. Went to boarding school and lived in a dorm
with 40 other teenage boys. Lived three years in a fraternity house.
There's not a person alive who can shock me with their flatulence."

She laughed so hard that she was doubled over. She looked up in horror.
\"Now, I\'ve peed my pants!\" She shuffled out to her car waving over
her shoulder, \"See ya Wednesday!\" Yoga is classy.

Be open to new experiences.

Alex
