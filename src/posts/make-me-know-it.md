I know I said IMYS was on hiatus. I can't stop writing.

<div>

------------------------------------------------------------------------

</div>

::: {#youtube2-c6cLPDVIp_A .youtube-wrap attrs="{\"videoId\":\"c6cLPDVIp_A\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=c6cLPDVIp_A), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

The title of this post comes from a saying that my grandmother Ruth had
(and the Elvis song above). She was a no-bullshit kind of person. When
faced with a salesman or anyone promising her something, she would say,
"Make me know it." It meant she wanted to see it happen or hear the
skinny. No more talk or you can walk. It's not clear if she took it from
the Elvis song or if she knew the phrase before Elvis recorded Otis
Blackwell's song.

As a kid, my Dad promised that he would get all the leaves raked out of
the yard in exchange for a quarter to go to the movies on Saturday. He
kept asking if he could get the quarter first to catch the morning
serials and then rake the yard later. Grandma Ezell would only reply,
"Make me know it." He raked the yard and caught the afternoon matinee.
She was not one to cave.

She used the phrase in a different way when a salesman came to the
truckstop restaurant that she ran and tried to sell her a machine to
help her make pies faster and easier. She always made the pie crust by
hand. This machine, in the true sign of the space age 1950s, was going
to free her from the burdens of that work. You know what she said to the
man. He proceeded to make a pie with his mixer as she made one by hand.
They served both to the customers and no one batted an eye. It was just
as good. She bought two and started selling pies to the other
restaurants in town. She had come to believe.

Recently, I have been trying to start a meditation practice. It takes
work to get to a place where it's bearable. I'm certainly not alone in
that. Quieting the mind and letting time flow around you is difficult
for any of us. Maybe especially so with the stress of the last year. To
help, I've been using a 30 day introductory course on Calm. The app is
extremely well done and free if you have an AMEX card. No, this
newsletter is not sponsored (But I ain't above that!).

For some reason in a recent session, this saying, "Make me know it,"
would not leave my mind. I was going through the various methods to
break free of an unbidden thought like equanimity, pop out of your
thoughts, noticing is not attention, etc. Nothing would drive this
mantra from my mind. Eventually, the meditation session ended and I had
found some clarity but this metal spike of a phrase sat right in the
middle of it.

Later in the day, I came across a video of someone selling a ShopSmith
woodworking system. They are pretty sweet if you ask me. (Again, I ain't
above shilling for something I can believe in.) The salesman in the
video was talking a mile a minute and using all the old salesman tricks
that my Dad used to employ. He hawked auto parts all around the
southeast for 25 years. They both had full confidence in their products
and knew that you would too if only they could make you know it.

As I watched that video, it all came together for me. The unintended
meditation mantra. My Dad's salesmanship. A ShopSmith machine (sometimes
the cosmos has a sense of humor). All these threads were braided around
an idea about how we learn. How we come to know something.

One of the definitions of the word "know" is "to be assured, confident."
That was the feeling that my meditation was driving home. Be assured and
confident that you are doing the right thing that needs to be done in
that moment. That meditation was the precise right action.

My Dad built confidence in his customers with things like
demonstrations, guarantees, an iron handshake, and an immovable honesty.
My Grandma Ezell wanted that assurance and that confidence because her
reputation was on the line. She wanted to believe what she was being
told so that truth could be passed on to the patrons of her restaurant.

So, the phrase, "Make me know it," is in some parts skepticism but it's
also about asking, "How can I be sure?" from a place of weakness. We are
asking others to help us feel confident and sure. We are admitting
trepidation and wariness. We are inviting sharing, education, proof, and
possibility into our lives.

That openness is paradoxical to the way the phrase sounds. It feels
closed off and threatening. Something in that meditation as the
repetition bounced around my brain unlocked this nuance about my
grandmother and my father that I'd never considered. They were certainly
people of their time and place but in their own way they had a curiosity
about the world that was often hidden behind humor and volume.

My grandmother, Ila Ruth (Folsom) Ezell, died 40 years ago last month
from Creutzfeldt-Jakob Disease. She was 59 years old. I was not yet 5. I
think this saying of hers says a lot more about her than I'd ever
considered. Her knowledge of the world may have been narrowed by
circumstances of birth and station. However, she found a way to use her
strength to force more truth into the world. It's often in short supply.

<div>

------------------------------------------------------------------------

</div>

Hope everyone is easing in to the Christmas Week.

Alex

::: {.captioned-image-container}
![Chicago, 2008. By Alex
Ezell](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/323807c1-53bc-430c-93eb-6e36aa7274c7_1934x1484.jpeg)
:::
