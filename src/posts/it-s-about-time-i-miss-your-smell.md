# I miss your smell

#### A weekly newsletter from Alex Ezell for us to share

\
Spring has broken through in my part of the world. Green sprouts and
gorgeous sunshine greet most afternoons. The natural world beckons me
out of the gloom my winter home as become. Thanks for joining me.\
\
**Nature\'s Unseen Hand**\
The NY Times Magazine consistently puts out fascinating work and
[\"Secrets of the Wave Pilots\" by Kim
Tingley](http://www.nytimes.com/2016/03/20/magazine/the-secrets-of-the-wave-pilots.html?_r=0)
is no exception as it details the work being done to save an ancient
navigational method while at the same time attempting to understand it.
There\'s a fine line being walked here for the scientists involved as
they risk explaining away a deep spiritual connection between the
Marshall Islanders and the sea. There are some incredibly understated
moments of American shame in this story when the author notes the forced
relocation and nuclear tests our government undertook in these islands.
It\'s amazing that any of their culture exists today.\
\
**There\'s Always Gonna Be a Lawyer**\
With its nearly Rick Springfield guitar riff and strong groove, the
political lyrics of [David Lindley\'s \"Talk to the
Lawyer\"](https://www.youtube.com/watch?v=1mNgU7fj5l4) are seemingly out
of place. Lindley is an accomplished musician who can basically play
anything with strings. He\'s best-known for his work as a band member
for artists such as Warren Zevon, Jackson Browne, and Bruce Springsteen.
His solo albums are exceedingly eclectic as the [panoply of styles on
his \"El Rayo-X\" will
attest](https://open.spotify.com/album/5YBOfNA4D8hUldUGivKZd7).\
\
**Spring Blooms**\
Sturgill Simpson blew a lot of people away with his album [\"Metamodern
Sounds in Country
Music\"](https://open.spotify.com/album/4makbOuLd5SUdyHMaNM1Ag) and
he\'s set to release a new album, [\"A Sailor\'s Guide to
Earth.\"](http://sturgillsimpson.com/ASGTE/?ref=) It appears he\'s
starting a trend of offbeat covers on his albums. Previously, [he
covered When in Rome\'s \"The
Promise\"](https://www.youtube.com/watch?v=-eWJmN8D820) and it appears
this new album will feature [a cover of Nirvana\'s \"In
Bloom.\"](https://www.youtube.com/watch?v=NpDYfkymaSE) That video,
[directed by Matt Mahurin](https://en.wikipedia.org/wiki/Matt_Mahurin),
is incredibly cool and odd while the cover is nearly unrecognizable.
Somehow, he makes it work.\
\
**The Never Ending Story**\
One of my fascinations with Shakespeare is that there\'s an ongoing
fascination with Shakespeare. That is, is celebrity has survived for
over 400 years though there were clearly times where he wasn\'t as well
known. Still, scholarship from various perspectives continue and we now
have [this short piece that claims to have identified a likely model for
the Othello
character](http://www.theguardian.com/culture/2016/mar/19/moroccan-ambassador-london-1600-real-othello-shakespeare).
Many of the dates and attributes line up. Could it be a great
coincidence? Of course. But isn\'t it lovely to think of Shakespeare
including someone who\'s seemingly tabloid fodder in a play that cuts
through human weakness so deftly.\
\
**There Is Art Over There**\
Despite the money-vortex that is modern pop country, there is still
great work being done in country music. Influences are being pulled in
from across the musical spectrum. Two newer artists are great examples
of what\'s simmering just below the surface in Nashville. Aaron Lee
Tasjan works in the grittier folk edge of country music with a wry
singer-songwriter style on [his new album, \"In the
Blazes.\"](https://open.spotify.com/album/3mWN0n4F5XETSqjuLqMA0Q)\
\
Mason Jennings also lives in that world where pop and folk and country
cross. I\'d argue he\'s a little less country-influenced that Tasjan but
there\'s a core of attractive melodies and plain-spoken, heartfelt
lyrics that ties his work together on [his new album, \"Wild Dark
Metal](https://open.spotify.com/album/5n6PNJcGeA6Ezbb0QIxbu3)\",
featuring a lead-off track about [the hip-hop group Death
Grips](https://open.spotify.com/track/50rm7j7JKHMeM2PpoELhk4).\
\
**This Art is Mine**\
I\'m nearly done reading a book that surveys the European explorers,
geographers, adventurers, archaeologists, and thieves that ravaged
Central Asia late in the 19th century. [Peter Hopkirk\'s \"Foreign
Devils on the Silk
Road\"](https://www.goodreads.com/book/show/149749.Foreign_Devils_on_the_Silk_Road)
is a surprisingly intriguing account of the travails of British, German,
Russian, and French \"historians\" to find and collect the \"lost\" art
that survived in the Taklamakan and Gobi Deserts along the Silk Road in
what became far western China. These men found art and writing that
showed a dazzling mixture of early Christian, Buddhist, Manichaean, and
even Classical influences. Given the commerce and people that flowed
along this route for hundreds of years, the various levels of synthesis
and evolution of all these influences became its own culture.\
\
These Western incursions into the area were some of the last gasps of
empire as Britain was losing its hold on India and the French were
further and further removed from southeast Asia. China was gathering
itself and WWI loomed on the horizon. These are tales of the last
\"gentleman explorers\" and while the theft of this art incurred
enduring Chinese wrath, it\'s possible the art would have been lost
forever if not for these expeditions. Hopkirk combines adventure, art
history, religious history, politics, and humanity while preserving a
moment when cultures didn\'t clash but wrestled for validity. If you\'re
interested in other aspects of the Silk Road or that unclaimed zone
between Persia, Russia, and China, let me know and I can send you a list
of other books on the topic.\
\
**Three Things I Need**\
I have two friends who\'ve impressed on me the need to get to know
Little Feat. The band led by Lowell George has long lived in that
strange world between jam bands, the New Orleans sound, southern rock,
and even some California folk-rock. It was never a thing I could wrap my
hand around but I found [a fun entry point with their song,
\"Willin\'.\"](https://www.youtube.com/watch?v=txX-kPn3h6s)\
\
**A Story Oft-Told**\
This week have had multiple conversations where [\"Under Tiberius\" by
Nick
Tosches](https://www.goodreads.com/book/show/23731889-under-tiberius)
has been germane. The book tells a sordid tale of Jesus and the Roman
\"speech writer\" that stage managed him to his eventual crucifixion.
It\'s sort of a more punk rock version of [Phillip Pullman\'s \"The Good
Man Jesus and the Scoundrel
Christ.\"](https://www.goodreads.com/book/show/7645932-the-good-man-jesus-and-the-scoundrel-christ)
The majority of the book reads as a patently sacrilegious sneer at the
story of Jesus. Near the end, there\'s a shift that takes place that
asks penetrating questions about how important it is that Jesus was or
was not what we are told he was. It begs us to answer whether we should
follow his teachings because they result in a greater happiness or
simply because he was purportedly the son of God. Heady questions to be
sure and the book takes a dangerous path to ask them. Be warned, this
book is likely deeply offensive to those who believe but I find a
fascinating question at its center that doesn\'t, to my mind, diminish
anyone\'s faith.\
\
**Two Gloriously Goofy Things**\
I\'m not sure what to make of [Disturbed covering Simon and Garfunkel\'s
\"Sound of Silence.\"](https://www.youtube.com/watch?v=u9Dg-g7t2l4) It
seems a bit like a joke gone on a bit too long. And in the annals of
bombastic, self-seriousness, [let\'s check out \"Counting Out
Time\"](https://www.youtube.com/watch?v=YXUmKw4pFdc) from Genesis\'s
double-disc concept album, \"The Lamb Lies Down on Broadway,\" which was
Peter Gabriel\'s last with the band.\
\
**Beauty Reigns**\
Combine an Everly Brothers tune with the unique voice of Langhorne Slim
and the startlingly beautiful voice of Jill Andrews and the brother\'s
harmonies might pale in comparison. There are a few live or sparsely
recorded version of this online but the duo officially released [this
strongly produced version of Sea of
Love](https://open.spotify.com/track/3ASn2iE7F4HvA8XyRbJ4tS).\
\
**R.E.M. Song of the Week**\
A bit of a two-fer this week. There was a hidden track on 1998\'s \"Up\"
called \"I\'m Not Over You\" which was a simple, and seemingly highly
personal, song featuring Michael signing and playing an acoustic guitar
alone. It\'s one of the little oddities about R.E.M. that were always so
endearing to me. It\'s not a particularly great song and Stipe is far
from an accomplished guitarity but that they included it on the album
and then played it live a good bit makes me smile. [Here\'s a video of
them doing it Rockpalast and letting it lead into another favorite
\"Walk Unafraid\" which is also from the \"Up\"
album](https://www.youtube.com/watch?v=nvrC8ab9OEs).\
\
\--\
\
You can access an archive of all these newsletters here:
[http://tinyletter.com/imissyoursmell/archive​](http://tinyletter.com/imissyoursmell/archive)
