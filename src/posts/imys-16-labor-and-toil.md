Welcome to the rodeo!

This is [Labor Day](https://en.wikipedia.org/wiki/Labor_Day) Weekend.
I'll offer that instead of shopping at some sale for which a retail
employee is barely making a living wage to supervise as you purchase
goods likely made overseas from a barely legal system of indentured
servitude like employment, just take a step outside, breathe in deeply,
and consider what work means to you. Is it a means to an end? Is it a
vocation? Is it a waypoint towards another job? How does it sustain you?
How does it injure you?

We spend most of our lives working or sleeping. I think we could all
benefit from thinking about the work we engage in a bit more deeply.

*Editor's Note: I've made the main links of each section more obvious in
big orange buttons. Let me know if this is better or worse. Other links
in the text are additional information.*

<div>

------------------------------------------------------------------------

</div>

## The Ears We Deserve

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/cb43447e-bdc7-480a-b9c3-4955b105db61_830x1218.jpeg)](https://www.nytimes.com/2019/08/20/magazine/neil-young-streaming-music.html){.image-link
.image2 .image2-1218-830}

*Neil Young By Stoned59 - originally posted to Flickr as [Neil Young,
Heart of Gold, CC BY
2.0](https://commons.wikimedia.org/w/index.php?curid=6262365)*

I'm currently listening to TOOL's latest album *[Fear
Inoculum](https://www.youtube.com/watch?v=q7DfQMPmJRI)*. The drums are
loud and physical. The stereo panning between the dual kick drums puts
me in a particular space. Some of this is because the band probably
fiddled over every nuance of the recording. Another part of it is that
I'm streaming a file at 24bit/96kHz, a so-called HiRes digital stream
from [Qobuz](https://www.qobuz.com), through a mid-level
[Focusrite](https://focusrite.com/) DAC on some mid-level near-field
[ADAM](https://www.adam-audio.com/en/) studio monitors. In short, this
setup is vastly more complicated and capable than the millions of people
listening to a Spotify stream through some free earbuds. Neil Young
wouldn't be satisfied but he might grudgingly say that I'm giving it a
good try. For years, he's worked to save music (and all art) from the
destruction of the digital. I think he has a point.

[Read about Neil Young Saving
Music](https://www.nytimes.com/2019/08/20/magazine/neil-young-streaming-music.html){.button
.primary}

## All That We've Seen

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/9bd45bed-9320-4755-b5b5-b6004c196b93_3916x2753.jpeg)](http://www.jamesnachtwey.com/){.image-link
.image2 .image2-773-1100}

*Photographer James Nachtwey By 方正 - [Public
Domain](https://commons.wikimedia.org/w/index.php?curid=36112717)*

When I was in Stockholm a few weeks ago, I saw [an exhibition of James
Nachtwey's work at the
Fotografiska](https://www.fotografiska.com/sto/en/exhibition/memoria/)
photography museum. I probably should have mentioned this in the issue
from Stockholm. When I think about it though, I feel I was likely
compartmentalizing the experience. It was confrontational, emotional,
and wrenching. The subject of his photography is journalism at its
roots. It's telling a news story. But, Nachtwey's skill, experience, and
art make these photos mini-narratives on the frailty of humanity and the
world we've built for ourselves.

[See more of James Nachtwey\'s
Photos](http://www.jamesnachtwey.com/){.button .primary}

## A Couple of Logs Here, A Few Rocks There, Voila

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/70e42975-6458-4788-a4a1-d0b8e287c0be_2288x1712.jpeg)](https://placesjournal.org/article/landscape-with-beavers/){.image-link
.image2 .image2-823-1100}

*An American Beaver, by Marcin Klapczynski, [CC BY-SA
3.0](https://commons.wikimedia.org/w/index.php?curid=223793)*

If we are really creative, we can imagine ourselves in the mind of a
beaver. Our goal is to be able to build a secure structure in which we
can raise our pups. To do that, we want to surround with what is
effectively a moat. But, there are only so many shallow ponds in our
environment. So, we need to make a pond, or a series of ponds. To do
that, we might need to make a few weirs to temporarily slow the flow of
a stream so we can build the main dam. In short, we are hydrological
engineers which have no heavy equipment and a body the size of a small
dog. What we are doing is providing not just a safe structure for our
beaver family but building an entire wetland environment that can change
with the seasons or the changing landscape over time. It's an entire
ecology wrought sheerly by the drive to survive. Yet, humans have
harvested beaver pelts for centuries and in recent years destroyed them
as pests which flood agricultural land. It's interesting to read this
research into how wrong we might be about the beaver.

[Read about a Beaver\'s
Landscape](https://placesjournal.org/article/landscape-with-beavers/){.button
.primary}

## Tear Down the Myths

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/d13c5a01-8ac5-4968-a013-c3ed863ad513_3000x2005.jpeg)](https://www.nytimes.com/2019/05/11/books/review/rick-atkinson-the-british-are-coming.html){.image-link
.image2 .image2-735-1100}

*By John Trumbull - Yale University Art Gallery, [Public
Domain](https://commons.wikimedia.org/w/index.php?curid=14619476)*

Those readers who grew up in the US know the American Revolution as an
honorable and obvious fight for the freedom we deserved. The historical
reality is muddier than that, as it often is. The populace was far more
split on the subject than we typically understand. The outcome of the
war was just as unsure. I remember being taught that muskets were very
inaccurate and that battles occurred infrequently among relatively few
combatants. But, [Rick Atkinson's new history of the American
Revolution](https://revolutiontrilogy.com/) brings the facts to bear on
the war and some of political machinations leading up to it. One nugget:
the American Revolution saw a larger proportion of the American
population killed than any other war save the Civil War.

[Read a review of Atkinson\'s new
book](https://www.nytimes.com/2019/05/11/books/review/rick-atkinson-the-british-are-coming.html){.button
.primary}

## Hike into the Sun

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/e5a20fd6-fca7-4dfa-952f-4018b541d086_2560x1920.jpeg)](https://japanhike.wordpress.com/){.image-link
.image2 .image2-825-1100}

*Mount Asama from Mount Aka By Mass Ave 975 - [CC BY-SA
3.0](https://commons.wikimedia.org/w/index.php?curid=4573275)*

In the recent past, I did a lot of trail running. That's not really
possible much these days but I have become interested in long-distance
hiking and walking. Plans are germinating in my brain about treks I'd
like to try but nothing has happened yet. I was delighted to discover
that Japan has quite [a network of long-distance trails that crisscross
the country](http://www.env.go.jp/nature/nats/shizenhodo/). It seems
like it'd be fun to tackle some of these trails. Sure, I live 20 minutes
from the Appalachian Trail and all the cool kids are hiking the PCT
(which I mentioned last week) these days. So, maybe let's skip all that
and spend a few months in the wilds of Japan and get ourselves good and
lost.

[Read a blog about Japanese
hiking](https://japanhike.wordpress.com/){.button .primary}

## The New, Old Song

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/0bbdf672-b7e7-4309-ab51-48f8b3282565_1920x1152.jpeg)](https://jakexerxesfussell.bandcamp.com/){.image-link
.image2 .image2-660-1100}

*Jake Xerxes Fussell by Brad Bunyea*

Jake Xerxes Fussell's new album, *Out of Sight*, sees him taking old
songs and reworking them into folk masterpieces. It's incredible to me
how the songs can sound ancient and fresh at the same time. It's a
testament to his internalizing the melodies and rhythms. These aren't
covers or homages. The opening tune, "[The River St.
Johns](https://www.youtube.com/watch?v=RaJCnhbVM64)," turns a
fishmongers sales pitch into a swirling, beguiling come-on. They are new
pieces of art imbued with the spirits of the original, so much so, that
I think the people who performed the originals come back a little each
time I listen.

::: {#youtube2-RaJCnhbVM64 .youtube-wrap attrs="{\"videoId\":\"RaJCnhbVM64\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=RaJCnhbVM64), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

[Buy \"Out of Sight\" at
Bandcamp](https://jakexerxesfussell.bandcamp.com/){.button .primary}

## Wanna Listen to a Book?

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/4e79aaaf-2409-4c95-8f6c-b65efd80bf4a_5716x5706.jpeg)](http://blogs.discovermagazine.com/d-brief/2019/08/22/reading-listening-activate-same-brain-regions/){.image-link
.image2 .image2-1098-1100}

*Kaarlo Virkki, an early proponent of audiobooks. By The Finnish Museum
of Photography from [Wikimedia
Commons](https://commons.wikimedia.org/w/index.php?curid=60472757)*

The audiobook market is exploding. On average, listeners are paying
twice the price for an audiobook versus what they'll pay for a print
book of the same title. So, publishers are doubling down with celebrity
readers/narrators and full-cast recordings. It makes sense. The market
wants this. Personally, I can't listen to audiobooks nor to podcasts. I
seem like the perfect audience and I want to like them but it just
doesn't work with my brain. However, the science says that listening to
an audiobook is no different than reading it as far as our brains are
concerned. This is new information as just a few years ago, research
pointed in the opposite direction. That original research was based on
the fact that auditory content and visual content are processed,
*initially*, in two different parts of the brain. That "initially" is
crucial. The latest research shows that our brains likely recognize this
input as being analogous and thus the content ends up being classified
and dealt with "as a book" insofar as our brain categorizes a book. I
think we have no idea how the human brain ingest, digests, and outputs
stories. It is magic because this insistence lies at the center of our
entire existence. We are beings driven by stories and little more.

[Read the audiobook
research](http://blogs.discovermagazine.com/d-brief/2019/08/22/reading-listening-activate-same-brain-regions/){.button
.primary}

## R.E.M. Song of the Week

This week we dig into the album tracks of R.E.M.'s *Document*.
Specifically, the red-herring titled "[Lightnin\'
Hopkins](https://www.youtube.com/watch?v=g67-MmLhIE8)." As a kid, this
title and a quick visit to my local library led me to [a blues musician
from Texas named Lightnin'
Hopkins](https://en.wikipedia.org/wiki/Lightnin%27_Hopkins). I found
some recording of his and found out that I liked blues music. However,
the song has nothing to do with him lyrically or musically. The myth is
that they couldn't think of a name and Peter Buck had a Lightnin'
Hopkins LP sitting around. So, they went with it. The lyrics are typical
early Stipe with any sort of meaning replaced with a pastiche of
impressionistic posturing. Still, the percussion production help's Bill
Berry's drums standout and the energy of the song feels anxious and
propulsive which works nicely with the burgeoning political content
R.E.M. was bringing into their work.

::: {#youtube2-g67-MmLhIE8 .youtube-wrap attrs="{\"videoId\":\"g67-MmLhIE8\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=g67-MmLhIE8), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

<div>

------------------------------------------------------------------------

</div>

I'm playing around with the format a bit. I hope you enjoyed the
pictures. Let me know how you liked it.

The images all come from the [Wikimedia
Commons](https://commons.wikimedia.org/wiki/Main_Page) where you can
also find tons of Creative Commons licensed images, videos, audio files,
or other digital media.

Your guide,

Alex

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/00d65456-fe3e-4314-b654-e66bcff9764e_4032x3024.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F00d65456-fe3e-4314-b654-e66bcff9764e_4032x3024.jpeg){.image-link
.image2 .image2-825-1100}

*Stockholm Public Library by Alex Ezell*
