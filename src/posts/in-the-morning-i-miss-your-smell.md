# I miss your smell

##### A weekly newsletter from Alex Ezell of the things that I appreciate

\
I hope you\'re continuing to enjoy this newsletter. Please let me know
what I can do to help you get more out of it. If you have ideas for a
header image, let me know or just send me something you\'ve made and I
might use it.\
\
**Legalize It All**\
Last week, I linked an article written by Kofi Annan about legalizing
drugs. Yesterday, I got my copy of Harper\'s magazine and the cover
article is [a report from Dan Baum titled \"Legalize It
All.\"](http://harpers.org/archive/2016/04/legalize-it-all/?single=1)
One of the points threading through the piece is that the United States
has strengths that make better sense when applied to regulation,
distribution, and taxation of drugs. I watched the movie *Sicario* this
week and it\'s a reminder of how far off we are from solving the drug
problem.\
\
We are, after all, a country that prides itself on our merchant class of
capitalist heroes. I\'m reminded of [David Priestland\'s
book](https://www.goodreads.com/book/show/15811525-merchant-soldier-sage)
*[Merchant, Soldier,
Sage](https://www.goodreads.com/book/show/15811525-merchant-soldier-sage)*
which is a historical synthesis detailing the ways these groups fought
for power since the Middle Ages. The book ultimately leads the reader to
see that an unfettered and unshackled merchant class brought the world
economy to its knees in 2008. Here, in the US, we are faced with a
presidential candidate in Donald Trump whose expertise as a merchant is
his leading claim to the highest office in the land.\
\
**Billions of Athens Music Fans**\
Speaking of merchants, [Showtime\'s show
\"Billions\"](https://en.wikipedia.org/wiki/Billions_(TV_series)) shows
us the inner workings of a hedge fund working in the rarified air of the
billionaires who make the US economy spin. Starring Damian Lewis (of
\"Homeland\" fame) as the mogul of a hedge fund and that inimitable rat
bastard Paul Giamatti as the district attorney desperate to bring him
low, the show is stylish, dark, and modern. On last week\'s episode, the
music was dominated by Athens bands Guadalcanal Diary and Pylon. Always
a band to play around with dynamics, Guadalcanal Diary\'s [punchy \"Say
Please\"](https://www.youtube.com/watch?v=ShFrfpaWWH4) and their [more
well-known \"Watusi
Rodeo\"](https://www.youtube.com/watch?v=Ut6yOjKoEUQ) featured
prominently in Lewis\'s characters tribulations during the episode. I
doubt there\'s much thematic connection between the show and the best
college town in the world but someone at Showtime must be a big fan of
the home of UGA. Much to my surprise, [Pylon\'s
\"Crazy\"](https://www.youtube.com/watch?v=bOSARRZiC2g) played in the
background of a bar scene. This comes on the heels of Pylon\'s music
being used in a Lexus commercial. So, yeah, merchants rule us right
now.\
\
**Money Knows No Borders**\
Globalism is a core tenet of the merchant\'s religion. More consumers
means more money. Emerging markets are ripe for slash and burn
capitalism. But, as most of us know, this globalism is a trap that
increasingly destroys the very markets and economies it claims to want
to build up. This [excerpt from Kevin
Bales\'s](http://blog.longreads.com/2016/03/08/your-phone-was-made-by-slaves-a-primer-on-the-secret-economy/)
*[Blood and
Earth](http://blog.longreads.com/2016/03/08/your-phone-was-made-by-slaves-a-primer-on-the-secret-economy/)*
[details how this global market hides
slavery](http://blog.longreads.com/2016/03/08/your-phone-was-made-by-slaves-a-primer-on-the-secret-economy/)
and ecological destruction. In the US, we are finding it easier to push
our own citizens into similar conditions when [we take away benefits
from hungry
people](http://www.thenation.com/article/congress-is-about-to-take-food-away-from-the-poorest-people-in-america/)
thanks to Clinton-era welfare reform. You may be aware that his wife is
currently vying for the Democratic nomination. I wonder if she is
similarly reform-minded? I would like to dedicate the [Violent Femmes\'s
\"Kiss Off\"](https://www.youtube.com/watch?v=5nza7AlHV5I) to Hillary
this week. The Femmes released a [new album last week titled \"We Can Do
Anything\"](https://open.spotify.com/album/2IQZb4UoPSX7rR7UMAIQt6) which
is their first album in 15 years. It seems they are back in great form.\
\
**What\'s Not To Love**\
I have listened to a lot of college/indie rock from the late 80s and
early 90s but somehow the [English band The House of
Love](https://en.wikipedia.org/wiki/The_House_of_Love) escaped my
notice. [Their song \"Destroy My
Heart\"](https://www.youtube.com/watch?v=5nza7AlHV5I) sounds as fresh
now as anything else I hear on the radio today. It\'s actually quite a
sad song belied by its post-punk leanings. I love to listen to sad songs
and I circled back this week to one of the saddest albums I\'ve ever
heard in [Slaid Cleaves\'s \"Everything You Love Will Be Taken
Away\"](https://open.spotify.com/album/27Sls2mhIrdd89rNZpOKPg) on which
you can wallow in the [tear-jerking \"Green Mountains and
Me\"](https://www.youtube.com/watch?v=FhBKHJy1Uk0) or feel [the horror
of \"Twistin\'.\"](https://www.youtube.com/watch?v=CleLy4lnNkU) Cleaves
was born in Maine but has been in Austin, TX for a while and his
songwriting strengths lie in narratives that seem personal and universal
and political all within a single line.\
\
**I Need Real Internet**\
I\'ve read the breathless reviews and heard the life-changing stories
about Amazon\'s Echo device. The NY Times recently published [an article
that details how much amazing promise is still left to
discover](http://www.nytimes.com/2016/03/10/technology/the-echo-from-amazon-brims-with-groundbreaking-promise.html?_r=0)
with the device. Sadly, I\'m on satellite Internet at my home and
devices and services like the Echo or Nest are absolutely useless
without fast, reliable Internet. The merchants continue to find newer
and faster ways for us to part from our dollars yet there\'s a large
swath of America that simply can\'t take part even if they want to. I\'m
pretty sure that Jason Isbell needs to add satellite Internet to [his
Saddest Song
Ever](http://www.stereogum.com/1864284/watch-jason-isbells-funny-commercial-for-the-saddest-song-ever/video/).\
\
**Two Albums That Have Nothing To Do With Each Other**\
There\'s a [live George Jones
album](https://open.spotify.com/album/7xuyP7IIKdeVnBFnUqrkxK) on Spotify
that is wonderful. George Jones probably never heard of space rock or
maybe even England but [Hawklord\'s \"25 Years
On\"](https://open.spotify.com/album/4PAuAbYJvlpFwg593X3hQz) is a deeply
fascinating piece of both.\
\
**Beauty Reigns**\
This week\'s moment of beauty comes from the recent discovery that [cave
paintings in the Sahara Desert depict nonhuman
hands](http://www.livescience.com/53944-prehistoric-rock-art-nonhuman-hands.html)
that are likely the feet of desert monitor lizards. I find this
beautiful because it meant that the artist was representing something
much more than their own likeness. Maybe it has a ritual significance or
maybe it was part of a joke. We\'ll likely never know but there\'s
something that connects us when we are struggling to express ourselves
like this artist was.\
\
**R.E.M. Song of the Week**\
Bound to be a weekly feature, here\'s the R.E.M. song you should listen
to right now. I love the piano part that Mike does on [\"Shaking
Through\"](https://www.youtube.com/watch?v=wdTujLHFsSU) from *Murmur*
released in 1983.\
\
Thanks for joining me this week. Have a great weekend!
