What does it mean to move forward and backward in time? We simply wake
up to some smart device telling us that we've moved backward in time.
Then, we pad around the house correcting all the dumb devices: the
microwave, the oven, the coffee maker. We tell them, "No, we've gained a
magical hour. What good can come of this?"

That our clocks moved during the witching hour just after a blue moon
Halloween should give us pause. Did we extend the witching hour? Do the
witches know? Does the poltergeist leave moist footprints halfway in to
our bedroom before wincing, "Shit. It's daylight savings. Be back in a
bit." Then, it rotates its feet completely around cracking the ankles
and walking on knees bending the wrong way giving a small bow of
apology.

Most of us lie in that delightful oblivion of sleep as the clocks
change. Given that time as we know it is simply a broken mathematical
model, we pass this magic hour in ignorance. Our circadian rhythms are
clumsily modified and the tension flows out as frustration, short
tempers, or simply a sick day later in the week. We show up late to
meetings we don't want to attend with the easy excuse, "the time
change," throwing up our hands to a wall of faces on a screen.

I spent a large part of my life searching for that liminal space between
some concrete reality and the designer oblivion of id-driven abandon. I
still push at those edges between the corporate, scientific structure of
a "successful" person and the ragged chaos of a life in pursuit of
beauty and art. This is a tension that's quite frankly tough to manage.
It manifests in my muscles and my more esoteric bodily processes. It
shows up in the sometimes cantankerous or, more charitably,
idiosyncratic way I go about my professional career. There is a yearning
unfulfilled.

This magical hour then might be a chance where in some cosmic sense, I
can exist comfortably in both states. My brain sorts through the
artistic impulse while the body rests an extra hour to prepare for the
requirements and responsibilities of the day. Maybe a lot of us are in
this airlock between what we want to be and what we've become. There's
safety through one door and a vast universe of the unknown through the
other.

If the magic hour of daylight savings lets us briefly exist in a nowhere
of both possibilities, how do we capture that freedom and opportunity in
all the hours of the year? How do we make our lives into the shape our
hearts crave?

I think the answer is being radically vulnerable. If we show up to each
situation as the self we can live with, rather than the self that others
might want to live with, we might make a dent in space/time that looks a
lot more like the shape of our soul.

It's November. The election is two days away. The trees don't know. The
birds don't know. Maybe it's OK if we forget for just a magic hour.

<div>

------------------------------------------------------------------------

</div>

## Movies to See Once

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/23d455c9-de9d-4730-b33d-7f56055a8fd0_800x600.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F23d455c9-de9d-4730-b33d-7f56055a8fd0_800x600.jpeg){.image-link
.image2 .image2-600-800}

There's a class of movies that you see and they stick with you possibly
in ways you'd rather they didn't. Darren Aronofsky's *Requiem for a
Dream* is one of those movies for me. I watched it on DVD one warm, late
night living in a mobile home amidst the dark woods of north Georgia.
That isolated darkness echoed the grim subject matter of addiction. The
direction spares nothing, nothing at all. At the end of the movie, I had
to take a walk out into the night. I went to pet the horses at the edge
of our property. I woke up the farm dogs and laid in the grass with
them. I needed to be surrounded by life and vitality. I took a long
shower and fitfully fell to sleep sitting in a chair as the sun came up.
I'll never watch the movie again and I'll never forget it.

[Requiem for a Dream at
20](https://www.theguardian.com/film/2020/oct/27/requiem-for-a-dream-at-20-aronofskys-nightmare-still-haunts){.button
.primary}

## There's Always Time to Bloom

::: {#youtube2-1crwMGppZR4 .youtube-wrap attrs="{\"videoId\":\"1crwMGppZR4\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=1crwMGppZR4), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Folk-punk? Post-rock? Twee-rock? I don't know how to classify the music
of The Front Bottoms. Even their name is a too-cute pun. Still, the
power of the lyric and the ramshackle energy is compelling. This track,
"everyone blooms," leads off their latest album, *In Sickness & In
Flames*. The title might refer to this year of our lord, 2020. There's a
positivity that falls from the dual vocals and the occasionally anthemic
song structures. This is among my favorite records of the year.

[Listen to everyone blooms](https://youtu.be/1crwMGppZR4){.button
.primary}

## Buy Some Art

::: {.instagram attrs="{\"instagram_id\":\"CGF_70IFDwK\",\"title\":\"A post shared by @gbenzur\",\"author_name\":\"gbenzur\",\"thumbnail_url\":\"https://scontent.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s640x640/120931625_118570479844076_5105351720156195249_n.jpg?_nc_ht=scontent.cdninstagram.com&_nc_cat=108&_nc_ohc=lCT6Kj8_sAoAX_53mQ8&_nc_tp=24&oh=359dac7008292068daa2472e84099c3a&oe=5FC8F511\",\"timestamp\":\"2020-10-08T19:08:42.000Z\"}"}
::: {.instagram-top-bar}
[gbenzur](https://instagram.com/gbenzur){.instagram-author-name}
:::

[![](https://scontent.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s640x640/120931625_118570479844076_5105351720156195249_n.jpg?_nc_ht=scontent.cdninstagram.com&_nc_cat=108&_nc_ohc=lCT6Kj8_sAoAX_53mQ8&_nc_tp=24&oh=359dac7008292068daa2472e84099c3a&oe=5FC8F511)](https://instagram.com/p/CGF_70IFDwK){.instagram-image}

::: {.instagram-bottom-bar}
::: {.instagram-title}
A post shared by [\@gbenzur](https://instagram.com/gbenzur)
:::

::: {.instagram-timestamp}
October 8, 2020
:::
:::
:::

Y'all know that I recently moved. I've been looking for some art to put
on the walls of my office. My taste in art is eccentric to say the best.
I largely respond emotionally and it's hard to pin down what will grab
me. However, a great friend, Gabe Benzur, is a painter in New York. I
was lucky enough to be able to buy a triumvirate (trilogy?)\[not a
triptych\] of paintings that respond to recent protests and state
violence. The subject matter is challenging but what I love about the
paintings is the energy that Benzur's brushstrokes echo in the scenes he
depicts. I also have been told about the pigments used and their history
and composition. Further, it's wonderful to have art that a friend has
made in the house. But, if you're into something a little goofier, I
also just bought [some of these
prints](https://www.etsy.com/shop/Notsofriendlygiant).

[Check out Gabe Benzur\'s
art](https://www.instagram.com/gbenzur/?hl=en){.button .primary}

## On the Nose

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/a6c97680-b989-4985-a2b2-3900082fc6a9_800x445.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fa6c97680-b989-4985-a2b2-3900082fc6a9_800x445.jpeg){.image-link
.image2 .image2-445-800}

It's not like I want to think about the pandemic. But, I can't avoid it.
On the news. In emails. Everyone in masks. So, it's hard to explain why
I watched this Russian TV show, *To the Lake*. It is definitely about a
pandemic but it excels, as the best genre shows/books/movies do, at
showing the fraying of a group of people both family and happenstance.
Specifically, the love triangle at the center of the story is
complicated and revealed in incredibly well-placed flashbacks. The
frozen landscape adds to the bleakness and the despair. The season ends
with a hell of a cliffhanger.

[Watch To the Lake](https://www.netflix.com/title/81302258){.button
.primary}

## History. On Repeat.

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/65a917ce-b492-4cd3-b974-ee977abfb14e_1400x840.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F65a917ce-b492-4cd3-b974-ee977abfb14e_1400x840.jpeg){.image-link
.image2 .image2-840-1400}

> ###### `Thomas Cole’s `*`The Course of Empire: Desolation`*`, 1836. `*`Courtesy New York Historical Society/Wikipedia`*

We know the phrase, "...doomed to repeat..." and this review of three
books about the fall of Rome shows us that there are many reasons a
decadent and visionless empire might fall. The drama is compelling
enough and there is no shortage of parallels to the times we live in.
Maybe that's always true? Perhaps empires are always ascending and
descending and we only find them similar to ours because of whether we
are on the winning or losing end of the change.

[Read No Barbarians
Necessary](https://www.nybooks.com/articles/2020/09/24/roman-empire-no-barbarians-necessary/){.button
.primary}

## Just Can't Win

::: {#youtube2-AmoYyXlFfcc .youtube-wrap attrs="{\"videoId\":\"AmoYyXlFfcc\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=AmoYyXlFfcc), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Yes, I've been listening to a lot of older country. There are some
incredible songs and wonderful singers. Earl Thomas Conley is certainly
in that category. "Can't Win for Losing You," is one of my favorites
with its combination of soul sounds and phrasing with a slightly country
guitar through line. It fits Conley's voice perfectly with its
melancholy yearning. The turn of phrase is often overdone but it works
here.

[Listen to Can\'t Win for Losing
You](https://www.youtube.com/watch?v=AmoYyXlFfcc){.button .primary}

<div>

------------------------------------------------------------------------

</div>

Thanks for indulging my longer, rambling musing at the front. If you
like those kinds of writings more than you like the links, let me know.
I might well make two different newsletters each week. One with more
links and less commentary and one with nothing but commentary and
writing.

As always, you can help me out by encouraging friends and enemies to
sign up here:

[Subscribe now](https://imissyoursmell.substack.com/subscribe?){.button
.primary}

Or, forward this email to them with a glowing recommendation or missives
of hate. All are welcome.

Your guide,

Alex
