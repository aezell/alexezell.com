::: {.captioned-image-container}
![Colonial Jamestown circa
1614](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/88b938ab-a04f-45e6-b9cf-765bb86da4f4_940x430.jpeg)
:::

This is a small glimpse into some genealogical work I've been doing. My
particular family history might be of light interest. However, the story
it tells about how we construct family narratives and how we see
ourselves in time I find fascinating.

Guy Fawkes tried to blow up the English Parliament in late in 1605.
Shakespeare was likely writing *King Lear* around the same period.
London was a place of incredible energy and opportunity.

Into this bustle enters Edward Gornto who arrives in London to prepare
for a trip to establish, or re-establish, a colony in the new world,
Jamestown. Maybe he caught the performance of *Macbeth* even as he
prepared ship's stores and got his affairs in order.

Would Edward have been able to read any of the news about Fawkes or pick
up some of the pamphlets decrying Shakespeare as an "upstart crow?"
Could he read at all? Would he even care? Maybe he had work to do
getting ready to sail across the sea to start a new life.

Edward would sail in December of 1606 and arrive in Jamestown in the
spring of 1607. He was 20 years old.

He was likely on the *Susan Constant* as he was known to be close with
Captain John Smith. Later, Edward would spend several months with the
Powhatan tribe teaching them how to hunt with muskets. It's nearly
certain, then, that he knew Pocahontas.

Edward would survive The Starving Time of 1609-10 when nearly 80% of the
Jamestown colonists died of starvation and related disease. He would
have seen the marriage of Pocahontas to John Rolfe as the prospects of
the Jamestown colony began to rise.

In 1626, Edward had 150 acres planted in a plot near the edge of James
Cittie \[sic\]. He was clearly doing fairly well for himself. As life
looked better and better, Edward married a woman named Ann and had two
sons, named William and John. Edward is my 8th great-grandfather.

It's possible that joining in at the christening of William Gornto was a
man named Timothy Ezell.

Timothy Ezell arrived in Jamestown sometime before 1640 as he married
Mary de Torres in that year in Jamestown. Timothy's mother passed away
in London in 1634. He was 16 years old. The family lived in Scrooby,
Nottinghamshire so their presence in London might indicate they were
preparing to sail for Virginia. They might have been inspired by the
Pilgrims who had a lot of support in Scrooby. It's unclear they would
have headed to Virginia instead of New England.

::: {.captioned-image-container}
![Scrooby Manor House where famous pilgrim William Brewster
lived](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/d76cd02b-5d80-4405-9247-7831de60b026_810x539.jpeg)
:::

It's not clear if Timothy sailed with his father or not. His father,
Richard d'Aiselle, died in Scrooby in 1651 so it's unlikely that he ever
made the trip to Virginia. Instead, young Timothy would have been 22 in
1640 and looking to start a family. Timothy is my 9th great-grandfather.

Edward's plot of land was on the same side of Surry County as the land
Timothy was granted. Edward, at the age of 53 in 1640, might have been
an older mentor for the 22 year old Timothy as they both worked mightily
through the trials of not just building a new world but of being
husbands and fathers. Timothy's first son was born in 1648.

Just under 400 years ago, it's likely that both my 8th
great-grandfather, Edward Gornto, and my 9th great-grandfather, Timothy
Ezell, lived within miles of each other, attended the same church, and
may well have worked and soldiered together.

But, the story doesn't play out how you'd think. Their offspring didn't
marry there in Jamestown. They didn't marry later in North Carolina as
the colonists spread to other parts of North America.

Instead, it would take their descendants all the way until 1946 to meet.
In that year, my paternal grandfather, Raymond Ezell, met a widower
named Ruth Folsom as they both lived and worked in south Georgia and
north Florida. Edward Gornto is Ruth's 7th great-grandfather and Timothy
Ezell is Raymond's 8th great-grandfather.

Two families present at the birth of our country disperse as so many
did. They move south. They move west. They fight in wars honorable and
dishonorable. They live long. They die early.

These families tell the story of migration drawn on bigger and bigger
maps by the Homestead Act. They are likely slaveowners. They fight in
the Seminole Wars. They guard Native Americans on the Trail of Tears.
(My 6th great-grandfather, Nathaniel Folsom, would marry Aiah Ni Chih Oh
Oy Oh and die in the Choctaw Territory in Mississippi.)

Their fortunes rise and fall. At the time Raymond and Ruth meet, Raymond
was returning to the family land in Florida (part of a Homestead Act
allowance) after fighting in World War II as part of the Navy. Ruth was
returning to south Georgia having built Liberty ships in Baltimore
during World War II. Her husband died in the war and she was caring for
two children.

They were farmers mostly growing tobacco. They had no idea that in their
history, in their DNA, in their pride, in their guilt, there was any
connection to the very earliest European colonization of these United
States.

For them, the goal was simple. Ride the explosion of American wealth and
prosperity after the war. Provide a leg up to their children. And they
did it. My father and uncle were the first in our family to graduate
from college. They went on to long professional careers that gave me
untold privilege. It was a giant leap.

Ruth died in 1980. I was five years old. I have just one memory of her.
She was serving me custard at a birthday party. I still have the custard
recipe. It's amazing.

Raymond died in 2017 three days after my father died. My memories of him
are fuzzy. We weren't close. I do have the memory of his enormous tears
as I graduated high school. He was filled with pride and spent broadly
of both sides of that coin.

It's disquieting to me that I can discover quite a lot about the lives
of my ancestors nearly 400 years absent and yet know so very little
about my own grandparents. Maybe we don't have those conversations
anymore. We don't tell the stories that link us all in this long chain
of innocence and experience.

I can describe a world of planted fields and hedgerows across which
Edward Gornto and Timothy Ezell discussed the collapse of the Powhatan
Confederacy. I cannot describe to you the dirt-floored house that my
father was born in though it's still standing. The stories it might
connect me to are lost to time.

::: {.captioned-image-container}
![Part of my family tree on
Ancestry.com](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/c5fdfce3-e40a-41bd-8888-6f3f93b7e6db_1630x882.png)
:::

I can trace the movements and varying fortunes across generations of
Ezells, Gorntos, Folsom, Carows, Higgins, Altenburgs, and all the
others. But, I can't pin down how the early death of my grandmother Ruth
changed my father irrevocably.

There's a wonderful gift in that. In the same way that we are given the
gift of forgetting, maybe too, the time and distance forgetting requires
helps us make sense of the then that was. We don't always have to filter
it through the now that is. We can let it exist in a moment of its own
making.

We are free to walk this timeline. We are challenged to let it root us
but not anchor us. Across 400 years of time, our connections don't
diminish. We remain a part of a grand, and ever-growing, narrative. It
is full of all that is human mystery. It is us.

What's your family story?

<div>

------------------------------------------------------------------------

</div>

I hope you'll enjoy some of these longer pieces that are for paid
subscribers only. Thanks for being open to taking a journey with me.
Again, if you'd prefer to go back to free and just get the weekly
newsletter, just let me know.

Your guide,

Alex
