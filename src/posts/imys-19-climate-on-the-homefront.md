Today is the Global Climate Strike. I'll be headed downtown here in a
bit to join in. The global nature of this movement is important but
paradoxically it has me thinking about home. Home is a loaded word. It
can mean a building. It can mean a city. It can mean the place where
particular people are. It can even mean a feeling that we get through
some combination of factors we couldn't possibly name.

My family and I are considering moving and so defining what home means
for us is forcing us to think in more concrete terms. Maybe it's getting
older. Maybe it's a reflex after years of being in different cities
every few years. We find ourselves yearning to find a home where friends
and family are close. But, in these modern times, our family is strewn
across the country and our friends are globally distributed. What do we
look for to define home in that circumstance?

Maybe it's what there is to do? Maybe it's whether it's near an airport
so we can visit all these far-flung places? Those are both luxuries. It
feels more like the answer is a return to roots. To a place where we
once felt comfortable and a part of a community. Perhaps that belonging
is what home is really about.

Thanks for reading today.

Send your friends and family to this link to sign up:

[Sign up now](https://imissyoursmell.substack.com/subscribe?){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

## I Drink Your Milkshake

###### 

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/35698250-ed74-48ca-8e26-f413c96ec38f_1024x415.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F35698250-ed74-48ca-8e26-f413c96ec38f_1024x415.jpeg){.image-link
.image2 .image2-415-1024}

###### 

We've all heard about the destruction that plastic is wreaking on our
planet. Bottled water is the poster child for wasteful and destructive
plastic use. However, harvesting the water that's put in those bottles
is an equally destructive force. It's extractive capitalism as bad as
oil or natural gas. It overburdens a precious resource and destroys
local ecologies in favor of global profits. [This New York Times
article](https://www.nytimes.com/2019/09/15/opinion/bottled-water-is-sucking-florida-dry.html)
details some of the destruction that Nestlé is causing in Florida with
their rapacious contracts to siphon off water from the Florida Aquifer,
possibly the largest underwater aquifer on Earth. Keep in mind this
quote from the former CEO of Nestlé, Peter Brabeck-Letmathe:

> The one opinion, which I think is extreme, is represented by the NGOs,
> who bang on about declaring water a public right. That means that as a
> human being you should have a right to water. That's an extreme
> solution. The other view says that water is a foodstuff like any
> other, and like any other foodstuff it should have a market value.

[Read about water
harvesting](https://www.nytimes.com/2019/09/15/opinion/bottled-water-is-sucking-florida-dry.html){.button
.primary}

## Take Me To Church

::: {#youtube2-IVdqdMcT1yA .youtube-wrap attrs="{\"videoId\":\"IVdqdMcT1yA\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=IVdqdMcT1yA), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

You've almost certainly heard Brittney Howard's powerful music in her
role as the leader of the band Alabama Shakes. Today, she's released a
new album, "[Jaime](https://brittanyhoward.bandcamp.com/album/jaime),"
and it's truly stunning piece of work. It's full of great guitar work
grounded in a mix of gospel, blues, soul, and occasional rock. It feels
like a full expression of artistic vision that in many ways is quite
separate from my experience yet still pulls me in. I highly recommend
checking out the whole album.

[Watch a live version of He Loves
Me](https://www.youtube.com/watch?v=IVdqdMcT1yA){.button .primary}

## Fear the Narrowed Perspective

Stephen Jenkinson is an author, teach, spiritual activist, farmer, and
found of the [Orphan Wisdom](https://orphanwisdom.com/) school. In a
series of interviews, he lays out what he sees as the foundational
problems of our North American society. Primarily, it's a story of our
shunning and denigrating the learnings of the previous thousands of
years. Sure, we have access to unknowable amounts of information. I work
daily on projects that provide this information to the entire globe.
Yet, we've lost the understanding of which of these things is truly
important and will lead us to an equitable and fulfilling future.
Information is not knowledge and Stephen helps us search for the
knowledge we may have lost. It involves finding the connections that
stretch to the past in order to build paths to the future. It involves
finding kinship through generations. It involves finding our place in
the natural order. It's hard work and we've become awfully lazy.

[Watch the Birdhouse Interview
Series](https://orphanwisdom.com/interviews/the-birdhouse-wisdom-series/){.button
.primary}

## I'm the Captain Now

There are plenty of stories of coal miners losing their jobs. In recent
weeks, I've poked at the coal industry as a symbolic but actually quite
small industry and suggested that perhaps we should look away. In this
mindset, I was surprised at the turns this article about women workers
in coal country slowly changing the world they live in. As the article
points out, women in these areas have long taken temporary work to keep
the lights on between layoffs or work slowdowns in the mine. What's
happening now is a noticeable shift of women moving to skilled,
full-time work mostly in the healthcare industry. That is giving these
women new-found freedoms and broadening horizons many of them never
before considered possible, desirable, or right. As expected, some men
fight against this under the macho contract that they alone should be
able to provide for their families. Others are begrudgingly encouraging
this shift. It's not lost on me that the work these women are taking on
is providing care for the health problems that come from coal mining,
intermittent poverty leading to poor nutrition, health effects of
pollution, and the disease of addiction. It seems a sad cycle but this
change might be part of breaking it.

[Read about Women Workers in Coal
Country](https://www.nytimes.com/2019/09/14/us/appalachia-coal-women-work-.html){.button
.primary}

## Walk On By

I hesitate to write about a book I haven't finished but I can't help
myself. Chuck Wendig's latest book, "WANDERERS," is truly incredible. It
tells the story of a mystery illness compelling some people to walk
towards an unknown goal. They don't eat. They don't sleep. They don't
react. They simply move, inexorably, toward some unknown end. The book
slithers between body horror, science fiction, personal loss, parental
responsibilities, and government run amok. It feels both of our time but
timeless in its concerns. The writing is visceral and clear. There's a
surprising amount of humor, most of it dark but still relieving of
tension in just the right way. It's a long book but reads like it's half
its length. I've seen hints of a delicious twist at the end. I've been
staying up late for a week working my way toward.

[Check out Wendig\'s
WANDERERS](http://terribleminds.com/ramble/project/wanderers/){.button
.primary}

## Strummer's Mind

I think a lot about the art and challenge of curation. This newsletter
is evidence of my desire to work through this task of finding what's
right, helpful, good, challenging, or just delightful. Whether you like
the music of The Clash or not, Joe Strummer's short-lived radio show is
a master stroke of curation. He plays music from across the genre
spectrum and the world. He has insights into what the music does for him
that are compelling. More importantly, his energy and passion about what
he's playing is infectious. The way he feels about these songs makes me
want to figure out how I feel about them. I don't even have to like the
songs but Strummer makes me feel like I should at least consider them or
how they make me feel.

[Listen to Joe Strummer\'s Radio
Show](https://beta.prx.org/series/27024){.button .primary}

## Of the People, By the People

The Bible is oft-said to be the most popular book in the world. My local
bookstore has two 20 feet long rows of shelves 6 feet tall full of
various versions, sizes, colors, and temperaments. It's clearly a good
seller. We could likely argue about how often it gets read. Still, the
book has a history. It was written by people, most likely men, and those
people had reasons for doing what they did. They had sources, goals,
pressures, and probably no surfeit of creativity. John Barton has
written a new book called "The History of the Bible" which I can't
recommend as I haven't read it. However, based on Kirsty Jane Falconer's
review in the Times Literary Supplement, it seems to be quite a
well-done work given the various pitfalls to which it could have fallen
victim. I understand this literary-historical examination of The Bible
runs counter to many people's conception of the book as a divinely
inspired, infallible document. Falconer makes it clear that Barton is
working to dispel this notion. However, like most books, there's always
something to be gained from its study. It can be crucially important
without being perfect.

[Read Falconer\'s
review](https://www.the-tls.co.uk/articles/public/false-witness-untruths-bible/){.button
.primary}

## R.E.M. Song of the Day

::: {#youtube2-zJB628tWEHk .youtube-wrap attrs="{\"videoId\":\"zJB628tWEHk\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=zJB628tWEHk), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

This week, we are going back to what many people consider to be R.E.M.'s
first album, "Chronic Town." It's just an EP and while it is recognized
as the first recorded output, there may be a few 7" shared singles that
got pressed but the validity of those is in question. So, let's go with
it. My favorite song off "Chronic Town" is "Wolves, Lower" which opens
the album. It sets the tone for the band quite clearly: guitar-forward,
but not powerful, more jangly and loose; rhythm section that propels the
rest of the song instead of just backing it; mostly inscrutable lyrics;
clear and important backing vocal from Mike Mills; a pervading sense of
anxiety and concern. And then the chorus blows that all up with an
almost 60s sounding, full-throated wail in harmony. It hooked me
instantly. Sure, they better play "Gardening at Night" at my funeral but
"Wolves, Lower" has a darkness that I like.

[Listen to \"Wolves,
Lower\"](https://www.youtube.com/watch?v=zJB628tWEHk){.button .primary}
