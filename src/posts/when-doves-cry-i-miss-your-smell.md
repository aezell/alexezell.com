# I miss your smell

### A weekly newsletter from Alex Ezell

As you\'re all aware, Prince died yesterday morning. In the last 24
hours, many beautiful and laudatory words have been written about his
art, his style, his singularity. I hope you\'ll indulge my using this
newsletter to share a personal recollection of Prince.\
\
\--\
\
The summer after my parents got divorced, my mom and I were living in a
small, two-bedroom townhouse in a cookie-cutter complex in Albany, GA. I
had just finished the 5th grade and was 11 years old. Mom was working
hard to make sure I had everything I wanted. I couldn\'t have known it
then but it had to have been quite a struggle in those early years after
the divorce. I didn\'t make it any easier on her as a boy hitting
puberty and trying to, ill-advisedly, assert some sort of control over
my life. I probably didn\'t deserve it but she made sure I had a
home-cooked meal every night even after 10 hour days at work. I don\'t
know how she did it.\
\
It was hard to make friends that summer as the new kid in the complex. I
didn\'t go to the same school as any of them. I went to the private
school right down the street and they somehow all knew this the day I
moved in. For the most part, they were all older even if only a year or
two. But the difference between an 11 year old and a 13 year old can be
measured in light years.\
\
Eventually, like all new kids everywhere, I got that one invite to play
football or throw the frisbee and then I was in. I\'ve never felt
totally in anywhere. That oddball group of kids was exciting, dangerous,
and loyal. It seemed like I could be in this crowd. It was what I wanted
and maybe needed.\
\
We would spend our days playing home run derby knocking tennis balls out
of the complex courtyard and running bases until someone would sprint
back from the parking lot and tag us out. Now I know it was like our own
form of cricket. At night, after we all back home for dinner and slowly
drifted back outside, we\'d gather around a big oak tree and tell dirty
jokes or scary stories. There was bragging about our BMX bike exploits
and dreaming about being anywhere other than where we were.\
\
There was a clubhouse next to the swimming pool in our apartment
complex. Sometimes, when it was too dark to ride our bikes anymore and
hide and seek got boring, we\'d use a screwdriver to jimmy the door of
the clubhouse to sneak inside. We\'d play ping-pong or pool and tell all
the really dirty jokes that didn\'t seem fit to be told in the open air.
We\'d tell second-hand stories of all the cool stuff our older siblings
were doing and about half of it would be true.\
\
One night, a 15 year old kid named Steve brought his boom box and a box
of cassette tapes. After he played a few tunes, I walked over and was
looking through the box and saw a tape with \"Prince and the Revolution
/ Purple Rain\" on it. I\'d heard whispers about the song \"Darling
Nikki\" but I\'d never heard any of the music.\
\
My parents have always been music fans. My house was full of music as a
kid whether it was oldies from the 50s and 60s or my dad playing Mickey
Newbury on the guitar. It was mostly contemporary country or pop. There
was very little rock and roll and certainly nothing like the college
radio music I\'d grow to love. The radio stations in our small town were
strictly Top 40.\
\
Given my somewhat sheltered existence at the time, this Prince tape was
forbidden fruit of the highest order. My mom had expressly forbidden
Purple Rain from my possession as well as Violent Femmes\'s self-titled
debut. Here was Purple Rain right in front of me and mom was nowhere to
be found. I stopped whatever garbage was playing to the howls and hoots
of the assembled miscreants.\
\
Call it luck. Call it fate. I played side two first and \"When Doves
Cry\" came tumbling out of the speakers. The Purple Rain album was old
news to most in our group but for me it was new and dangerous. I swear I
could see the sounds pulsating from the speakers. Even for the lazy and
simple ears of a bunch a teenagers, the song brought the room to a
standstill. It demanded and received our full attention.\
\
Though I was at least 6 months from really hitting my stride in puberty,
lyrics like \"the sweat of your body covers me\" made for vivid pictures
in my head. I had no idea where they were coming from but it felt
unstoppable. The music was unpredictable and it oozed something I
hadn\'t known before.\
\
There was a girl, probably 14, who had blossomed really early and she
was dancing along to the song. I couldn\'t take my eyes off her. All of
us, the other girls included, just gaped. Some part of me thought about
the snake charmers I\'d seen in cartoons and hypnotism and zombies.
Anything that could make sense of the power this music had. Never
looking away from her, I turned up the volume. If a little of this was
good, more would be better.\
\
Several of the kids were beating out that iconic rhythm on the tables in
the clubhouse and it seemed like our tribe had found its theme song. We
no longer had to be \"the private school kid\" or \"the stoner\" or
\"the jock.\" Prince\'s music became the foundation of a summer of
surpassing friendship.\
\
It would be years before I\'d have the access to really delve into
Prince\'s work. Years before I had the knowledge and experience to even
understand much of what he was writing about. But over that summer, I
listened to the Purple Rain album almost every night with our crowd. A
few times, Steve let me take the tape home with me. I\'d sit in my room
with a small portable tape player with the volume turned as low as
possible while I fell asleep to \"The Beautiful Ones.\" I had to be sure
Mom wouldn\'t hear it.\
\
That fall, Steve died of a drug overdose while Nancy Reagan went on TV
and begged us to \"Just Say No.\" The pretty older girl moved away
because her dad couldn\'t stay out of prison. Mom and I soon moved into
our own house which she worked so hard to buy. I think she knew I
brought Steve\'s copy of Purple Rain with me.\
\
I left those apartments, those hidden trails through the barren fields
next to the Harvey\'s grocery store, those endless evenings of home run
derby, and the clubhouse where, one summer, Prince gave me the slightest
nudge to begin putting away my childish things.\
\
\--\
Alex
