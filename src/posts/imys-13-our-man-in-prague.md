## Dobrý den

This edition comes to you from Prague where I've traveled for work. It's
the third time this year I've been in this city. I've seen it across
three seasons. This morning, I went for a run along the Vltava River and
was reminded how many of the great capitals of Europe are bisected by a
river. It's obvious that cities would begin near sources of water. In
particular, a river can mean transportation, sustenance, protection, and
hygiene. What's less obvious to me is how, as a city grows, the
bifurcation of that city by the river determines its character, or more
likely, dual character. There's the saying about "the wrong side of the
tracks" and I imagine there's a "wrong side of the river" in many
places. On the run this morning, I saw a beautiful residential complex
built around a small marina on one side of the river. Directly on the
other side was a graffitied, tumble-down industrial plant. The river is
a connector and a divider. It creates and destroys. It is corralled and
wild. Is there are river that runs through the middle of all of us?

<div>

------------------------------------------------------------------------

</div>

## Goodbye David Berman

I woke up Thursday morning to the news that David Berman died. I want to
write something profound and real about what his art meant to me.
Because it did mean something. Something essential. His [most recent
album under the moniker Purple
Mountains](https://purplemountains.bandcamp.com/album/purple-mountains)
tapped into many of my deepest fears and regrets. In fact, it reminded
me of some parts of myself that I hide away and feel shameful about.
Some of those are things I've been working on in this training during my
trip. So, it's all very raw and fresh. Maybe in a few weeks or months,
I'll understand more about what that actually means. Until then, [this
Mick Flannery
song](https://mickflannery.bandcamp.com/track/fuck-off-world) says more
about how I felt on hearing the news. It's pithy and angry and probably
shallow but it's real for me in this moment.

## The Story of Stevia

[Stevia is the product of
biopiracy](https://www.atlasobscura.com/articles/where-is-stevia-from),
a somewhat relatively new term which describes the act of taking,
usually indigenous peoples', ecological knowledge and claiming to "own"
it so that it can be turned into profit. This case has all the usual
touchstones of racism, colonialism, bureaucratic ineptitude, and
ecological destruction. What touched me more about the story is that the
Guaraní people expected the outsiders to not just understand the
specific attributes of the plant from which Stevia is derived but to
also treasure its spiritual, medicinal, and ceremonial attributes. There
was an implicit assumption that all people would be looking for that
same connection. Of course, they've been bitterly disappointed but it's
heartening to glimpse that belief in a world where oneness is often seen
as a yearning only by the weak. Unfortunately, the Guaraní have been
forced to seek legal means to protect that heart of unity. Hardly the
connected and unified solution they'd hoped for.

## Imagine the Other Path

I've been listening to Lucinda Williams's song "[Side of the
Road](https://www.youtube.com/watch?v=fXXaYvmtHzE)" repeatedly for
weeks. The picture she paints of wanting to imagine herself and her life
if something very core was different is a powerful evocation of
possibility. In this song particularly, it doesn't feel like escapism
but a way to reinforce the rightness of the choices we've made. In other
words, we live in the other life for a moment to truly understand the
joy of the life we've chosen.

::: {#youtube2-fXXaYvmtHzE .youtube-wrap attrs="{\"videoId\":\"fXXaYvmtHzE\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=fXXaYvmtHzE), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

[Here's a decent
interview](https://music.avclub.com/lucinda-williams-reveals-the-sad-stories-behind-her-son-1798244644)
with her about some of her iconic songs including this one.

## Wikipedia for Teens

I work for the Wikimedia Foundation. We are a nonprofit that supports
Wikipedia and other related projects through funding, program
management, software engineering, and legal help. We have nothing to do
with the content. Please don't ask me why there's no page for your
favorite Instagram star. Instead, you should be asking that question of
[the people at Famous
Birthdays](https://www.theatlantic.com/technology/archive/2019/07/famous-birthdays-wikipedia-gen-z/594682).
The company has built a site that allows anyone to "suggest" changes
like a wiki but with far fewer limitations on what the English Wikipedia
would call notability. The site has attracted a young audience intent on
learning about and sharing information about the stars of Youtube,
Instagram, TikTok, and more. The new celebrities of our time. I love the
idea of any group collaboratively curating knowledge they deem
important. The Internet used to be a thriving hub of subcultures and
obsessively niche interests. It was better then.

## We Have No Idea

The more I read about nutrition and how what we consume affects our
bodies, the more I'm convinced that we don't know much. I take Magnesium
supplements every day based on my doctor's measurement of my blood
levels and certain symptoms that occur from my Hashimoto's Disease. In
researching how magnesium works in our body, I came across [this very
long article describing some of the possible roles magnesium plays in
many, many biological
processes](https://www.ncbi.nlm.nih.gov/books/NBK507265/). Most
interestingly is its role in major depression and the potential
connection to overall inflammatory load. While most mainstream media
will look to this as a silver bullet, it's probably more helpful to
think of it as part of an overall picture of our health. One more piece
of the puzzle each of our bodies represents.

## An Apple a Day

You might think that the apples you see in your grocery store are the
apples that have always grown and they end up in your store because some
farmer decided to pick them and bring them to you. Instead, you'll soon
be able [to buy the Cosmic Crisp
apple](https://story.californiasunday.com/cosmic-crisp-apple-launch)
which combines the latest innovations in market research, genetic
modification, and branding. Presumably, the apple tastes good but that's
not what's interesting. The apple is patented and there are millions and
millions of dollars invested in its creation and production. This is
effectively equivalent to bringing a new pharmaceutical to market or
introducing a new crossover SUV. This is marketplace manipulation at its
finest. I can only assume that someone is growing heirloom apples to
sell me at my farmer's market. I'll pay more for the privilege.

## On the Wing

Given how much travel I've been doing recently, I've taken to maximizing
my spending for credit card points and frequent flyer miles. There's an
entire world on the Internet where you can lose yourself amidst the
minutiae in charts detailing how much the points of different providers
might be worth in cash equivalency. You can sign up for notifications
about ever-changing deals that credit card companies hawk in an attempt
to generate business. This older article details [the experience of Ben
Schlappig](https://www.rollingstone.com/culture/culture-news/up-in-the-air-meet-the-man-who-flies-around-the-world-for-free-43961/),
one of the heroes and stars of this world of people who treat it all
like a grand game. What seems sad to me about all of this is that many
of the people engaged in "The Hobby" don't appear to take time to enjoy
many of the places they go. Maybe they embody the notion that the
journey is the goal? I guess if I got to [hang out in one of these
Emirates suites](https://www.youtube.com/watch?v=a7NJ6Gek9v4) for 14
hours, I might feel the same way.

## A Highway Lament

Fred Eaglesmith has a voice that seems like it doesn't want to come out
and face the world but it must. It's insistent, pained, and powerful in
equal measure. This song, "Trucker Speed," highlights the grim drudgery
of the disconnected American long-haul trucker. It's also the story of
anyone who's ever wanted to escape to find out what lies at the end of
the long, black highway to oblivion.

::: {#youtube2-lRbrjSf9K7s .youtube-wrap attrs="{\"videoId\":\"lRbrjSf9K7s\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=lRbrjSf9K7s), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

## The Meaning of Life

Charles Eisenstein is a thought-provoking writer, philosopher, and
teacher who is trying to chart a new description of the world we could
have. He is the author of [several of my favorite
books](https://charleseisenstein.org/books/) which often work not to
change my mind but to draw out the truths I already knew but had hidden
from my conscious awareness. His new course jumps farther into the
metaphysical than he has previously done and tries to answer the ageless
question, "Why Am I Here?" I'm excited to see what comes out of this. I
don't know if I have the time right now to actually participate but I do
hope it's a successful avenue for him as I'd like to see his thoughts on
these topics continue to develop.

::: {#youtube2-eoLND-F1fhY .youtube-wrap attrs="{\"videoId\":\"eoLND-F1fhY\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=eoLND-F1fhY), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

## Timeless Oakie

Speaking of ageless, Paul Oakenfold's mix sets on BBC Radio 1's
Essential Mix series float in a world where the fads of electronic music
fade away behind expert song selection and incredible transition mixing.
My favorite set of all of Oakie's Essential Mix sets is [this one from
the Rojan in
Shanghai](https://soundcloud.com/pauloakenfold/paul-oakenfold-radio-1-10).
Incredibly, this set was made 20 years ago but the beats and songs hold
up. Long after gabber and dubstep and other stuff comes and goes, this
work stands up with solid curation and adroit skill.

::: {.soundcloud-wrap attrs="{\"url\":\"https://api.soundcloud.com/tracks/61920553\",\"title\":\"Paul Oakenfold - Radio 1 Essential Mix, Live from Rojan in Shanghai, China 26-09-1999 by Paul Oakenfold\",\"description\":\"Since being the first ever guest DJ to record an Essential Mix for BBC Radio 1, Paul Oakenfold has gone on to record a further 35 EMs.\\r\\n\\r\\nThis archive will enable Paul Oakenfold fans new and old to listen to these legendary mixes in their entirety on demand.\\r\\n\\r\\nThe Taste Experience - Highlander\\r\\nEverything But The Girl - Five Fathoms (Club 69 Future Club Mix) [Virgin]\\r\\nTeam Progress - Symphonic Music\\r\\nVengaboys - Paradise\\r\\nDesyfer - 831 (Supa Kings Dub)\\r\\nEve - Riser (Christian West Remix)\\r\\nThe Blackmaster - Solid Tube\\r\\nAmanda Ghost - Filthy Mind [WEA]\\r\\n ?\\r\\nMoogwai - Viola [Perfecto]\\r\\nSpace Man - DJ Orkideo Remix/Stage One/Space Manouveres [Perfecto]\\r\\nMobile Bitch - Sex For Pleasure [Bitch]\\r\\nPlanet Perfecto - Bullet In The Gun [Perfecto]\\r\\nSkip Raiders - Another Day (Trance Mix) [Perfecto]\\r\\nAmoeba Assassin - Speedway Revisited [Amoeba Assassin]\\r\\nGiorgio Moroder - The Chase (Perfecto Remix) [Polydor]\",\"thumbnail_url\":\"https://i1.sndcdn.com/artworks-000031166151-27tmhw-t500x500.jpg\",\"author_name\":\"Paul Oakenfold\",\"author_url\":\"https://soundcloud.com/pauloakenfold\"}"}
::: {.iframe}
::: {#widget .widget .g-background-default .g-shadow-inset style="height: 100%"}
:::
:::
:::

<div>

------------------------------------------------------------------------

</div>

I appreciate your joining me again this week. If you have any kind of
feedback, just hit reply and let me know. If you think others would
enjoy this, forward it on and encourage them to sign up. Next week's
edition will be coming to you from Stockholm.

Your guide,

Alex

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/87ff49d5-8fe1-449a-aa9f-359fc28e2b7d_4048x3036.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F87ff49d5-8fe1-449a-aa9f-359fc28e2b7d_4048x3036.jpeg){.image-link
.image2 .image2-825-1100}

*The old and new in Prague.*
