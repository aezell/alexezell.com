---
CocoaVersion: 1894.6
Generator: Cocoa HTML Writer
---

[Thanks for joining me for the inaugural edition of I miss your smell.
You\'ll notice this is a pretty bare-bones affair. I\'d like to keep it
that way unless you tell me it needs more whiz-bang.\
\
My hope is to share the things I come across that I enjoy, despise,
can\'t figure out, or laugh at. This is going to be loosely formatted so
if there\'s a section or type of thing you particularly enjoy, let me
know so I can do it again. If you think I\'d have a particularly strong
reaction to something, hit reply and make me know it.\
\
**Spelling is Hard for Computers**\
I wanted to listen to the amazing Athens, GA band,
[[Pylon]{.s2}](http://wearepylon.com/) so I did a search for it on
Spotify. Turns out, Spotify doesn\'t know shit about a umlaut such that
the band, [[PÃ¿lon]{.s2}](http://www.pylon-doom.net/), is mixed in with
my beloved art-punk-pop. Turns out that PÃ¿lon is a Christian Metal band
with the great domain name, Pylon-Doom.net. Spotify is going to fix
things up so in the meantime, enjoy [[Pylon\'s
Crazy]{.s2}](https://www.youtube.com/watch?v=bOSARRZiC2g).\
\
**A Resurrection**\
Commiserating with my buddy, Matt, about the genius that is the early
Indigo Girls (especially [[Kid
Fears]{.s2}](https://www.youtube.com/watch?v=O6zm704DLkE)), I came
across [[this recording of Jesus Christ
Superstar]{.s2}](http://www.daemonrecords.com/jesus_christ_superstar_jcs.html)
featuring a broad assemblage of Atlanta musicians including Kelly Hogan
and Mike Mantione (Five Eight) and the Indigo Girls. Nothing could hit
my sweet spot any better except maybe some of that hummingbird cake my
grandma used to make. Turns out that
[[Benjamin]{.s2}](https://en.wikipedia.org/wiki/Benjamin_Smoke), a
well-known denizen of the musical and artistic Atlanta underground,
appears on the CD which led me to revisit the work of his band Opal Foxx
Quartet and the song, [[Clean White
Bed]{.s2}](https://open.spotify.com/track/6JciaNDqIpZrPU6ksIBAkl). Put
aside the Tom Waits vocal comparisons.\
\
Quiet Hounds is a band from Atlanta (maybe no more) that initially came
to my attention with the song [[\"Beacon
Sun\"]{.s2}](https://www.youtube.com/watch?v=DAifd4s-w-4). They\'ve got
a new album out full of shimmery indie pop. Apparently, the band has
been separated geographically and the songs seems to echo that sense of
distance and abstraction. Check the new album out here: [[Shake Don\'t
Shatter]{.s2}](https://open.spotify.com/album/4OOBD1xfIkSxkQv2It3SRn).\
\
**A Revolution**\
My friend, Tate, shared a link to [[Amanar\'s new album
Tumastin]{.s2}](https://sahelsounds.bandcamp.com/album/tumastin) which
is a stunning example of the Tuareg guitar style often called \"desert
blues.\" This music grows out of the combination of Malian rhythms and
Bedouin culture filtered through electric guitars with their treble
turned to 11. [[Sahel Sounds]{.s2}](https://sahelsounds.bandcamp.com/),
out of Portland, OR, brings a lot of music from sub-saharan West Africa
back to Western listeners via field recordings, cell phone dumps, and
live studio recordings. Often characterized as timeless or infinite, I
find this music to echo our immediate present with its danceable rhythms
and often political lyrics.\
\
I donated to a political candidate for the first time this week. This
election seems important and finally there\'s a candidate that echoes
many of my beliefs. It got me thinking about books that might shine some
light on the state of politics in our current century. [[O/R
Books]{.s2}](http://www.orbooks.com/) is a publisher that is putting out
some fascinating books exposing the details behind many political and
social issues that are important in the current election cycle. From
books on Hillary Clinton, [[*My Turn* by Doug
Henwood]{.s2}](http://www.orbooks.com/catalog/my-turn-by-doug-henwood/),
to the state of healthcare in America, [[*Killer Care* by James B.
Lieber]{.s2}](http://www.orbooks.com/catalog/killer-care-by-james-b-lieber/),
O/R Books presents a nuanced collection of well-written, deeply observed
books that penetrate the rhetoric of our superficial media.\
\
Technical debt is a concept well-embedded in the modern jargon of
software development. [[Kellan Elliot-McCrea makes a reasoned
argument]{.s2}](https://medium.com/@kellan/towards-an-understanding-of-technical-debt-ae0f97cc0553#.mna7sd2az)
for how this notion is destroying our ability to confidently deal with
software systems whose architecture and age are maturing amidst a sea of
quickly changing standards and approaches. I especially like his attempt
to describe the emotional impact that working on these kinds of projects
has on developers.\
\
**A Reassessment**\
Growing up in the United States, most of what we hear about World War II
are the heroic stories of American soldiers storming the beaches of
Normandy under withering fire. Make no mistake, the boys (and they were
mostly just boys) who were part of the invasion and the eventual capture
of Germany were indeed acting on a level of duty and brotherhood that
few achieve. That said, there\'s much more to WWII than most of us
typically learn about in school.\
\
Two books that I\'ve come across this week delve into the historical
milieu that existed on either side of the war. [[Timothy Snyder\'s
*Bloodlands: Europe Between Hitler and
Stalin*]{.s2}](https://www.goodreads.com/book/show/6572270-bloodlands)
examines the murderous regime of US ally Josef Stalin and how that is
related to the mass murders that occurred in Germany under Adolf Hitler.
Examining the period after the war, [[Giles MacDonogh\'s *After the
Reich: The Brutal History of the Allied
Occupation*]{.s2}](https://www.goodreads.com/book/show/852167.After_the_Reich)
takes a look at how the German people (and others) and nation were
treated after the war ended. It is a sobering and piercing view into
human foibles like revenge and hubris writ large across an entire
nation. Bonus book: [[Nicholson Baker\'s *Human Smoke: The Beginnings of
World War II, the End of
Civilization*]{.s2}](https://www.goodreads.com/book/show/1948985.Human_Smoke)
attempts to be a holistic rethinking of the myths created around the
war.\
\
**Self-Defeating Thought**]{.s1}

[How many children could survive off that whole pizza I just ate?]{.s1}

[**Beauty Reigns**\
By chance, I caught [[Davina and the Vagabonds doing Etta James\'s
\"I\'d Rather Go
Blind\"]{.s2}](https://www.youtube.com/watch?v=EzWAePrAJsw) on Bluegrass
Underground. This performance is stunning.\
\
If you watch this next video right after that, you\'ll probably die.
It\'s just too damned beautiful. This is [[The Gloaming performing the
traditional \"Sailor\'s
Bonnet\"]{.s2}](https://www.youtube.com/watch?v=usva3chWf5w) live in a
church in Cork, Ireland. [[The Gloaming\'s second album
\"2\"]{.s2}](https://open.spotify.com/album/2G10IAMXTYotpRk5w5psSV) was
released today.\
\
Bonnie \"Prince\" Billy gets awkward and weird with [[his video for \"I
See a Darkness.\"]{.s2}](https://www.youtube.com/watch?v=d-xSxWX7_-4)
Let\'s not forget Johnny Cash [[covered this song back in
2000]{.s2}](https://open.spotify.com/track/0eg0nRL9dLBaT0X43keMBc).\
\
]{.s1}

[[[]{.s3}](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F1b71929c-95e2-41b1-b1d5-2ad014bd2f20_640x429.jpeg)]{.s1}

[This is a composite photo by Stephen Wilkes from the [[January 2016
issue of National
Geographic]{.s2}](http://ngm.nationalgeographic.com/2016/01/national-parks-centennial-text)
used for an article about the importance of national parks.\
\
Thanks for joining me and let me know what I can do to make this better
for you. See you next week!]{.s1}
