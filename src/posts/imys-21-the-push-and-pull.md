A murder of crows has made its roost in the cemetery behind our house.
Like some harbinger of doom from a Dickens novel, they sit and caw and
screech at each other. Occasionally, they tumble down and take flight to
harass a hawk or jay that dares enter their territory. They share a
strange resemblance to the fraternity that lives just down the street.

I watch them from my window. They gather material for nests or just out
of curiosity. They land on the roof just outside with string, pieces of
plastic, and even a loan child's glove. I don't know what they are doing
with all of these things. I can't see them in the tree where they roost.

These birds have found ways to adapt to this environment. They are using
the castoffs of human existence to beautify their surroundings. They
hide things in the gutters of my house and come back and get them later.
They have figured out how to succeed in spite of our encroachment on
their lives.

I'm envious of that adaptability. As I grow older, I get more easily
frustrated by change. And yet, I want so many things to change. There's
a paradox here that I can neither predict nor reliably explain. We all
feel it. We know the things in our lives that we want to lock away. The
smell of a freshly bathed baby. The comfort of a favorite pillow. We
also know those things that we wish were different from the shallow,
more money, to the profound, a deeper relationship with our family. This
tension is universal.

But the crows seem to have it figured out. And what I notice about this
is that they help each other. Two of them flew by yesterday carrying
some heavy piece of plastic. It was too big for one of them to do alone
so they did it together. They helped each other. It was simple.

How could we help each other manage these deeper changes in ourselves?

[Share](https://imissyoursmell.substack.com/p/imys-21-the-push-and-pull?&utm_source=substack&utm_medium=email&utm_content=share&action=share){.button
.primary}

[Sign up now](https://imissyoursmell.substack.com/subscribe?){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

## The Behemoth

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/b09534cd-d9b2-4b53-9d43-5df507325c7c_5500x3516.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fb09534cd-d9b2-4b53-9d43-5df507325c7c_5500x3516.jpeg){.image-link
.image2 .image2-703-1100}

###### By [William Balfour Ker](https://blog.mcny.org/2011/09/20/the-age-of-innocence/) - [Public Domain](https://commons.wikimedia.org/w/index.php?curid=79828934)

My distaste for Amazon is well-known among my friends and family. I
despise their labor practices. I despise their anti-competition
practices. I despise their ethos. I specifically despise what they've
done to the book business. However, I have to acknowledge that their
platform and their web services are the best in the world. I shudder to
think that in our current capitalist system, this business is not only
legal but is lauded. Communities and individuals trip over each other to
garner a whiff of some of that Amazon money. The New Yorker explores
whether all of this might make the business unstoppable. There is some
rich irony in the fact that a business that might be destroying our
economy as we know it is called Amazon while that same global economy
creates the pressure destroying the actual Amazon forest.

[Read about the Amazon
Juggernaut](https://www.newyorker.com/magazine/2019/10/21/is-amazon-unstoppable){.button
.primary}

## Wail and Froth

::: {#youtube2-UIcVwH47uxQ .youtube-wrap attrs="{\"videoId\":\"UIcVwH47uxQ\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=UIcVwH47uxQ), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

The band Big Thief is difficult to pin down. Their songs range from
quiet, whispered laments about a love dashed to pieces to intricate pop
musings about nature to noisy rock encapsulating the thrall of anxiety.
This song, "Not," is definitely in the latter camp. It comes off a
wonderful new album, *Two Hands,* described by the band as the "Earth
twin" to their previous album, *U.F.O.F.* What I love about the song I'm
sharing here is how the first half of the song drives on the repetition
of the lyrical structure and the increasing rawness of Adrianne Franke's
voice. The second half of the song echoes that back with the falling
apart of the guitar solo. It's wonderfully constructed with an intricacy
and interplay between the instruments that's sneakily hidden behind the
emotion and energy.

[Listen to \"Not\" by Big
Thief](https://www.youtube.com/watch?v=UIcVwH47uxQ){.button .primary}

## From or To?

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/891817bb-c1c1-4dab-9c3a-475f2d190b15_2016x2520.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F891817bb-c1c1-4dab-9c3a-475f2d190b15_2016x2520.jpeg){.image-link
.image2 .image2-1375-1100}

###### By TheLegNY - Own work, [CC BY-SA 4.0](https://commons.wikimedia.org/w/index.php?curid=75612760)

I've shared here before about my interest in ultrarunning. It's easy to
make it in to something which sounds quite fantastic and special. But,
really, it's just running as far as you can as fast as you can. The fast
part is mostly optional. Tom McGrath knows the thrill of this kind of
running. He once held the record for running across the United States.
This profile of him in the New York Times hints at what people always
want to know about this kind of running: "Why do you do it?" It'd be
overly simple to say that McGrath is outrunning his demons but it might
not be totally wrong either. The most endearing part of this story is
that McGrath seems so casual about his lifetime of overcoming
challenges. In the same moment, it feels all very serious, too. It's
that paradox between the simplicity of what he's doing and the
complexity of why he's doing it. It seems almost like art in that way.

[Read about Tom McGrath\'s final
run](https://www.nytimes.com/2019/08/15/nyregion/tom-mcgrath-ultramarathon-ireland.html){.button
.primary}

## When Protesting is Terrorism

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/0241ca95-b43e-4f54-87dc-534715706b1e_980x460.png)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F0241ca95-b43e-4f54-87dc-534715706b1e_980x460.png){.image-link
.image2 .image2-460-980}

###### [A protestor "locked on" to construction equipment.](https://globaljusticeecology.org/woman-locked-to-construction-equipment-to-protest-mountain-valley-pipeline/)

About 10 miles from where I'm sitting, workers are cutting down a swath
of the National Forest and burying enormous 48" pipes to carry fracked
natural gas from West Virginia over to the coast. This is the Mountain
Valley Pipeline project. It has been a source of concern in our area for
years. The project has faced legal battles, marches in the street, and
leafletting campaigns. Many cars around town have "NO PIPELINE" bumper
stickers on them. Still, the chainsaws whir and the heavy machinery
scars the mountainside. For those people determined to stop the
pipeline, the only form of protest left is "direct action." While direct
action is a controversial method of protest, recent cases have shown
that law enforcement wants to treat these acts as terrorism. You would
be shocked at the lengths that Forest Service officers have gone to, not
to protect the forest, but to protect the corporate interest destroying
that forest. Eminent domain has been widely used in our area to make way
for the pipeline and people have been forcibly removed from the land
their families have been on for generations. Now, they and the people
supporting them through nonviolent protest are increasingly being
labeled terrorists. It's the best way for these corporate interests to
try to squash the growing backlash against their destructive businesses.

[Read about the New Green
Scare](https://progressive.org/magazine/the-new-green-scare-king-191001/){.button
.primary}

## How's the crop?

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/2860b0f1-5029-4f54-969d-830a4add09e8_1296x972.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F2860b0f1-5029-4f54-969d-830a4add09e8_1296x972.jpeg){.image-link
.image2 .image2-825-1100}

###### By LSDSL - Own work, [CC BY-SA 3.0](https://commons.wikimedia.org/w/index.php?curid=2232399)

I truly believe that psychedelics or drugs derived from natural
psychotropic compounds will change the face of mental health treatment.
It will take a will, sure, but that sea change is coming. Maybe it's
more hope or faith than actual certainty but the look the same in the
right light. Mike Jay tells the story of the farmers who were asked to
supply the ergot, a parasite that grows on rye, from which LSD was
extracted before it could be synthesized. It's fascinating that so many
people think of LSD as some scary chemical cooked up in a lab by mad
scientists. In reality, it's just some compound that a fungus produces.
It's mushroom sweat. If one ascribes to the Gaia principle, we might
consider that this compound exists because it is required within the
ecosystem. Our environment provides so many tools for us to live, grow,
and prosper. Yet, we have lost touch with all of those ancestral ways of
healing and existing in harmony. We insulate ourselves from the natural
world. We close ourselves off from the energy and flow that exists just
the other side of the window I'm looking out of. Sure, LSD and other
psychedelics are not going to be a panacea for everyone. However, we
might take the opportunity to look around and identify the gifts our
Earth has provided.

[Read about the Acid
Farmers](https://mikejay.net/the-acid-farmers/){.button .primary}

## A Little Heartbreak

::: {#youtube2-RKyR-kOMLNY .youtube-wrap attrs="{\"videoId\":\"RKyR-kOMLNY\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=RKyR-kOMLNY), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Son Little is an R&B artist who has worked with The Roots, Mavis
Staples, and others. This song, "about her. again," comes off as a
modern combination of soul and and R&B with some interesting production
choices. It comes off of a new EP from the singer that jumps around
stylistically but never loses sight of the emotion and dexterity of
Little's voice. This particular song is one I'd love to see performed
live. It feels like one that could tear the roof down.

[Listen to \"about her.
again\"](https://www.youtube.com/watch?v=RKyR-kOMLNY){.button .primary}

## Strong Brown God

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/dcf39b28-9412-4b71-a4f9-0e5c68f88e27_1280x850.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fdcf39b28-9412-4b71-a4f9-0e5c68f88e27_1280x850.jpeg){.image-link
.image2 .image2-730-1100}

###### By Dirk from San Diego, USA - Mississippi River Barge, [CC BY 2.0](https://commons.wikimedia.org/w/index.php?curid=5387312)

For decades now, rivers have been consigned to the old way of moving
things from place to place. Planes and trucks are faster and easier and
more flexible. More recently, many cities have begun to reclaim the
rivers at their heart. Tearing down industrial wastelands and making way
for parks and recreation space. In Germany, it's a law that a river must
have a certain amount of green space on either side of it within a
city's limits. It leads to incredible river side parks where city
dwellers can take a break from the confines of concrete and glass. Back
in the US, the Mississippi is all of these things and more. The river is
the vibrating, electric wire running through the middle of our country.
Millions of lives depend on it for water, food, and commerce. This
profile of the barge pilots that move material up and down the river
shares a slice of an America most of us will never see. It's fascinating
to me how quick we are to move away from those things that work simply
because they seem slow or old. We are culture of the forever new and
improved.

[Read about barge
captains](https://www.atlasobscura.com/articles/triumph-tribulations-towboats-mississippi-river){.button
.primary}

## R.E.M. Song of the Week

::: {#youtube2-kXVeHjj_odw .youtube-wrap attrs="{\"videoId\":\"kXVeHjj_odw\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=kXVeHjj_odw), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

*Fables of the Reconstruction* was released in 1985. I was nine years
old. I wouldn't hear it for another five years. But, it would leave an
indelible mark. The title of the album hints at the circuitous and
diaphanous lyrics which refuse to be pinned down. The album feels like
it's steeped in a history very different from our own. The second song
on the album, "Maps and Legends," voices this fantastical element quite
well. There's a layering of sound here that finds me searching for the
song's heart which bears repeated listens.

<div>

------------------------------------------------------------------------

</div>

Let's find our heart's song this weekend.

Your guide,

Alex

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/d9f9d176-6c34-47d8-9b0f-1a20ad36189e_4032x3024.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fd9f9d176-6c34-47d8-9b0f-1a20ad36189e_4032x3024.jpeg){.image-link
.image2 .image2-825-1100}

###### By Alex Ezell - Washington and Jefferson National Forest
