I buried this in yesterday's newsletter under my wall of text so I'll
highlight it here.

[Get you some mail](https://forms.gle/SwpGsHnLqK1S1ucH9){.button
.primary}

If you provide your mailing address, I will send you something small
that I made with my own two hands. It's just a thank you for being a
subscriber as we close in on 100 subscribers. I promise not to use this
for anything other than sending this one single item.

Encourage your friends, enemies, doppelgangers, and hangers on to sign
up.

[Share I Miss Your
Smell](https://imissyoursmell.substack.com/?utm_source=substack&utm_medium=email&utm_content=share&action=share){.button
.primary}

While you fill out that form, listen to this:

::: {#youtube2-gxRq23qVE8A .youtube-wrap attrs="{\"videoId\":\"gxRq23qVE8A\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=gxRq23qVE8A), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Your guide,

Alex
