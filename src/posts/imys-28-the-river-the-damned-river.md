We took a ride through a swath of southern Indiana yesterday. Cannelton
to Tell City up and around the bend to Troy then swing back down through
Grandview to Rockport. There's an Ohio River Scenic Byway that runs
along the river for the most part. It swings around a few industrial
spots and makes sure to tack you through those small towns. Some of
them, like Tell City, have found some renewed vigor after decades of
decline. Others have floundered as the economy has left them behind.

It's a midwestern, river version of what I've seen across the South as
I've lived and traveled in the area. The local industry goes away. Pick
a reason: offshoring, technological disruption, ecological disruption,
and/or a multitude of others. Without jobs that can support a family,
the young people leave. Those tensions lay bare whatever societal ills
already exist: racial disharmony, economic disparity, drug abuse, the
usual suspects. Then, the center gives way and the tumble begins.

We stopped at a small diner in Tell City for a cup of coffee and a piece
of pie. The wall was lined with pictures of little league teams and
crowds gathered to police trash along the riverfront. They all seemed to
stop sometime around 1988. The edges of the photos curled out yearning
to be cherished or else to escape. The coffee was \$1.25, unlimited
refills. The custard pie was nearly as good as my grandmother's.

The waitress talked about working double shifts to make Christmas money.
But, her double was eating into her time delivering food for Grubhub
over in the big city, Evansville. Her kids had knocked a hole in the
wall of their house because they were kids and they were alone. The
neighbor called to tell her because she saw them peeking their faces
out. It was 35 degrees last night and the cardboard over the hole didn't
help much with the cold getting in.

We walked over one block to a classic downtown furniture store. They
also sold power equipment. A sign read: A Sofa for the Lady, A Saw for
the Gent. The sign looked like the nameplate on a 50s Bel Air, all
swooping lines and chrome nearly in motion. If it was in a bigger city,
it would be nostalgic and kitschy. Here, it was an anchor to another
time. Was that anchor saving this town from the storm battering at its
edges or was it holding it from reaching the safety of the harbor?

Each of these towns had large flood walls at the river's edge. They were
marked with lines and the years that the floods reached that height.
Some were likely nuisances. Others might have wiped the lesser buildings
away. Now, the wall protected them, presumably. It also obscured any
view of the river and only allowed the narrowest of access to the
riverfront. It was protection but I'm not sure from what.

There were some bright spots. New apartments built up to see over the
wall and engage with the river. New restaurants and shops built where
older manufacturing businesses had died. Life spurted out of the cracks
in the hard broken bits, of the streets, of the wall, of the people.

I've read about how the larger cities, at least in the US and parts of
Europe, are losing population. People are moving out. It's likely a
temporary thing but if we find ourselves as a culture, as a species,
reaching back to the smaller gatherings, the huddlings at the edges of
modernity and safety, wouldn't it make sense? Wouldn't there be some
sense of the cycle of human growth and retreat that would see people
yearn for something they can wrap their head around more easily?

Science fiction has long posited the megalopolis as our near-certain
endpoint. Sometimes, these massive conglomerations are owned by
corporations. Sometimes, they are chaos made visible. Is there another
outcome? One where humanity searches out right-sized communities so that
our individual abilities and ideas and interests can blossom. What would
that look like? How would you build a community like that?

The opportunity is there. Tell City has a population of just about 8,000
people. Cannelton is only about 1,500. Troy is just 400 or so. If
someone with the energy, vision, and commitment tapped into the history
and connection that the locals have with this place they call home,
could we achieve a whole sparkling firmament of right-sized communities
with the river as the backbone connecting them all.

How about where you live? How would you revitalize these places that are
disappearing? What connects them and makes them unique? Maybe it's you.

[Share I Miss Your
Smell](https://imissyoursmell.substack.com/?utm_source=substack&utm_medium=email&utm_content=share&action=share){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

## Get in the Boat

::: {#youtube2-tHPwTL3GI-g .youtube-wrap attrs="{\"videoId\":\"tHPwTL3GI-g\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=tHPwTL3GI-g), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

All this thinking about the river reminded me of this Jacques Yvart
song. Yvart is a French singer who was enamored of the sea and the life
of the sea. I saw him play a small cabaret-style show in Atlanta as part
of a field trip in high school. The trip was arranged and led by our
French teacher, Randa Mixon. Mrs. Mixon passed away a while back. She
was an incredible teacher. When I lived in Switzerland, speaking French
every day, one of my coworkers asked me where I learned French. I told
them that I had an amazing teacher in high school, Mrs. Mixon. They
agreed that she must have been excellent because my French was "pretty
good for an American." She introduced me to Jacques Yvart and the work
of Jean-Paul Sartre. This song reminds me of her and the buoyant chorus
echoes her smile.

[Listen to \"Ohé Ohé du
bateau\"](https://www.youtube.com/watch?v=tHPwTL3GI-g){.button .primary}

## Can't Get There From Here

::: {.captioned-image-container}
![The classic plot
structure](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/2cc16fa9-6934-4921-a778-e558dbf34d1e_630x419.png)
:::

Issa Manley's amazing comic details how our existing narrative
structures fail to help us describe and understanding the climate issues
that plague us. The explanations are clear. The art is beautiful and its
simplicity impactful. I love the idea of challenging how we structure
our stories to understand more deeply the world around us. Given that I
place narrative at the center of our existence, this shoving out at the
edges of a constricted understanding is welcome and elucidating.

[Read about the Shape of a
Story](https://www.youtube.com/watch?v=k3fz6CC45ok){.button .primary}

## Let's Dance

::: {#youtube2-ASrS7GrhBH0 .youtube-wrap attrs="{\"videoId\":\"ASrS7GrhBH0\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=ASrS7GrhBH0), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

It doesn't always have to make sense to just feel right. Los Bitchos is
a psychedelic, instrumental band making cumbia music with a modern
twist. They appear to be based in London with members from Sweden,
Uruguay, and parts unknown. None of that is terribly important when you
just let the rhythms and the guitar move your body. This is dance music
with personality.

[Listen to Los
Bitchos](https://www.youtube.com/watch?v=ASrS7GrhBH0){.button .primary}

## Bump

::: {.captioned-image-container}
![By Meow - Own work, GPL,
https://commons.wikimedia.org/w/index.php?curid=4288403](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/05ff5501-3b2d-41f5-82b8-610489b63376_898x546.png)
:::

This love letter bemoaning the loss of bulletin boards or Internet
forums says a lot of the things I've been trying to recapture about the
earlier Internet. Forums are still my favorite way to interact with the
slightly like-minded. It's asynchronous. It's passive. It requires only
the attention you want to give to it. These forums can be delightfully
hermetic. I've been a part of music-focused forums that coalesced around
the leadership of two ketamine-soaked admins with too much server space.
I still talk to a masseuse in Hawaii and a machinist in Ohio that I met
on that board. I was there on TribalWar when the "All Your Base Are
Belong To Us" meme was created. While this article rightly focuses on
the value of moderation, what it appears to enjoy about that time of
forums is the community that developed. That was always the draw.

[Read about the death of Internet
forums](https://www.engadget.com/2020-02-27-internet-forums-dying-off.html){.button
.primary}

## Don't Wait

::: {#youtube2-YTovORb24UQ .youtube-wrap attrs="{\"videoId\":\"YTovORb24UQ\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=YTovORb24UQ), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

What you'll notice first about Trampled by Turtles is their sheer speed.
The band plays fast, very, very fast, bluegrass. It's a modern,
pop-tinged bluegrass that builds on the ancient instrumentation and
simple harmonies with solid songwriting and storytelling. The above
song, "Wait so Long," is a fan favorite as you can tell by the audience
shots here. A track called "Alone" is one that possibly captures a
broader set of their strengths. I appreciate the connection to the old
form brought to a new and more open space.

[Listen to
\"Alone\"](https://www.youtube.com/watch?v=qpyeK_3arbg){.button
.primary}

## Delivered by the Muse

::: {.captioned-image-container}
![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/7ac2d28c-cbab-4ce5-a405-eefc401dc4cb_750x466.jpeg)
:::

I can't comment on any of the math in this article about Srinivasa
Ramanujan. But, it's questions and thoughts about the origins of
creativity and genius touch neatly on the conversation we had recently
about reaching for beauty. That is, Ramanujan very clearly saw his
inspiration as divinely delivered. For him, it appears that was the
source. His capturing what was shared was a religious ecstasy. Much is
written about the notion of the Muse. Still, there's a through line here
that it all centers on our desire and openness to channel whatever this
energy is. Ramanujan had to decide to spend his time away from work
writing down and working through his dream visions. Any artist has to
make the time and space to let the inspiration make its way to the
world. As in Ramanujan's story, there are pressures that tend to hold
back that expression. Imagine a world that embraces this energy.

[Read about Ramanujan\'s
Garden](https://www.scienceandnonduality.com/article/the-secrets-of-ramanujans-garden){.button
.primary}

## Thinking's the Last Thing

::: {#youtube2-3U0arXdrvlE .youtube-wrap attrs="{\"videoId\":\"3U0arXdrvlE\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=3U0arXdrvlE), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Speaking of muses, the Wood Brothers get it. This song nails it all.
Life might be the muse if we don't think about too hard. This band pairs
brothers Chris Wood who played with Medeski Martin Wood for decades and
Oliver Wood who was part of the band King Johnson. Their work together
brings their interests and influences to the front with a level of
personal songwriting that maybe the previous bands never had.

[Listen to \"The
Muse\"](https://www.youtube.com/watch?v=3U0arXdrvlE){.button .primary}

<div>

------------------------------------------------------------------------

</div>

Last chance to get mail from me. Sign up now. I'll be mailing things out
later this week hopefully.

[Get some mail from me](https://forms.gle/F4F49R5xqmHPCJocA){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

Have a great week! Thanks for spending a bit of time in my head.

Your guide,

Alex

## School 2020

::: {.captioned-image-container}
![By Alex
Ezell](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/7a1e3aa6-c4df-4356-a401-c501c20b79b1_4032x3024.jpeg)
:::
