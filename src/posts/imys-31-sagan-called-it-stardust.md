IMYS will be on holiday break until after the New Year. Get some rest.

<div>

------------------------------------------------------------------------

</div>

Yesterday, my dear friend, [Tate](https://music.tateeskew.com/), sent me
a link to an article about [Native American tribes buying back their
sacred
lands](https://www.nationalgeographic.com/history/2020/12/dna-of-this-land-cherokee-quest-reclaim-stolen-territory/).
In that piece, there was this quote from Amy Walker about some ancestral
land that she was gardening and shepherding, "I thought about the ions
of ancestors, of their bones that are still deep in the ground, and how
does that relate to me? I have a responsibility to raise the corn."

It's a common thought to consider that one's ancestors might have lived
in a place for a long time but Walker's use of the word "ions" really
caught my eye. It's a science-y word that actually mirrors the reality
of the fact that we are made up of the elements and molecules of all
that's come before us.

I'm especially struck that this concept applies not just to the humans
we know and love but even those we despise. There is no difference
between us when we are broken down into our constituent parts. The
neighbor with a different yard sign. The coworker taking all the credit
for your work. In 200 years, you'll all be mixed up in a miasma of
multitudes. Then, add in all the animals and bugs and plants and it's
just one giant biomass of which we represent a small and overly
impactful part.

That's the global, universal view but I'm more interested in the local
way this plays out. How does this natural process of creation and
destruction change who we are and how we respond to the places we
inhabit? This molecular connection to place is something that has shown
up in my writing often.

To that end, as a way to close the year out (and to encourage me to
finish it) I wanted to share a short excerpt of the novel I've been
working on. It touches on some of these ideas of how we might be
physically connected at an atomic level.

The context is a teenager dealing with the embarrassment of his changing
body and burgeoning awareness of the world and those who inhabit it.

<div>

------------------------------------------------------------------------

</div>

I hoped I'd be flung into the red clay mud. I'd sink down into it. Have
the red paste fill my nose, my ears, and my mouth until I was more clay
than human. Keep sinking as I dissolved into smaller and smaller pieces
until I worked through some layer of limestone and dripped down a
stalactite into one of those underground aquifers. By now, I'd be long
forgotten. My hard-on just a funny story told by an old man about his
disgusting friend.

I'd be shot back toward the surface and be reborn into the world as
clean water. The new me, the clean water me would be sucked into the
roots of a Green Ash. I'd work my way up to the branches carrying my
burden of minerals and nutrients. I'd make a hundred choices of left or
right as I navigated the branching into smaller and smaller avenues
until I arrived at a leaf just in time. Leaving behind my burden, I'd
move toward the light and bead on the outside of the leaf as a perfect,
clean drop of water.

I'd sit there getting larger and larger until I was too large to hold on
anymore. I'd fall then toward the pool of water below. I'd be lucky that
the Green Ash chose the edge of this spring. I'd be lucky that it grows
tall and straight here because I'd have plenty of time to take in the
scene below as I fell. Two boys swimming, splashing, and laughing.

My fall would be stopped as I landed on the head of the one boy. The
thin one with the easy smile and the blue eyes. There'd be other water
there in his hair but I'd stay me. I'd be unique. As he leaned back into
a deep belly laugh, inertia would pull me down his forehead limning his
eye and skirting past his nose finding the corner of his mouth.

He'd lick his lips and I'd be inside him. I would then take another
journey through a maze of branches and dead ends until I found myself in
his head or in his heart or in his blood. Each of those would have a
part of me. I might cease to exist at that moment. I would have become
that which I was meant to be.

<div>

------------------------------------------------------------------------

</div>

As an experiment, I recorded an audio version of the above. Maybe you'd
rather listen?

::: {.soundcloud-wrap attrs="{\"url\":\"https://api.soundcloud.com/tracks/947348521\",\"title\":\"Book Excerpt - The Spring by aezell\",\"description\":\"\",\"thumbnail_url\":\"https://soundcloud.com/images/fb_placeholder.png\",\"author_name\":\"aezell\",\"author_url\":\"https://soundcloud.com/aezell\"}"}
::: {.iframe}
::: {#widget .widget .g-background-default .g-shadow-inset style="height: 100%"}
:::
:::
:::

<div>

------------------------------------------------------------------------

</div>

Happy Holidays!

Alex
