::: {#youtube2-yVuCKaetRss .youtube-wrap attrs="{\"videoId\":\"yVuCKaetRss\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=yVuCKaetRss), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

It's quite rare that anything I might have to say would be
time-sensitive but this certainly is.

The National Theatre occasionally films their stage productions and
provides them for free on Youtube. One of their latest productions is
*Death of England: Delroy.*

It tells the story of a good man on a bad day. It\'s a one-man play with
a riveting, high-wire performance from Michael Balogun. The lighting and
sound design start simple and become perfect companions to Balogun\'s
unraveling Delroy. It\'s stunning.

This makes me want to be back in a theater either in a seat or on the
stage. The fearlessness and vulnerability of a performance like this is
intoxicating.

[Watch Death of England:. Delroy](https://youtu.be/yVuCKaetRss){.button
.primary}
