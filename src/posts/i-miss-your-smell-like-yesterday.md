# I miss your smell

### A weekly newsletter from Alex Ezell

While political battles rage and society tears at itself rending a
culture into disparate quivering bits, some of us might sit for a moment
and take a longer look at the creativity and wonder of which humanity is
capable.\
\
\
**The Best Dreams I\'ve Ever Had**\
Proper Scots Frightened Rabbit have [a new album out
titled](https://open.spotify.com/album/2I7bN0vjfoFw9di77xAZ4Y)
*[Painting of a Panic
Attack](https://open.spotify.com/album/2I7bN0vjfoFw9di77xAZ4Y)* and
it\'s a beauty. Layering their confessional yet abstract lyrical style
with a mixture of electronic, rock, and folk sounds, the band seem to be
at their most accomplished. The layering of traditional acoustic sounds
on electronic foundations is compelling if not exactly novel. I
especially [enjoyed the lead-off track \"Death
Dream.\"](https://www.youtube.com/watch?v=YYLZrCuhu_0) I\'m a sucker for
the slow-build dynamic and it\'s used to great effect here. Lyrically,
it\'s a simple notion of how powerful dreams can be.\
\
**Forced Culpability**\
I\'m not sure what to make of the world\'s seeming rush to embrace VR
technology. I haven\'t tried it yet but I have ethical and moral
questions about what it means to participate in the activities that will
be part of the first wave of VR content. A new movie begs these
questions more thoroughly through its use of new first-person camera
technology. *[Hardcore
Henry](https://www.youtube.com/watch?v=96EChBYVFhU)* [is a Russian-made
film](https://www.youtube.com/watch?v=96EChBYVFhU) conceived and
directed by Ilya Naishuller that tells the story of some cyborg who
needs to rescue, blah, blah, doesn\'t matter. The conceit here is the
first-person nature of this hyper-violent movie. What does that mean for
the viewer\'s culpability in the actions? When the third-person,
observer perspective is erased am I now more responsible for that which
occurs seemingly at my hands? At any rate, it\'s new and interesting and
deserves some attention for the challenges it makes of us. Naishuller is
also lead signer of a band called Biting Elbows and their [new music
video for the song \"For the
Kill\"](https://www.youtube.com/watch?v=Ph5YmTDDdVI) shows some behind
the scenes views at how the movie was made. Naishuller came to
prominence for his direction of earlier Biting Elbow music videos like
[this one for \"Bad
Motherfucker.\"](https://www.youtube.com/watch?v=Rgox84KE7iY) The music
reminds me a bit of Moneybrother, [check out \"Born Under a Bad
Sign,\"](https://www.youtube.com/watch?v=IiylR5BrFsM) mostly for the
vocals but there\'s also this connection of taking sounds that originate
elsewhere and processing them through some idealized notion of what they
should be.\
\
**What if Humans Intentionally Interfered**\
It\'s not secret that our oceans are suffering from the garbage and
pollutants that we dump into them every day. Coral reefs are especially
impacted because of their fragile and complex ecosystem and proximity to
our shores. New work being done in Hawaii [aims to solve some of these
issues through direct human
interference](http://www.newyorker.com/magazine/2016/04/18/a-radical-attempt-to-save-the-reefs-and-forests).
Often, conservation or ecological repair focuses on how to eliminate
human impact on an affected ecosystem. That is, how do we get humans or
our effluent detritus away from the area? Or something like a giant DO
NOT TOUCH sign. The model detailed in the article takes a different tack
of direct and purposeful interference to engineer or fabricate
ecosystems that can survive the human impact that is likely to never
subside.\
\
**The Podcast Killed the Radio Star**\
NPR has been fighting for survival for a long time. Whether it was
conservatives trying to kill funding for what was perceived as a liberal
media bastion, or anti-intellectuals refusing for their tax dollars to
pay for anything as \"worthless\" as the common good, or simply
people\'s rush away from listening to the radio. There\'s a [new
combatant now tearing down the walls of the kingdom of
NPR](http://www.slate.com/articles/news_and_politics/cover_story/2016/04/the_fight_for_the_future_of_npr_can_public_radio_survive_the_podcast_revolution.html)
and it\'s a worthy competitor. Podcasts exploded onto the Internet when
audio recording tools became standard on cell phones and audio editing
became easy consumer-level tasks because of streamlined software. Add to
that, near ubiquitous fast internet and podcasts, which can be
specialized and targeted like almost no media before it, have become the
media source for news and entertainment for many younger, more tech
savvy consumers. Big money has moved in for sponsorships and big names
from other entertainment circles, like Marc Maron and Kevin Smith, are
betting big on the future of the medium. The core question, as the
article states, is if hard news can really work in a podcast context.
That question remains unanswered.\
\
**Rock en Español**\
There\'s long been a rock music scene in Mexico that was influenced by
American sounds but generated its own take on what was happening up
North or even in Europe. Two bands that I\'ve loved from that scene are
Caifanes and Café Tacvba. Caifanes was heavily influenced by the New
Wave and maybe even Goth sounds that were coming out of England as [this
song, \"Mátenme Porque Me
Muero,\"](https://www.youtube.com/watch?v=7ZVfWt7uYjc) from their
self-title first album shows clearly. It\'s easy to chuckle a bit at the
Cure-influenced look of the band and what seems on the surface a
derivative sound. However, listen to the rhythm section a bit and that
latin influence comes through. Lyrically, it is decidedly goth and I\'m
not culturally astute enough to know how those lyrics played to the
contemporary Mexican ear. The band Café Tacvba takes a different
approach by starting with an evolving, unique sound which assimilates
foreign influences for political and creative purposes. Their album,
*Cuatro Caminos,* exhibits [ballads like
\"Eres\"](https://www.youtube.com/watch?v=98Akpf1ph2o) alongside
[electro-punk like \"Cero y
Uno.\"](https://www.youtube.com/watch?v=dVaM0UL0j3M) Clearly, this is a
band with some things to say and I find them endlessly fascinating.\
\
**I Bet We Can Win**\
Campaign finance in the United States is a complete disaster. The
Citizens United ruling has stripped away all sanity around funding
candidates election bids. Now,
[this](http://www.motherjones.com/politics/2016/02/sheldon-adelson-macau-casinos-lawsuit)
*[Mother
Jones](http://www.motherjones.com/politics/2016/02/sheldon-adelson-macau-casinos-lawsuit)*
[article](http://www.motherjones.com/politics/2016/02/sheldon-adelson-macau-casinos-lawsuit)
seems to draw a line from Chinese gangsters to several conservative
candidates through the Macau casinos of Sheldon Adelson. Whether the
money given to SuperPACs or candidates is actually money being spent by
corrupt Chinese officials in shady offshore casinos is beside the point
because it\'s clear that this is but one example of what could happen
given the current laissez-faire attitudes towards regulating campaign
finance. Our elections are being bought on all sides and we who can\'t
buy our elected officials shouldn\'t stand for it.\
\
**Confidence is Smooth**\
McClain Sullivan exhibits a wry but not sardonic perspective in her
lyrics. I appreciate the self-referential humor and the mostly laconic
delivery. Her combination of jazz influences with pop and rock sounds
works to create what seem like small songs seem larger and more open
than they might be as more straight jazz. Witness the growing dynamics
and tasteful horns [on a song like \"Cool
Enough\"](https://www.youtube.com/watch?v=pDmhi2Ua0fM) which also has a
downbeat bridge/ending that makes the song not ironic but instead
solemn. There\'s more of this on [her new
album,](https://open.spotify.com/album/6FuA1kMZ565ekxEaEmtXmI)
*[Rachel](https://open.spotify.com/album/6FuA1kMZ565ekxEaEmtXmI)*.\
\
**Beauty Reigns**\
I\'m just about done reading [Alastair
Bonnett\'s](https://www.goodreads.com/book/show/18222723-unruly-places)
*[Unruly Places: Lost Spaces, Secret Cities, and Other Inscrutable
Geographies](https://www.goodreads.com/book/show/18222723-unruly-places)*
which details places on our Earth that exist outside of time or human
influence, or have been lost or erased, or else choose to remain in a
space between spaces. The book is an introduction to the idea of
psychogeography. While I expected it to be sensationalist and Indiana
Jones-y, the book strives to show how geography and the notion of place
are deeply rooted in our conceptions of beauty, order, and stability.\
\
**R.E.M. Song of the Week**\
Diffuser has [a nice write-up
about](http://diffuser.fm/rem-murmur-anniversary/?trackback=twitter_top_flat_4)
*[Murmur](http://diffuser.fm/rem-murmur-anniversary/?trackback=twitter_top_flat_4)*
which was released 33 years ago this week. I [always loved the song \"We
Walk\"](https://www.youtube.com/watch?v=RWgTv9TvZys) which has a
sing-song, bouncing guitar that echoes something out of the 1950s but
there\'s a hint of the obscure with Stipe singing about Marat\'s bath.\
\
\--\
\
Thanks again for reading. Have a wonderful weekend. Consider
[pre-ordering Pylon\'s first official release in 20
years](https://chunklet.bandcamp.com/album/gravity-b-w-weather-radio).\
\
Alex
