# I miss your smell

## A weekly newsletter from Alex Ezell

\
I never think about any sort of theme to these newsletters. However,
after writing this one there seemed to be a subtext here about dualities
and paradoxes. Like a dream, I don\'t know what to make of that but
maybe it speaks to something in the ether around me.\
\
\--\
\
**The Words for What We Are**\
Words have unlimited power to heal, harm, create, destroy. They are
building blocks of nearly every human culture still in existence. Even
those cultures without written languages, maybe especially those, kept
their cultures alive through the use of words. So, [Douglas Harper\'s
essay Mother Tongues](http://slavenorth.com/columns/sanskrit.htm) does a
wonderful job illuminating the poetry which can be be found in that gap
of understanding between words of languages unknown to you. Human
languages are rife with crudities and beauties in equal measure. Our
language is maybe the best reflection of our being.\
\
**Flip a Coin**\
I suspect Shooter Jennings vacillates on a regular basis between trying
to evade his father\'s (Waylon Jennings) shadow and embracing his
heritage. To my ear, [his most recent album Countach (For
Giorgio)](https://open.spotify.com/album/2WoZWIKwqh8MTk0yISiH0a) is a
nod to both of those states. Starting with a sample of his father\'s
Don\'t You Think This Outlaw Bit\'s Done Got Outta Hand which morphs
into a synthesizer meltdown, the album makes its way to a cover of The
Neverending Story theme song. It\'s a departure for sure but quite
welcome in my collection.\
\
**All That I Am**\
Mumford and Sons seem to have fallen out of favor with the folks that
originally raised them up. It seems like a similar backlash that
occurred with Dave Matthews. At any rate, [this new collaboration with
Baaba Maal called There Will Be
Time](https://www.youtube.com/watch?v=eCIHPdx1OAs) is a great song that
is equal parts emotional and danceable. In a time when the word
appropriation is thrown around with abandon, this collaboration seems
more genuine than not for if nothing else it is delivered with passion.\
\
**The Center Cannot Hold**\
With news that recent college graduates are faced with less job
prospects than any generation of graduates before, fights in the culture
about social safety nets and minimum wage, the weakening of unions
across the country, and a society hell-bent on voting based on the
manufactured rage of the week, it\'s no wonder than [the struggle of the
middle class is disappearing from our entertainment
world](http://www.nytimes.com/2016/05/01/magazine/tvs-dwindling-middle-class.html).
We watch the rich and the poor battle it out in \"reality\" TV or we
transport ourselves to fantasy lands that little resemble our own. We
seem to crave only what lies at the extremes. What we don\'t appear to
have time for is any kind of mirror that might reflect what we\'ve
lost.\
\
**Of a Place, But Not That Place**\
If I mention that a band is from New Orleans, you\'ve already heard them
in your head. Probably some funky groove, maybe some horns, definitely
some non-4/4 time happening, likely a melding of quite a few other
sounds. Well, a band called Motel Radio basically sounds nothing like
that. More like an indie band from southern California than anything
near the delta. Their [recent single \"Gimme Your
Love\"](https://www.youtube.com/watch?v=F7bbtAr2snY) is a shimmery,
layered love song driven by a bouncy synth line and some smooth guitar
work. It will be interesting to hear what the full album sounds like
when it\'s out soon.\
\
**Where Does Duty End**\
As I\'ve gotten older, my nascent claustrophobia in childhood has grown
more and more severe. The idea of scuba diving makes me cringe and
seppuku would be preferable to spelunking. So, to imagine scuba diving
in a deep, dark cave is what my nightmares are made of. This [story from
the BBC](http://www.bbc.com/news/magazine-36097300) tells of a team of
divers who encountered disaster while cave diving and then defied any
logic I can come up with to revisit the site of their tragedy. These
folks are made of sterner stuff than I.\
\
**Art is Imitation**\
I\'ve never been to Scotland. I know a few people from there and they
are both warm and aloof at the same time. Maybe it\'s a Scottish trait
to be so. The Scottish band Lional wears their Joy Division influences
on their sleeves but there\'s something else [in their debut single
\"Season of Salt\"](https://www.youtube.com/watch?v=fMOsFrByQes) that
maybe recalls their Jesus and Mary Chain countrymen. While those
influences battle each other, there\'s a pop sensibility here that\'s
approachable and attracting.\
\
**Rock the Role**\
Lydia Loveless defies all your preconceived notions when you see her
picture on an album cover. Yes, she plays country-influenced rock but
she\'s from Ohio. Yes, her last name is Loveless but she\'s not related
to Patty Loveless. Yes, she\'s an attractive woman but that\'s not why
she\'s on the cover. Yes, she writes literary, heart-felt songs but she
also writes about being drunk and horny. This [live version of \"I
Really Wanna See You\"](https://www.youtube.com/watch?v=bqnze8LpE5Y)
captures a lot of the fascinating things she does. Be sure to [check out
her most recent
album](https://open.spotify.com/album/15UfMA8VC7IxY6z6CbRsMN) as well
which features [this great song \"Wine
Lips\"](https://www.youtube.com/watch?v=xf_-nsaoAnQ).\
\
**Gems in the Rough**\
In the late 90s and early 00s, music genres like emo, screamo, and
hardcore burst into the public eye from a long underground of post-punk,
post-grunge wailing. This music was, as its name implied, emotional in
the fullest sense of that word. Full of longing, regret, love, loss, and
lots of anger, once the music saw the bright light of day, it blew up
into pop-punk and was quickly co-opted by the money machines of the
music business until it was recognizable. There came a steady stream of
inferior later bands and albums and the genres themselves become black
marks on a band\'s bio. Still, as NME recognizes in their [list of 20
emo albums that still
work](http://www.nme.com/photos/20-emo-albums-that-have-stood-the-test-of-time/367692/),
there was some great music in that scene and that time.\
\
**Beauty Reigns**\
My friend Tate reminded me recently of my Red House Painters obsession I
had in college. [This song Trailways is absolutely
stunning](https://www.youtube.com/watch?v=FjlxG0WU22w) and has been in
my head all week.\
\
**R.E.M. Song of the Week**

Most folks give short shrift to 2004\'s *Around the Sun* and probably
for good reason since even the band says they were bored with the
material and the band itself. However, [the song \"Leaving New York\" is
the best of the bunch](https://www.youtube.com/watch?v=tCvnGxfBfiw) and
echoed some of the other R.E.M. gems.\
\
\--\
\
Thanks for subscribing. Have a great weekend!\
\
Alex
