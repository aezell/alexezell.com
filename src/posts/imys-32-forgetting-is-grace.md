I put this newsletter on a holiday hiatus back just before Christmas.
I'm glad I didn't have the pressure to try to respond with any kind of
intelligence over the last few weeks. Frankly, it's been mostly anger
interspersed with gallows humor. That's just where I am with...all of
this...

My friend, Geoff, wrote this morning about how many computer systems use
a "logical" delete which doesn't actually delete anything but instead
simply hides it. It's all still sitting there ready for revelation. That
got me to thinking about the grace that is forgetting.

While I might well remember the feeling I had when someone said
something or even did something particularly mean to me, it's difficult
for me to conjure the exact words used. Even if I try to think
specifically about that moment, over time the sharp edges dull and what
might have invoked fury is now maybe just mild discontent. This is the
power of forgetting.

We now live in a world where nothing is forgotten. Think of the stories
where tweets made a decade ago resurface to dog someone. Those
utterances are then elevated to the same level of import and meaning as
anything being said in the moment. I'm not bemoaning people being held
to account for racist or sexist positions. In fact, this lack of
forgetting is probably helping to educate a lot of people about what is
acceptable and what isn't. They are probably also learning the dangers
of simply putting every thought they have out into the world. You reap
what you sow.

In my work, not only do we not delete things but we preserve a
decades-long history of who said it and when. Sure, there's a large
portion of anonymity but the record still exists. Since we deal in
trying to record the sum of all human knowledge, this kind of hoarding
seems important. The folks at Internet Archive are engaged in similar
efforts. There's a paradox here that on the Internet nothing ever
disappears yet we have two very large organizations trying to save all
the stuff that's disappearing.

So, there must be some other force at play. It seems to me that it's a
lack of grace. The old books fade into formats no one can access. The
personal pages full of the stuff of culture fall apart as technology
moves on. But, we humans make sure that the petty stuff of bickering and
mouthing off with our worst intentions are preserved and relitigated
over and over again at length. This is the nature of what we choose to
save and what we choose to let decay.

That's an awfully damning conclusion. The evidence seems clear though.
We have a chance to set in our collective memories those moments of
grace, compassion, growth, and vulnerability. We even have leaders among
us who can take us there. We have to choose to follow.

<div>

------------------------------------------------------------------------

</div>

## A Little Kindness

This *New Yorker* profile of Shigeru Miyamoto highlights that dreamers
still exist and that kindness can still be something more than a
charitable, selfless pursuit. When asked what he might change about how
world, Miyamoto says, "I wish we were all a little more compassionate in
these small ways. If there was a way to design the world that
discouraged selfishness, that would be a change I would make." It gets
to the heart of the matter about many of us seek to collect and to
gather as opposed to give and to share.

[Read about the creator of Super
Mario](https://www.newyorker.com/culture/the-new-yorker-interview/shigeru-miyamoto-wants-to-create-a-kinder-world){.button
.primary}

## Christ Among the Children

::: {#youtube2-_ewojHMEdXQ .youtube-wrap attrs="{\"videoId\":\"_ewojHMEdXQ\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=_ewojHMEdXQ), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

The amazing art-pop band, Love Tractor, recently reissued their first
self-titled album in a beautiful vinyl version. Produced by R.E.M.'s
Bill Berry, the album often shows up in your favorite band's list of
favorite albums. The video above is for a song called "Christ Among the
Childen" from a later album, *The Sky at Night*. I celebrate their
entire catalog. If you love that Athens sound of the early 80s, you
will, too.

[Check out the Love Tractor
album](https://lovetractor.bandcamp.com/album/love-tractor){.button
.primary}

## Get it Back

We all the know the horrors visited upon the native tribes of the
Americas by the white Europeans who came here. In addition to murder,
enslavement, and rape, the white settlers also systematically defrauded
the native tribes of their land. Through theft or fraudulent contracts,
land was "bought" or "reclaimed" as the tribes were continually
relocated. I grew up learning about a white-washed version of the Trail
of Tears. Well now, using the contractual means originally employed
against them, some tribes are buying back their land and rebuilding
their nations.

[Read about reclaiming stolen
land](https://www.nationalgeographic.com/history/2020/12/dna-of-this-land-cherokee-quest-reclaim-stolen-territory/){.button
.primary}

## Why does beauty exist?

Robin Wall Kimmerer's book *Braiding Sweetgrass* is a classic of
ecological thinking and you should read it. This article and the radio
piece it accompanies give a nice intro to the topics that Kimmerer
studies and writes about. She is certainly one of the foremost
scientists reframing our attachment to the natural world. I love this
simple sentence, "We speak a grammar of animacy." How easy it can be to
acknowledge the agency of the world we inhabit.

[Listen to Robin Wall
Kimmerer](https://www.cbc.ca/radio/tapestry/why-is-the-world-so-beautiful-an-indigenous-botanist-on-the-spirit-of-life-in-everything-1.5817787){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

That's all I can muster for this week. I'm still reeling. If you want to
get poetry, other ramblings, and more music, buy a subscription. Paid
subscribers get bonus stuff.

[Subscribe now](https://imissyoursmell.substack.com/subscribe?){.button
.primary}

Have a great week!

Alex

## The Orb Weaver

::: {.captioned-image-container}
![An orb-weaver hanging out on our barn. by Alex
Ezell](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/a357f276-2f39-4a17-82dc-657ffadd384b_635x801.jpeg)
:::
