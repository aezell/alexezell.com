This week's edition comes to you from Stockholm, Sweden. I've been here
for nearly a week already and it's very quickly become one of my
favorite cities. I'll need to revisit in the winter to see if my
feelings stay the same. There's a lot going for the city not the least
of which is the genuine friendliness of the people. Many people speak
perfect English but will beam a big smile if you can manage a few words
in Swedish. At that point, they open up and want to know more about you
and share more about their home. I recommend a visit before everyone
else discovers this vibrant and beautiful place. If nothing else, we
could learn a lot from
[the](https://en.wikipedia.org/wiki/Coffee_culture#Sweden)
*[fika](https://en.wikipedia.org/wiki/Coffee_culture#Sweden)*.

<div>

------------------------------------------------------------------------

</div>

## The Canopy Connects

The research might be a touch dubious, however, there's a nugget of
truth hiding in [this article which tells us about how trees actually
make us
healthier](https://www.citylab.com/environment/2019/07/urban-tree-canopy-green-space-wellbeing-research/595060/).
I've shared plenty of articles about how trees anchor and protect
ecosystems and how they affect mental health. One interesting conclusion
in this particular study is that trees specifically, more than just
greenery or nature, do something to human beings that makes us happier
and healthier. Is it the sense of protection they provide? Is it
something biological?

## Nashville's Reggae Blues

Aashid Himons passed away over eight years ago but I heard his name for
the first time this week thanks to my friend Josh who does [a radio show
at the wonderful
WXNA](https://www.wxnafm.org/shows/loveless-in-nashville). He was a
prolific musician with a creative perspective that was singular and yet
welcoming. He combined soul music with the mountain music of West
Virginia roots. [He brought the blues to
reggae](https://www.nashvillescene.com/news/article/13037748/remembering-aashid-himons-19422011-the-man-who-brought-reggae-to-music-city).
Eventually, he collapsed it all into space music that gave him the space
to experiment and weird out. His most famous work was with the group he
put together called Afrikan Dreamland. We are lucky to have the whole
1981 album, *[Jah
Message](https://www.youtube.com/watch?v=5SbYZgUbLyE)*, on Youtube to
enjoy. Dig deep into this uniquely American sound.

::: {#youtube2-5SbYZgUbLyE .youtube-wrap attrs="{\"videoId\":\"5SbYZgUbLyE\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=5SbYZgUbLyE), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

## Let's Go Plogging

Sweden has a wealth of natural beauty. The people here in Stockholm are
serious about disposing of trash in the least impactful way possible.
There are no less than five different containers anywhere I throw
something away. It makes sense then that the Swedes would come up with
Plogging or [the act of jogging/running while picking up
trash](https://spark.adobe.com/page/i4iKpksF47Qkm/). Ideally, we'd live
in places where this kind of thing wasn't even possible because trash
would get to where it should be. But, there's something delightfully
silly and corny about turning your next jog into a neatening up session.

## It's A Trap

In 1968, William F. Buckley, Jr. convinced [Jack Kerouac to appear on
his chat show](https://www.youtube.com/watch?v=oaBnIzY3R00). It was a
trap because counter-culture voice Ed Sanders of the band The Fugs and
the academic Lewis Yablonsky were also there unbeknownst to Kerouac
until he arrived. Kerouac is clearly in very poor form during the show
but his wit and piercing clarity is compelling through the sad spectacle
of his drunken decline. The discussion feels a bit artificial but
Buckley does a great job managing the personalities in his distinct way.
During the conversation, Kerouac disparages Allen Ginsberg several
times. Yet, [Ginsberg defended Kerouac and his
behavior](https://www.youtube.com/watch?v=_zAW02FmLiY) in a much later
interview. If nothing else, these two videos might make you want to
revisit *On the Road* since you haven't read it since high school and
neither have I.

## The 1619 Project

The New York Times has released [an ongoing project to collect essays,
photos, poems, and other creative means of discussing, processing, and
facing Americas
roots](https://www.nytimes.com/interactive/2019/08/14/magazine/1619-america-slavery.html)
as a nation built by and on the backs of black slaves. I've not read all
of the essays yet but I will be watching as they post new ones and
working my through the entire project as it is released.

## What is it about LA?

I've never been to Los Angeles. I know a few people who live there and
they all seem to love it. However, the story in my head is that it's not
a place anyone actually likes living. That's clearly wrong and probably
tainted by the sensational media coverage of things like the "gang wars"
and Rodney King's assault. It's interesting how that coverage might have
colored a city's reputation for decades to come. Makes me wonder what
will happen with Detroit. Anyway, there's a [new album from LA-based The
Regrettes](https://www.youtube.com/watch?v=FoX8YjQCR5I) and this single
makes me think that if music like this comes from LA, it can't be all
bad.

::: {#youtube2-FoX8YjQCR5I .youtube-wrap attrs="{\"videoId\":\"FoX8YjQCR5I\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=FoX8YjQCR5I), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

## The Shelves are the Soul

Y'all know that I love books. I think a lot about how books affect our
lives, our society, and I think maybe they even chart the course of our
existence. I'm that person that on my first visit to your house walks
over to your bookshelves and quickly catalogs all your choices. This
short essay shares [a look at what books are being read right now in
Germany](https://www.the-american-interest.com/2019/08/03/what-germans-are-reading/)
and how they are changing the public discourse and possibly creating
social neuroses where maybe none actually exists. I'm intrigued to think
that books could be affecting such a large, modern society driven by an
exploding economy. Such nations are often said to be above the arts,
that is, operating in some technocratic utopia of managed markets and
managed madness. Can the written word truly turn that tide?

## R.E.M. Song of the Week

Yeah, I know I forgot this last week. You sent me exactly no emails
letting me know. 1988's *Green* was the first R.E.M. album I ever bought
with my own money. However, *Document* was the first R.E.M. album I ever
had and its hold on my heart hasn't wavered since then. This week's tune
is a live version of "Welcome to the Occupation" recorded during the
rehearsal shows in Dublin, Ireland. Michael's plaintive "Listen to me!"
at the end of the song echoed loudly in my 12-year-old heart.

::: {#youtube2-g7tdQEgpi94 .youtube-wrap attrs="{\"videoId\":\"g7tdQEgpi94\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=g7tdQEgpi94), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

<div>

------------------------------------------------------------------------

</div>

Next week's edition will be winging to you from the home base back in
the US. Encourage your friends and family to sign up. If you have
feedback, let me know.

Your guide,

Alex

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/5e0fd0e7-bc1f-4e23-9dca-55fd56c53efb_4032x3024.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F5e0fd0e7-bc1f-4e23-9dca-55fd56c53efb_4032x3024.jpeg){.image-link
.image2 .image2-825-1100}

*The Storsand beach on the island of Utö in the Stockholm archipelago.*
