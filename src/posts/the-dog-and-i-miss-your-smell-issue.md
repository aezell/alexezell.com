# I miss your smell

### A weekly newsletter from Alex Ezell

\
We\'ve had a resurgence of Winter in these parts. Expecting snow
tomorrow after a week or so of warmth and sunshine. It\'s a dastardly
return to the frigid, the shriveled, and the gray. There\'s still
brightness to be found. See below.\
\
**The Sailor and the Okie**\
Sturgill Simpson has a new album coming out. It seems like it\'s going
to be another example of his personal songwriting matched with the
cosmic mystic buoyed by a variety of styles with clear roots in the
honesty of country music. You can [check it out on NPR right
now](http://www.npr.org/2016/04/07/473237504/first-listen-sturgill-simpson-a-sailors-guide-to-earth).
Simpson continues to be one of the more interesting artists working
today. Hailed as the alternative to alternative country (whatever that
means) Simpson appears to be determined not to deliver that which is
expected of him. He continues to challenge himself and listeners. It\'s
refreshing and enlightening music. Merle Haggard died on his birthday at
79 years old. Garden & Gun magazine [printed this interview between
Merle Haggard and Sturgill
Simpson](http://gardenandgun.com/article/legend-and-renegade). A timely
piece that strikes me with how much they have in common despite their
milieus being light years apart.\
\
**I See, I Hear, I Am**\
I was enthralled with Paul Bogard\'s book, *[The End of Night: Searching
for Darkness in an Age of Artificial
Light](https://www.goodreads.com/book/show/16131044-the-end-of-night)*,
as Bogard attempted to trace our modern loss of darkness. Bogard
combines scientific, technological, spiritual, and personal viewpoints
as he laments the creeping destruction of the darkness that we fear and
crave. At times lyrical, Bogard takes the reader chapter by chapter to
the darkest parts of the world. It\'s a descent back in time as well as
back to a seemingly more natural state. I especially appreciated his
passages describing Thoreau\'s walks in the woods at night. I was
recently recommended George Prochnik\'s book, *[In Pursuit of Silence:
Listening for Meaning in a World of
Noise](https://www.goodreads.com/book/show/7322998-in-pursuit-of-silence),*
which tackles the aural aspect of Bogard\'s lament on the visual.
Prochnik uses the personal stories of several people to make his point
that our loss of silence is one of the world\'s greatest environmental
catastrophes.\
\
**Unadorned and Stunning**\
Dori Freeman\'s voice is at once reminiscent of an earlier time while it
has tinges of the modern. With vulnerability in tension against power,
Freeman [will carry you away on her new self-titled
album](https://open.spotify.com/album/1fNeNleD7ljfyW9IDnymut). The
lead-off track, [\"You
Say,\"](https://www.youtube.com/watch?v=R5wS01SM22g) is tender and
bewitching while the album\'s last track, [\"Still a
Child,\"](https://open.spotify.com/track/7DP4bSuiL4UnQokyjVmISf) tells a
darker story of love. This kind of music is so rarely heard on any radio
station but it\'s precisely the sort of unassuming, lovingly-made music
that reminds me that mostly music is art and not commerce. Her voice is
so naturally attracting that it deeps into that instinctual notion of
beauty that we all carry inside.\
\
**Too Much Sugar for a Nickel**\
My Dad always said that something that was probably too good to be true
or a little too much to handle was \"too much sugar for a nickel.\"
Sugar and the soft drinks that are weighed down by it fall in to that
category. There\'s more and more good evidence for us to keep up the
pressure on our culture to make sugary soft drinks the cigarettes of a
new age. This [article from Nature points out the amount of
water](http://www.nature.com/nature/journal/v526/n7571/pdf/526034a.pdf)
that\'s used to make soda. Combine that horrendous waste of our fastest
dwindling resource, clean water, with [this piece from The Guardian
about how sugar\'s deleterious nutritional effects were known 40 years
ago](http://www.theguardian.com/society/2016/apr/07/the-sugar-conspiracy-robert-lustig-john-yudkin)
but were ignored by the majority of scientists at the time. I certainly
am rethinking allowing our son to drink any of these drinks. It\'s been
years since I had anything other than water to drink. I can\'t say I\'ve
missed it once I got over the initial hump.\
\
**Blues For Our Times**\
If you grew up with a father who traveled the South cataloging folklore
and folk music but had a yearning to revisit traditional forms and
tropes, you might become a musician that sounds a lot like Jake Xerxes
Fussell. On his [self-titled album released in
2015](https://open.spotify.com/album/4OG9vUqtYKQHjCA1CHFcri), Fussell
finds new veins to mine musically and lyrically through the deep roots
of blues on [songs like \"Let me
Loose.\"](https://www.youtube.com/watch?v=E3qzpcwsMA4) With a name like
Xerxes, a man must be fairly earnest and deft and Fussell does not
disappoint. This appears to be Fussell\'s first full-length album. It
hints at so many possibilities as he might continue to incorporate new
styles and instrumentation. With a grounding in solid American roots, it
seems to me that given the chance, Fussell could do some very
interesting work indeed.\
\
**Are We Participating?**\
Journalist Gay Talese made statements recently that many interpreted as
him disparaging female journalists or perhaps more strongly stating that
they simply didn\'t matter to him. However we parse what we said, he\'s
an idiosyncratic write whose [recent piece, \"The Voyeur\'s Motel,\" in
New Yorker magazine about his pen pal correspondence with a peeping
tom](http://www.newyorker.com/magazine/2016/04/11/gay-talese-the-voyeurs-motel)
is odd and off-putting and full of ethical and moral ambiguity. In
short, it makes for an example of what Talese has done for decades. At
times infuriating, beguiling, and impeccably accomplished, Talese\'s
writing has been oft-emulated. Perhaps another question for us of
whether we can appreciate the art while not elevating the artist.\
\
**Two Unrelated Things**\
I used to listen to the [Hoodoo Guru\'s \"Blow Your
Cool](https://open.spotify.com/album/47CRqx6c9sIWmzpM5GXFYG)\" on
cassette tape in my Sony Walkman while I was cutting the grass as a kid.
[Black Mountain\'s new album
\"IV\"](https://open.spotify.com/album/1iRr9SxJtbenO0gygNltV7) likely
requires a bit more concentration to really digest and I don\'t think
it\'s been released on cassette tape, yet.\
\
**Beauty Reigns**\
This song has taken over my world in the last few days. A single release
from Mitski\'s new album, [\"Puberty
2,\"](https://mitski.bandcamp.com/album/puberty-2) please [take a listen
to \"Your Best American
Girl.](https://www.youtube.com/watch?v=BjGB9hc5huk)\"\
\
**R.E.M. Song of the Week**\
Everyone remembers \"Losing My Religion\" and \"Shiny Happy People\" but
[the raw sound of \"Country
Feedback\"](https://www.youtube.com/watch?v=0CYitiDJPTE) is a stand-out
track from *Out of Time*. Michael has occasionally said this is his
favorite R.E.M. song.\
\
\--\
\
Have a great weekend. Thanks for reading.\
\
Alex\
