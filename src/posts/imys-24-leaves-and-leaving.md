From my desk here at the new house, I have a narrow but enchanting view
of the woods surrounding our back pasture. If I lean a little to the
left, I can see out to the side pasture. Each view is framed or
backgrounded by trees.

My interest in trees is no surprise to my regular readers. You'll see
more of this when the novel comes out. Trees help me make a narrative
that orders my world in the way I'd like it to be. Strong, mostly quiet,
collaborative, and slow. Always biding time until the fad passes or the
ennui fades.

This time of year, those trees are shedding their leaves. The birds and
squirrels that call them home are leaving or planning to leave. We are
in the middle of a migration path for a number of finch species. Our
trees have been safe and comfortable overnight roosts for the finches
these last few weeks. Even that pattern has slowed to just a few birds
here and there.

Time is slowing as the days get shorter. The horses in the pasture are
fattened against the leaner winter. The world is retracting and leaving
space for all our human intrusions. I have a feeling that this winter,
we'll all be in a hibernal pause moreso than ever before. Find a good
book and dig out your favorite albums. Settle in to the peace.

<div>

------------------------------------------------------------------------

</div>

## Patterns of Gaia

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/628c02bc-49a5-4d2a-8017-e5d12d133694_460x276.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F628c02bc-49a5-4d2a-8017-e5d12d133694_460x276.jpeg){.image-link
.image2 .image2-276-460}

I'm a fan of brutalist architecture. Imagine the concrete monoliths
built in Europe in the 50s and 60s. They are nearly anti-human in their
visible function and building materials. This combined with a scale and
texture that is clearly denatured. At the same time, I celebrate the
architecture of nature (trees, obviously) but the great mountains of the
world and the landscapes both widescale and minute. With these two
potentially opposing ideas in mind, this research into biophilic
patterns in design points how we might be able to marry patterns found
in nature with the functional and physical needs of our living and
working places. More than that, art might lie in that intersection.

[Read the 14 Patterns of Biophilic
Design](https://www.terrapinbrightgreen.com/reports/14-patterns/){.button
.primary}

## Gold in the Morning Sun

::: {#youtube2-MDt9bPCPBGk .youtube-wrap attrs="{\"videoId\":\"MDt9bPCPBGk\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=MDt9bPCPBGk), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

I contend that outside of some modern composers and some ambient music,
very little contemporary music could be said to melodically beautiful.
To my ear, modern music tends toward melodies that are catchy or that
provide a tension echoing the anxieties of our time. I was pleased
earlier this week to catch this Don Williams song on the radio. One of
the purest, most beautiful melodies I've ever heard. The lyrics are a
bit sardonic and loveworn. The melody though is spun sugar and light
that forces you to pay attention lest you miss some capital-T Truth.

[Listen to \"I\'m Just a Country
Boy\"](https://www.youtube.com/watch?v=MDt9bPCPBGk){.button .primary}

## Walk on By

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/a1392eff-9136-4446-ab84-b015f3ad6d86_650x494.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fa1392eff-9136-4446-ab84-b015f3ad6d86_650x494.jpeg){.image-link
.image2 .image2-494-650}

In the UK and parts of Scandinavia, there is an ancient "right to roam"
which lets anyone walk across nearly any piece of land. In open areas,
that includes going off any path or track that might already exist.
Landowners typically provide ways to pass through or over fences to
ensure this access. To someone living in the US, this sounds insane
given our focus on property rights and trespassing. Still, it makes for
a rich heritage of walking through nature or simply to get from one
place to another. Slow Ways is an organization working to identify, map,
and comment on all the ways to walk from place to place in the UK.
There's already an excellent network of paths and this effort to codify
them is quite welcome.

[Tread the Slow Ways](https://slowways.uk/){.button .primary}

## No More Troubled Water

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/dd5c44fc-592c-4bd6-873b-116f85dca60f_2798x1856.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fdd5c44fc-592c-4bd6-873b-116f85dca60f_2798x1856.jpeg){.image-link
.image2 .image2-966-1456}

###### `Jennyzibreva, CC BY-SA 4.0, via Wikimedia Commons`

I visited Prague quite a bit last year for work. It's a beautiful city
if a bit Disneyfied in its touristy areas. That said, the Charles Bridge
still stuns as it crosses the Vltava River right in the center of the
city. What's more impressive is that it was built during a time when
there was no power equipment. It was the purest application of
engineering principles in the same way that the great vaulted ceilings
of cathedrals reach for perfection. This animation shows some of the
techniques behind how those medieval engineers accomplished the feat.

[Build the Charles
Bridge](https://www.youtube.com/watch?v=nJgD6gyi0Wk){.button .primary}

## Biscuit in the Basket

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/43be8e28-17d1-49be-8407-1baaa79c84b3_1024x576.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F43be8e28-17d1-49be-8407-1baaa79c84b3_1024x576.jpeg){.image-link
.image2 .image2-576-1024}

###### `From `[`Journal of Nomads`](https://www.journalofnomads.com/kok-boru-dead-goat-polo-kyrgyzstan/)

Sportsball of various kinds is the single largest pasttime in the entire
world. This wasn't true in ancient times. However, there's more evidence
now that ball games in Eurasia were more widespread earlier in history
than previously thought. Central to these games was the player's
horsemanship. We've all heard about the great Mongol hordes conquering
enormous swaths of Asia and Europe on top of their small, fast, and
fearless ponies. How better for them to stay in shape and to practice
than to compete with each other?

[Ancient horse ball games in
Eurasia](https://www.media.uzh.ch/en/Press-Releases/2020/Horse-Riders-.html){.button
.primary}

## Lovesick

::: {#youtube2-YLoUDI7waWQ .youtube-wrap attrs="{\"videoId\":\"YLoUDI7waWQ\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=YLoUDI7waWQ), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Lydia Loveless (no relation to Patty) recently released an amazing new
album named, "Daughter." While the title track is certainly a good song,
the cut "Can't Think" is one I keep coming back to. It's lyrics simplify
the complicated power that love can have over us as it turns to
obsession and maybe even into sublimation of the self. The crescendo of
the song echoes that creeping awareness of loss that the worst kind of
love tends to find.

[Listen to \"Can\'t
Think\"](https://www.youtube.com/watch?v=YLoUDI7waWQ){.button .primary}

## The Hell of Good Intentions

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/a946db50-3ea4-4339-8f79-d65eb3b33c34_662x1000.webp)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fa946db50-3ea4-4339-8f79-d65eb3b33c34_662x1000.webp){.image-link
.image2 .image2-454-300}

The author Max Brooks made his name with the novel, *World War Z*, which
was later made into a Brad Pitt film. The structure of that book, with
its firsthand accounts sprinkled with news and research reports, holds
up in a more epistolary form in his new novel, *Devolution*. This time,
Brooks looks at a supposed Sasquatch attack that occurs at a locked down
ecovillage near Mount Rainier. There is social satire aplenty even as
the terror and scares ramp up. The pace is relentless and the
inside/outside narration by the main character is a fascinating device
to create mystery and tension. Highly recommended for the Halloween
season.

[Read Max Brooks\'s
\"Devolution\"](https://bookshop.org/books/devolution-a-firsthand-account-of-the-rainier-sasquatch-massacre/9781984826787){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

Thanks for reading this week. Pass this along to your friends and
family. Post it on Facebook, Twitter, whatever. More readers means my
typos get caught faster.

Your guide,

Alex

## Banyan Tree, Fort Lauderdale, FL

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/6128c09c-0743-48ae-8d6c-ed88109f2586_4032x3024.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F6128c09c-0743-48ae-8d6c-ed88109f2586_4032x3024.jpeg){.image-link
.image2 .image2-1092-1456}

###### `By Alex Ezell`
