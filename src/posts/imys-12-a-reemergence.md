## Hello again

Welcome to the reemergence of I Miss Your Smell. As a reminder, this is
a newsletter that I hope to send to you each week. In it, you'll find
articles to read, music to listen to, books to consider, and hopefully
at least one thing that makes you think a bit more deeply about the
world we live in.

Thanks for joining me. Let's dive in.

<div>

------------------------------------------------------------------------

</div>

## Do you like American Music?

Sometime around 1985, my friend and I got in trouble for listening to
the Violent Femmes song, [Blister in the
Sun](https://www.youtube.com/watch?v=JE-dqW4uBEE), in his mom's pudding
brown Suburban. She'd taken the tape away from us multiple times but we
kept dubbing copies from our buddy. The music was more novel than I
understood then with its combination of punk aesthetics and acoustic
instrumentation. Later, I fell in love with their cover of Culture
Club's [Do You Really Want to Hurt
Me?](https://www.youtube.com/watch?v=Jmsj67QpYZI). The band reunited a
few years back and have released their second album since that
reformation. You should check out the first single and title track,
[Hotel Last Resort](https://www.youtube.com/watch?v=Jc14JPwnp6M). Gordon
Gano is still an idiosyncratic lyricist and they've expanded the
instrumentation but held on to that punky, acoustic vibe.

## The Invisible Story

I've been circling around this idea of narrative as an organizing force
not just for our human endeavors but for all of existence. I haven't
nailed the idea yet but I'm coming across writers and artists who seem
to be channeling some of what I'm grasping for. One of those is Richard
Powers's latest novel, *[The
Overstory](https://www.goodreads.com/book/show/36075657-the-overstory).*
While it doesn't read all that well as a novel, in that the plot is a
bit on the nose, it stands as an excellent book that achieves many of
its other aims with powerful precision. The book tells us of a number of
people caught up in and reacting against our modern condition who
eventually find or recognize the role that nature, specifically trees,
play in our story. That is, our human story.

## The Podcast Bubble

Not a day goes by that someone doesn't recommend a podcast to me. It
doesn't even matter that many of my interests line up with the sweet
spot of podcast content. There are now thousands of podcasts about
nearly any subculture, hobby, fad, movement, or interest one could
imagine. However, with great abundance often comes great waste. The New
York Times tried to capture some of [the struggles at the edges of the
podcast
boom](https://www.nytimes.com/2019/07/18/style/why-are-there-so-many-podcasts.html).
Podcasts just don't work with my brain or the way I listen, work, or
relax. I don't foresee them going away anytime soon but I do wonder if
the gold rush is over.

## Athens Music Art School

Many of you know that I'm an R.E.M. fan. In fact, each of these
newsletters has an R.E.M. song at the end for your prescribed weekly
dosage. Some of you know that Athens, GA was, is, and, hopefully, will
be a place that generates a lot of great art, especially music. The
B-52s were one of the bands that exploded out of Athens around the same
time as R.E.M. but they were distinctly different from that band. This
well-done, interactive [New York Times piece about their
influences](https://www.nytimes.com/interactive/2019/07/15/arts/music/b52s-influences-playlist.html)
walks through each song of their 1979 debut album and examines its
influences. It'd be really cool to see a similar article tracing the
songs and bands that were later influenced by these B-52s songs.

## Eiderdown and Civilization

The world is ripe with stories of how human gathering, hunting, farming,
and mining have changed and in many cases destroyed our natural world.
This story about [harvesting Eider down in
Iceland](https://www.theguardian.com/world/2019/jul/19/eiderdown-harvesting-iceland-eider-duck)
starts off as a story of humans coexisting and maybe even helping a wild
creature in a symbiotic relationship. But, as expected, the story turns
and we find the mistaken blunderings of man trying to tame and manage
nature in a way that simply never works. It also touches on how
modernization and scientism and urbanization are affecting these ancient
ways of life.

## The Wheels of Forgiveness

This year's edition of the Tour de France just ended. In an era where
the sport is trying to put behind it the disaster of Lance Armstrong,
the US media has found it expedient to include Armstrong in their
coverage of the race. This Columbia Journalism Review piece provides [a
brief overview of the problems with
Armstrong](https://www.cjr.org/analysis/lance-armstrong-tour-de-france.php)
continuing to play a role in cycling media. Should all be forgiven in so
far as we should continue to enrich this man who not only cheated but
destroyed the careers and lives of many around him? How long is long
enough before he's paid his debt? Would it be ignorant of us to ignore
his experience and expertise? I suspect these are the kinds of questions
that each person has to answer for themselves. There's no single "right"
response. Maybe it boils down to each of our ideals around the notion of
punishment as opposed to suffering. It would seem some prefer the
latter.

## Touring Coal Country

I live in Southwest Virginia which has a rich history of coal mining. It
also has the faded company towns that have traced the declining fortunes
of that industry. However, some towns and counties in the region are
finding ways to develop new sources of jobs, revenue, and happiness. In
Damascus, Virginia, this year's edition of [Trail
Days](http://traildays.us/), which celebrates all the Appalachian Trail
thru-hikers, was a grand success. In perhaps a more interesting turn of
events, there's a [road in Smyth and Tazewell counties, called the Back
of the
Dragon,](https://www.roanoke.com/news/local/back-of-the-dragon-breathes-new-life-into-far-southwest/article_b2638fb9-b85e-5243-b304-a9b8abdbc168.html)
that is drawing more and more driving enthusiasts. They come on
motorcycles. They come in sports cars. The key is that they come and
they bring money with them. The tourists flocking to the road are
enabling a downtrodden community to rediscover its spirit. If you pay
close attention, you'll see that the magic of the road is the natural
scenery around it.

## Beauty and Wonder

David M. Haskell is a writer and biologist who has been trying to share
the story of nature with us through a couple of books. This piece about
[birdsong and how it connects us to another
world](https://emergencemagazine.org/story/the-voices-of-birds-and-the-language-of-belonging/)
from Emergence Magazine uses text and audio and beautiful illustrations
by [Obi Kaufmann](https://coyoteandthunder.com/) to place birdsong back
into our awareness. As insulated as we are from the outside world, this
is a welcome tug on our attention. I sit in front of a window staring at
trees and the occasional bird yet I remain separate. This piece has
reminded me to seek out that connection and, even more importantly,
become a conduit for its message.

## R.E.M. Song of the Week

Much has been written about Michael Stipe's seemingly inscrutable
lyrics, especially from those early albums. So, I find it endearing that
on 1991's *Out of Time* the band included [this subdued, mostly
instrumental chamber pop piece called
"Endgame](https://www.youtube.com/watch?v=Q3dBqHJ8SFo)." Yes, Stipe does
provide some sort of doo-wop kind of vocalizations, but the focus is on
the melody plucked across a guitar and echoed in a string arrangement.
It came in the middle of the album, belying its title, and was followed
by "Shiny Happy People" of all things. Somewhere, the band is laughing.

<div>

------------------------------------------------------------------------

</div>

Thanks for joining me this week. I hope this is a welcome arrival in
your inbox. It makes me really happy to share these things with all of
you. If you [visit the website](https://imissyoursmell.substack.com/),
you can comment on this issue and read all previous issues.

Your guide,

Alex

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/2bec1550-9c46-456e-9905-c41b0acbe454_1000x750.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F2bec1550-9c46-456e-9905-c41b0acbe454_1000x750.jpeg){.image-link
.image2 .image2-750-1000}

*Sunset in San Diego*
