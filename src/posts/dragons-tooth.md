---
title: Dragon's Tooth - A Sleep Story
description: A sleep story about a hike on Dragon's Tooth
date: 2020-12-20
tags:
  - sleep-stories
layout: layouts/post.njk
---
It's non-stop holiday preparation over here. Putting the rub on the pork butt. Debating about how to do the prime rib. Counting how many pieces of pie every one will eat. Wrapping presents and running out for last minute "[tampons and Marlboro Lights.](https://youtu.be/oqN483jm6JE)"

https://www.youtube.com/watch?v=0Mf-MXo8CT4

In the midst of all that, I managed to write and record this sleep story. I had no idea that sleep stories were a thing until I signed up with the Calm app (still not sponsored). This one describes a gentle hike up to Dragon's Tooth along the Appalachian Trail near where I used to live.

I figured I'd try my hand at a story with some calm elements, repetition, and lots of sense description. It's also just the second time I've used my recording setup and the first YouTube video I've created from whole cloth.

All that to stay that this is an experiment. Let me know if you think more of these would be something you'd like to listen to.
