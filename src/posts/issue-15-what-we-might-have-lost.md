I'm writing to you from my home office looking out over the cul-de-sac
where I live. I feel a small rumble in my feet from the bass of the
music playing down at the university campus. The students are back. Our
little town has doubled in population almost overnight. The tree-covered
mountains in the distance register none of this. They seem more focused
on the scar being hewn across their belly by the Mountain Valley
Pipeline. Flying in a few days ago, I saw the knife swipe of destruction
like a rift through paradise. Progress.

If you are enjoying this newsletter, please ask your friends to
subscribe by sending them to this website:
[https://imissyoursmell.substack.com](https://imissyoursmell.substack.com/)

<div>

------------------------------------------------------------------------

</div>

## Swamp N' Roll

If you haven't heard the music of the band Shinyribs, let today be that
day. The band is the genius of Kevin Russell. Russell used to lead the
revered Texas band The Gourds, often known for their cover of Snoop
Dogg's "Gin and Juice." But, this new band takes those East Texas roots
and mixes them with some swamp boogie along with a heavy dose of wry
surrealism and humor. Musically, the band's sound has grown from the
first album to include a deft horn section, accomplished keys, and
bedrock background vocals. Don't let the fun groove and silliness blind
you to the accomplished lyrical flourished. [This is a live version of
"Take Me to Lake Charles"](https://www.youtube.com/watch?v=PlCBStUTsHs)
that shows off the spirit of the band.

::: {#youtube2-PlCBStUTsHs .youtube-wrap attrs="{\"videoId\":\"PlCBStUTsHs\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=PlCBStUTsHs), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

## A Fire on our Deep History

With the advent of the Internet's seemingly unending storage that's
backed up and duplicated, the idea that any sort of media can be lost
forever seems like a figment of the past. However, the Universal company
owned and stored hundreds, thousands of master recordings of TV shows
and musical recordings. The New York Times Magazine [recently published
a report about a 2008 fire that destroyed many of these
recordings](https://www.nytimes.com/2019/06/11/magazine/universal-fire-master-recordings.html).
As you'll read, the company held closely the true depths of the
disaster. Only now are we beginning to understand how our history was
lost. Many of these things will be lost forever and some of them we
won't even know that we've lost them. The list of [destroyed recordings
continues to
grow](https://www.nytimes.com/2019/06/25/magazine/universal-music-fire-bands-list-umg.html).
Forgetting is a great gift we humans are given but in this case,
forgetting feels more like mourning.

## It's Hard. Can It Be Harder?

Every year, thousands of people start a trek at the border with Mexico
(or Canada) to walk the entire Pacific Crest Trail. The Internet abounds
with videos and blog posts written by those on the trail, successful or
not. [This view of the trail from space is especially
interesting.](https://earthobservatory.nasa.gov/features/pacific-crest-trail)
There are myriad reasons for a hiker's success or failure. Now, [there's
a restaurant with a cheeky take on whether they are supporting the
hikers or hindering
them](https://www.outsideonline.com/2401060/pacific-crest-trail-restaurant-morgans-diner).
The idea is to use temptation of the most base kind, that of hunger, to
make this PCT challenge just a little bit harder. It's all done in good
fun. I had an idea for an ultramarathon with a similar mechanic. The
idea would be a 100 mile race on a loop. Starting at about 75 miles, the
race would offer to refund some of your race fee if you quit. The value
of that refund would grow for each mile you run after that until at mile
99, you'd basically get all of your money back. But, of course, you'd
get a DNF (Did Not Finish). Maybe one day I'll give this a shot.

## Strangers Wherever I Go

A few issues ago, I wrote briefly about the death of the musician David
Berman. Many better writers than I published obituaries and remembrances
as well they should have. I confess to not having read many of them.
However, [this obituary from Jacobin struck a chord with
me](https://jacobinmag.com/2019/08/david-berman-silver-jews-death-obituary)
as being appropriately respectful and honest insofar as my experience of
Berman's music goes.

## Pop Eats the World

I still remember hearing that song "The Middle" for the first time. Why
wouldn't I? It's undeniable. The guitar riff alone immediately grabs
you. However, I never gave much thought to the band behind the song. It
turns it that in the ensuing years, Jimmy Eat World have become one of
my favorite bands. They consistently turn out pop music that's
well-crafted and engaging. There's the occasional thematic of lyrical
surprise but mostly it's strong songwriting and knowing production that
builds their appeal. [Here's another song from the
album](https://www.youtube.com/watch?v=GCOO5ZlUfvU) *[Bleed
American](https://www.youtube.com/watch?v=GCOO5ZlUfvU)* but I encourage
you to check out *Clarity* as well.

::: {#youtube2-GCOO5ZlUfvU .youtube-wrap attrs="{\"videoId\":\"GCOO5ZlUfvU\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=GCOO5ZlUfvU), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

## Long Enough That We Forget What We Forgot

One of [my favorite
T-shirts](https://peteroren.bandcamp.com/merch/goodbye-to-this-world-shirt)
is one made by the musician Peter Oren which he made to sell along with
his most recent album
*[Anthropocene](https://peteroren.bandcamp.com/album/anthropocene)*.
It's the name of our supposedly new geological epoch and as the *anthro*
bit suggests, it represents an epoch where the geology and climate of
the Earth are fundamentally changed by humans. However, when [looked at
on the right time scale and without the ego that we humans generally
lean on, the Anthropocene is barely even an event in geological
time](https://www.theatlantic.com/science/archive/2019/08/arrogance-anthropocene/595795/)
much less an actual epoch. This article does a great job of explaining
just how long these timeframes are and how small, infinitesimally small,
our time on this Earth really is. Now, does it mean humans aren't
affecting the Earth profoundly? I don't think it does. What it tells me
is that we are likely only engaging in bringing about our own mass
die-off but that any long-term, geologically long-term, change we've
wrought will be invisible in the historical record.

## Symbolism Aflame

The fire at Notre-Dame was a few months ago. The repairs will continue
for years. The psychic damage to the heart of a nation will last longer.
It's likely hard for Americans to understand how central that building
is to a French person's, especially a Parisian's, identity. I don't know
that a similar monument exist in the U.S. Even the non-religious of
Paris and France revere the artistry of the building and its contents.
It's location near the center of the capital city further bestows on it
some mystical force of centrality in mind and spirit. [This New York
Times article describing how close the building was to
disaster](https://www.nytimes.com/interactive/2019/07/16/world/europe/notre-dame.html)
does several things really well. It uses its medium, the web, perfectly.
It also tells a story quite simply. This telling detail reveals the
truth: when the last ditch plan to save the building was discussed, one
team refused to do it, so another did instead. It was literally do or
die.

## Sounds Like Poison

Vinyl records are making a comeback. We've all read the articles and
seen records popping up in Barnes and Noble, Target, and even Walmart.
[However, as Benn Jordan describes, these slabs of vinyl hold not just
incredible amounts of nostalgia and tactile joy but also quite a
downside](https://www.youtube.com/watch?v=aZ2czFuIYmQ). Not only are
they environmentally problematic but they might also represent a health
hazard. Preferences about sound quality or listening habits aside, the
simple fact that the material itself is poisonous is enough for me to
consider getting rid of all of my albums.

::: {#youtube2-aZ2czFuIYmQ .youtube-wrap attrs="{\"videoId\":\"aZ2czFuIYmQ\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=aZ2czFuIYmQ), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

## The Road Home

I suspect dogs are the best example of man living and working with
animals. Sure, we ride horses and use ox as beasts of burden. We have
cats as ornaments. [This Helen Macdonald review of the
book](https://www.the-tls.co.uk/articles/public/pigeon-treat-helen-macdonald-jon-day/)
*[Homing](https://www.the-tls.co.uk/articles/public/pigeon-treat-helen-macdonald-jon-day/)*
[by John Day shows us Day's case that pigeons, specifically racing
pigeons, have a lot tell us about our connections to the natural world
and to each
other](https://www.the-tls.co.uk/articles/public/pigeon-treat-helen-macdonald-jon-day/).
I've not read the book but this excellent review pulls at my attention
with its discussion of the concept of home and getting home when we
might otherwise be lost. Isn't that most of what we do as adults? Try to
get back home? Try to make a home where we are? And then fill that home
with family in whatever form it takes. The almost magical ways these
pigeons find their home seems like an aspirational drive I'd like to
mimic.

## R.E.M. Song of the Week

*[Reckoning](https://en.wikipedia.org/wiki/Reckoning_(R.E.M._album))* is
a beloved album from R.E.M.'s earlier years. With the distinctive
production of Mitch Easter, it retains the feel of those heady early 80s
college rock albums but hints at a slightly different path for the band.
The lead track, "[Harborcoat](https://youtu.be/8oC6pxjjGHw)," elevated
the background vocals of bassist Mike Mills while also featuring his
strong bass work. The guitars feel punchier and the whole song has a
vibrancy and immediacy that was missing from the earlier *Murmur*. Below
is the album track but here's [a live version from the German TV
show](https://www.youtube.com/watch?v=Sehg7gEbgyU)
*[Rockpalast](https://www.youtube.com/watch?v=Sehg7gEbgyU)* that shows
off how crucial Bill Berry's drums are. I think it also illustrates how
the recording worked to capture this live feel more than on *Murmur*.

::: {#youtube2-8oC6pxjjGHw .youtube-wrap attrs="{\"videoId\":\"8oC6pxjjGHw\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=8oC6pxjjGHw), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

<div>

------------------------------------------------------------------------

</div>

That's it for this week. I already spy some nice pieces for next week's
issue. As always, thanks for subscribing and reading.

Your guide,

Alex

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/7988cb36-04ee-4142-a0ea-9db3d298eca6_2792x1914.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F7988cb36-04ee-4142-a0ea-9db3d298eca6_2792x1914.jpeg){.image-link
.image2 .image2-754-1100}

*Buckingham Fountain in Chicago. Turning the fountain on marks the
official first day of summer in the city. This was that day.*
