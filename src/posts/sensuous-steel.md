::: {.captioned-image-container}
![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/cb051f1a-6a4d-4801-b477-516fe88b9d40_1000x520.jpeg)
:::

A few years back, I was living in Nashville and had the chance to visit
an exhibit at the Frist Center for the Visual Arts titled, "[Sensuous
Steel](https://nashvillearts.com/2013/06/sensuous-steel-at-the-frist-center/)."
It was an exhibit of Art Deco cars dating predominantly from the 1930s.
These are beautiful machines whose style certainly makes them worthy of
high art.

I was inspired that day to write a poem about how those cars made me
feel. That exhibit was in 2013. In 2015, Jason Isbell released his
album, *Something More Than Free* with the song, "Hudson Commodore." In
it, he mentions the car of the title and the Delahaye 135. I have to
believe that we were both struck by this same exhibit. His song is
admittedly better than my poem. However, we were both responding to how
this luxury and beauty came about in the midst of the Great Depression.

::: {#youtube2-yQr5LDMR_gA .youtube-wrap attrs="{\"videoId\":\"yQr5LDMR_gA\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=yQr5LDMR_gA), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

He told a story of the car's representation of freedom. I was more
focused on the sensual response I had to the cars. Any object of beauty
should inspire a multitude of responses both cerebral and physical. I
won't claim to share much artistic space with Jason Isbell but on this
day, we were beset by the same muse.

Below is the poem I wrote that day, "An Afternoon with a Delahaye."

### An Afternoon with a Delahaye

###### By Alex Ezell

###### 4 July 2013

##### **A zephyr of mercury avoiding inertia**

##### **Liquid speed begging to be touched**

##### **Dodging air as it moves with a shape**

##### **Like a grounded Spitfire in love**

##### Two thousand pounds that you can

##### Never quite grasp in your hands

##### Beauty expressed in sensuous steel

##### Begs to be adrift in a sea of adrenaline

##### Wrapped in luxury without bounds

##### While the Depression grows great

##### As dreams go in the fuel tank

##### And passion grasps the steering wheel

##### Practicality lost the battle to the sensate

##### Needs gave way to desires

##### Science found expression in art

##### And inspiration still tumbles from the exhaust

<div>

------------------------------------------------------------------------

</div>

Did anyone else see this exhibit?

Your guide,

Alex
