[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/8371ffba-a36f-4af6-b934-37bde3622f97_718x470.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F8371ffba-a36f-4af6-b934-37bde3622f97_718x470.jpeg){.image-link
.image2 .image2-470-718}

Hi y'all,

I'll make this short and sweet. I'd like to get the I Miss Your Smell
newsletter back up and running on a regular cadence.

I wanted to give y'all a heads up. You can certainly unsubscribe via the
links at the bottom of this email.

I hope you'll stick around and join me for the next issue coming
tomorrow.

Alex
