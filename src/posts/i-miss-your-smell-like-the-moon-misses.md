# I Miss Your Smell

## A weekly newsletter from Alex Ezell

Thanks for indulging me last week as I commandeered this newsletter to
tell my personal story about Prince. We\'re back to our normal format
this week. I hope you enjoy it and have a great weekend.\
\
**Death Rides a Rat**\
I remember studying the Bubonic Plague, or Black Death, in grade school
and it seemed that many folks were afraid of catching the disease from
those who were already ill. In [this fascinating piece on History
Today](http://www.historytoday.com/ole-j-benedictow/black-death-greatest-catastrophe-ever),
the case is made that the Bubonic Plague was an insect-borne disease and
more specifically, those insects were carried to and fro within large
Black Rat populations. Seldom was the disease spread as Pneumonic Plague
which could be mildly contagious via the breath of an infected human. It
also seems that the growing global trade was an important catalyst for
the spreading of this disease. There\'s a metaphor in there somewhere.\
\
**An Eater\'s Guide to Waffles**\
I\'ve written about Sturgill Simpson\'s new album in a few of these
newsletters. His [performance on Colbert\'s Late
Show](https://www.youtube.com/watch?v=sz7cBnlhhG4&feature=youtu.be)
shows what a great band he has. While his songs tend to be more literary
and poetic, Simpson clearly [has a strong sense of irony and humor as
evidenced by his new Waffle House
song](https://www.youtube.com/watch?v=yHe_1lOJYN0).\
\
**A Knowing Smile**\
While I won\'t admit to ever doing what [Charlie Mars recommends in his
song, \"Listen to the
Darkside,\"](https://www.youtube.com/watch?v=PtEXBhs2Vtw) I do know that
this song has a solid groove. The simple, wry lyrics are offset by some
interesting phrasing. There\'s a [similar effect at work on his newer
song, \"The Money.\"](https://www.youtube.com/watch?v=SwaJUWf_Iho) These
are honestly simple songs that seem to have a sense of humor masking a
deeper uncertainty.\
\
**Your Car Doesn\'t Have to Be Christine to Kill You**\
On the heels of the revelation that Volkswagen has been cheating
emissions tests for years, there\'s [recent news that the vast majority
of diesel vehicles are polluting far
more](http://www.theguardian.com/business/2016/apr/23/diesel-cars-pollution-limits-nox-emissions)
than they claim. Whatever the outcomes of investigation into these
claims, whether leaders of these companies knew, and/or leaders of the
testing agencies knew, what\'s clear is that greed driven by the
unregulated capitalism our society continues to support has no interest
in preserving that what maintains it, the customer. These companies
appear to not understand that killing your customer is bad business.
Tell that to the cigarette and alcohol companies.\
\
**Yeah, But Have You Heard This**\
I\'m a sucker for an esoteric music discovery, so [this list of \"100
Lost Albums You Need to
Know\"](http://www.nme.com/list/100-lost-albums-you-need-to-know/371352#UR0jDjsR0rcZ7dOr.01)
is reading fodder for days. Props for including [The
Go-Betweens](https://www.youtube.com/watch?v=-mY4CRzIKag) and their
delightfully goofy college rock, [The Red
Devils](https://www.youtube.com/watch?v=BOAVSK7VhYk) featuring some
blistering harmonica, the non-Breakfast Club [Simple
Minds](https://www.youtube.com/watch?v=5W3AlLVqQPE), and some live [Bad
Brains](https://www.youtube.com/watch?v=cnVRuH4vJWg) which I would know
nothing about if it weren\'t for the Rouse brothers.\
\
**Your Brain on Nature**\
What happens to our bodies, specifically our brains, when we spend time
in nature? Why do some many of use feel comfort not just from nature but
even from pictures or videos of nature? Is it some primal instinct? This
[short video from National
Geographic](http://video.nationalgeographic.com/video/ng-live/thys-le-nadkarni-brain-nature-nglive)
is an introductory companion piece to [this article about the science of
what nature does to our
brains](http://ngm.nationalgeographic.com/2016/01/call-to-wild-text).
For me, as a more recent convert to trail running, I\'ve found
noticeable differences in my mood, energy levels, and outlook from even
a brief walk down a tree-lined path. I\'m lucky enough to work in an
office where I can look out the window at a tree-covered ridge and I
find that a few minutes simply absorbing it allows me better focus. I
don\'t doubt their science one bit.\
\
**Bubblegum with Glass**\
Sylvain Sylvain came to prominence as the guitarist in the
famous/infamous New York Dolls. When that band dissolve, he released a
couple of solo albums that seem to collapse styles from the American
canon into a stew of 50s jukebox bop, soul, and rock. It wasn\'t a giant
departure from what the New York Dolls were doing but Sylvain Sylvain
greatly diminished the punk influence that the Dolls were wearing. As
evidenced in tracks like [\"Teenage
News\"](https://www.youtube.com/watch?v=QbjjbHRGxHs) and [\"Every Boy
and Every Girl\"](https://www.youtube.com/watch?v=lG8fw0gF85s) Sylvain
Sylvain is mining a purely American vein with occasional glimpses of the
dark hedonism the Dolls might have been trying to capture but with a
more innocent sheen.\
\
**Beauty Reigns**\
Julien Baker is a young artist from Memphis, TN whose self-titled album
came out last year. On that album, she [has a song called \"Sprained
Ankle\"](https://www.youtube.com/watch?v=wmGVIvf8Q6s) that puts her
clear, vulnerable voice to great use while extolling the challenges of a
person finding their voice, their place, their meaning in this world.
It\'s a short, yet haunting song that recalls a universal pain. You
might also check out [this video of a live
performance](https://www.youtube.com/watch?v=ME1gGWsK9rE) in
Nashville\'s Percy Warner Park where they are normally racing horses.\
\
**R.E.M. Song of the Week**\
1991\'s *Out of Time* was one of R.E.M.\'s largest commercial successes
largely on the heels of the global single \"Losing My Religion.\" Known
as the album where Peter discovered the mandolin, my favorite track off
the album has always been the [mandolin and harpsichord driven \"Half a
World Away.\"](https://www.youtube.com/watch?v=AWiBIrPMEWk)
Incidentally, this album was produced by Scott Litt who also helped
produce the Sylvain Sylvain solo albums mentioned above.\
\
\--\
\
Thanks for subscribing. May you have a walk in the woods and discover
peace.\
\
Remember there\'s an [archive of these dispatches on the web
here](http://tinyletter.com/imissyoursmell/archive).\
\
Alex\
