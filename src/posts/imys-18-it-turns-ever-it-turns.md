The heat outside wouldn't let you know it but Fall is coming. The trees
know. I see yellow and orange leaves popping up on the trees outside my
window. Chipmunks and squirrels hurry about each morning to fill their
larders for the long winter. Flocks of birds wing overhead headed to all
points south. The wheel turns and the sun's rays hit us at different
angles.

We are a part of this cycle. We can change our clocks. We can turn on
our electric lights. Our hearts know that this is a time to slow down.
Pull family close. Turn our pursuits and our thoughts inward. Many of us
fill the winter season with stressful holidays and family get togethers.

Maybe this year, we can acknowledge the wisdom of the plants and animals
around us and take a little better care of ourselves.

Thanks for joining me. I hope you enjoy this week's issue.

Send your friends and family to this link to sign up:

[Sign up now](https://imissyoursmell.substack.com/subscribe?){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

## Blowing Smoke

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/a640dc63-d1e5-4a43-afc4-1eef42881ece_3000x2317.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fa640dc63-d1e5-4a43-afc4-1eef42881ece_3000x2317.jpeg){.image-link
.image2 .image2-850-1100}

###### By Russell Lee - U.S. National Archives and Records Administration, [Public Domain](https://commons.wikimedia.org/w/index.php?curid=17048405)

If you work for someone, there is both an explicit contract and an
implicit agreement that they will pay you for that work. This social
contract is as old as dirt. It is the trust on which societies and
cultures have rested. Exchanging one's labor for pay is almost
instinctual. However, this story from Kentucky shows an example of what
happens when that contract is broken. It's not clear how this story ends
but one of the interesting nuggets within is that the coal industry,
nationwide, is only 53,000-plus jobs. For the people working in them,
they are of vital importance just like any one of our jobs is important
to us. But think about the political wrangling and cultural
identification that's happening around just 53,000 jobs. Walmart employs
1.5 million people in the US. The fight for their labor rights is
ongoing but is dwarfed by this romantic association with the coal
worker. For the people I know who've worked in coal or had family
members working in coal, the only answer is always, "It's the only thing
we know." They could come to know something else if we invested in those
programs.

[Read about the Harlan County
Blockade](https://www.newyorker.com/news/dispatch/the-battle-for-a-paycheck-in-kentucky-coal-country){.button
.primary}

## Family Definition

::: {#youtube2-RNgGX_8Vn2A .youtube-wrap attrs="{\"videoId\":\"RNgGX_8Vn2A\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=RNgGX_8Vn2A), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

This song from Benjamin Tod works through the complexities of family
that isn't quite family. Many of us have those people in our lives. The
family we chose instead of the family we were born to. Sometimes, those
bonds are stronger than blood. Tod is a member of the Lost Dog Street
Band but [has a solo album from 2017](https://benjamintod.bandcamp.com/)
that's quite good. The songs are confessional and sometimes
uncomfortable in their rawness. His voice is at times clear and strong
like a creek tumbling down a holler. At other times, there's a gravel
strain that belies the tough times Tod's lived in the past.

[Listen to \"We Ain\'t Even
Kin\"](https://www.youtube.com/watch?v=RNgGX_8Vn2A){.button .primary}

## The Obvious Hypocrisy

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/869e4545-570b-4205-8ca2-8a2adb5089f2_2048x1365.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F869e4545-570b-4205-8ca2-8a2adb5089f2_2048x1365.jpeg){.image-link
.image2 .image2-733-1100}

###### By [Shealah Craighead](https://www.whitehouse.gov/1600daily) - [Public Domain](https://commons.wikimedia.org/w/index.php?curid=58933284)

My friend pointed out that the new HBO show "The Righteous Gemstones"
seems like a fictional punchup of what Politico says is happening within
the Falwell Ministry. Some of us are old enough to remember Jerry
Falwell crying on TV when he caught stealing money, fornicating, or
lying or whatever it was he did while telling all of us not to do it.
It's the easiest joke to make. But, [what's happening with Falwell's son
at Liberty
University](https://www.politico.com/magazine/story/2019/09/09/jerry-falwell-liberty-university-loans-227914)
is no joke. I've driven by the school many times in recent years. It's
growing at a breakneck pace. Millions and millions of dollars are going
into the campus and presumably the pockets of those involved. Now, Jerry
Falwell, Sr. is seeing cracks in the walls of silence and obfuscation he
and his family have built. Insiders are either tired of the deception or
tired of not getting a big enough slice of the pie. This is the story
anyone could see coming and Politico covers it in crushing detail.

[Read about the problems at
Liberty](https://www.politico.com/magazine/story/2019/09/09/jerry-falwell-liberty-university-loans-227914){.button
.primary}

## Power and Presence

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/f0ccb254-69ff-4aa9-8305-127ccb3551ac_1207x733.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Ff0ccb254-69ff-4aa9-8305-127ccb3551ac_1207x733.jpeg){.image-link
.image2 .image2-668-1100}

###### Shamefully stolen from eBay

In 1987, Tina Turner visited Albany, GA on the "Break Every Rule" tour.
It was the first concert I ever went to. It was the first time a girl
ever kissed me. I'm not sure it's fair to say that I kissed her back. I
mostly just stood there stunned. I don't remember anything in particular
about the show except for Tina herself. A shimmery, shaking gold
minidress and those legs. She owned the stage, then the people on the
floor, and then the entire arena. It took thirty seconds before we were
all in thrall. I loved every minute of it. It still stands in my mind as
the best concert I've ever seen. So, I'm happy to hear that Tina is
living exactly how she wants on the shores of Lake Zurich. She still
seems to have the sense of humor, the poise, and the power that has been
admired and copied for decades. That we should all have the chance to be
exactly who we want to be.

[Marvel at Tina
now](https://www.nytimes.com/2019/09/09/theater/tina-turner-musical.html){.button
.primary}

## There's Fast, There's Fearless

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/3a1cb5d8-e410-4796-a1f6-163b35ac38a3_2908x2092.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F3a1cb5d8-e410-4796-a1f6-163b35ac38a3_2908x2092.jpeg){.image-link
.image2 .image2-791-1100}

###### By Sérgio Valle Duarte - Own work, [CC BY 3.0](https://commons.wikimedia.org/w/index.php?curid=51433128)

I have been known to run long distances. It may be behind me now but I
once ran over 40 miles in a single day. For me, it was a slow jog, a
slightly faster, jog, and then a death march. But, for someone like Zach
Bitter, that 40 miles is just getting warmed up. [Bitter recently broke
the record for the fastest 100 mile
run](https://www.runnersworld.com/news/a28819230/zach-bitter-100-mile-world-record/).
He covered that 100 miles in 11 hours, 19 minutes, and 13 seconds.
That's an average of 6 minutes and 47 seconds per mile. I probably can't
run a single mile that fast. Most of us probably can't run a single mile
that fast. But, Bitter did it for 100 miles. Given that's the average,
many of those miles were a good deal faster than that. While the elite
marathoners try to break the two hour mark, records for the longer
distances keep falling dramatically. Bitter's team beat the old record
by over 10 minutes. I'm excited to see how close humans can get to the
limit for long distances like this.

[Read more about Bitter\'s
record](https://www.runnersworld.com/news/a28819230/zach-bitter-100-mile-world-record/){.button
.primary}

## The Skull

::: {#youtube2-yBSIyN1XuRo .youtube-wrap attrs="{\"videoId\":\"yBSIyN1XuRo\",\"startTime\":null,\"endTime\":null}"}
::: {.iframe}
::: {#player}
:::

::: {.player-unavailable}
# An error occurred. {#an-error-occurred. .message}

::: {.submessage}
[Try watching this video on
www.youtube.com](https://www.youtube.com/watch?v=yBSIyN1XuRo), or enable
JavaScript if it is disabled in your browser.
:::
:::
:::
:::

Saint Cecilia is the patron saint of musicians. Music festivals have
been held in her honor since at least 1570. It makes sense then that a
woman-fronted band from Los Angeles which blends many types of latin and
western music would name themselves [La Santa
Cecilia](http://lasantacecilia.com/). While the song "Calaverita" is a
fun and romantic tune, other songs of theirs bend to the political and
social commentary. Throughout it all, lead singer Marisol Hernandez's
voice is strong, supple, and has an emotional depth that pulls you in
even if you don't understand the Spanish words.

[Watch the Calaverita
video](https://www.youtube.com/watch?v=yBSIyN1XuRo){.button .primary}

## Only 999 Stories in the City

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/5465b732-c209-481a-9e5f-5a5da747ffb2_2000x1218.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F5465b732-c209-481a-9e5f-5a5da747ffb2_2000x1218.jpeg){.image-link
.image2 .image2-670-1100}

###### By ynes95 - originally posted to Flickr, [CC BY-SA 2.0](https://commons.wikimedia.org/w/index.php?curid=79465924)

U.S. cities have long been a draw for people not just within the country
but from all over the world. It is how we have multicultural melting
pots of humanity. In many ways, those cities like New York have defined
both our American ideals and our American anxieties. Now, [the Atlantic
tells us that the shape of those cities is
changing](https://www.theatlantic.com/ideas/archive/2019/09/americas-three-biggest-metros-shrinking/597544/).
On the whole, people are leaving those cities. It's more complicated
than that so it might be more fair to say that the growth of those
cities is plateauing or perhaps just changing, swapping out one group of
people for another. To be sure, the cities are always in flux. These
trends move in cycles like most other. It is curious to contemplate
though a rise of the mid-tier city. Some place like Nashville or Austin
or Dallas or Charlotte or Atlanta. Those kind of cities might be
catching whatever outflux is happening from the large metro areas. How
will the cultures shift in all of these places as new people arrive and
others leave?

[Read about our Shrinking
Metros](https://www.theatlantic.com/ideas/archive/2019/09/americas-three-biggest-metros-shrinking/597544/){.button
.primary}

## The Gentleman Dissenter

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/7b8e0431-c1d1-435b-b107-69d488e5aa6f_640x480.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F7b8e0431-c1d1-435b-b107-69d488e5aa6f_640x480.jpeg){.image-link
.image2 .image2-480-640}

###### By Guy Mendes - Guy Mendes, [CC BY-SA 3.0](https://commons.wikimedia.org/w/index.php?curid=29726407)

I've written about Wendell Berry before. He's a poet, essayist,
novelist, and farmer. He has a peculiar brand of agrarian dissent. [The
Nation lays out a perspective on his stances across a variety of
subjects](https://www.thenation.com/article/wendell-berry-essays-library-of-america-review/).
Some of Berry's views are paradoxical to others that he's written at
length about. Sometimes, Berry has changed his mind or buried in nuance
a topic on which he'd previously been clear-eyed. In short, Berry is
human and he is at his best when responding to events in the moment.
While much of his writing and thinking is on a long timeline that a
farmer needs to think about, he also was an activist in and of the
moment. This article does a good job of being honest about Berry's views
in their human complexity. It does away with the open-mouthed, nodding
head, back to the farm apologists and looks at how his views, taken as a
whole, might be summarized.

[Read about Wendell Berry\'s
dissent](https://www.thenation.com/article/wendell-berry-essays-library-of-america-review/){.button
.primary}

## The Stories We Tell

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/30e6d905-91f9-46a3-8342-977ec7ba368d_1280x857.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F30e6d905-91f9-46a3-8342-977ec7ba368d_1280x857.jpeg){.image-link
.image2 .image2-736-1100}

###### By Hrald - Own work, [CC BY-SA 3.0](https://commons.wikimedia.org/w/index.php?curid=6967806)

Martin Shaw is a writer, philosopher, and naturalist, among other
things. In this interview, [he digs deep into how the myths that we tell
ourselves contain the intelligence we've
collected](https://emergencemagazine.org/story/mud-and-antler-bone/)
over centuries on this planet. It's in some way an evocation of the
power of oral history. It's also a recognition of an idea I've been
circling around for years, which is the possibility that narrative,
simply stories, are the defining construct of our human existence.
Stories are the way we make sense of, categorize, remember, structure,
connect, and share all that we know and experience. I think Martin would
agree with that and this interview is a novel way to get at the idea

[Listen to Martin Shaw tell a
story](https://emergencemagazine.org/story/mud-and-antler-bone/){.button
.primary}

## R.E.M. Song of the Week

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/721dcb5a-f180-41d9-8115-101ff48144d2_2016x1512.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F721dcb5a-f180-41d9-8115-101ff48144d2_2016x1512.jpeg){.image-link
.image2 .image2-825-1100}

###### By Coast Guard Air Station Clearwater, [Public Domain](https://commons.wikimedia.org/w/index.php?curid=81877708)

Hurricane Dorian recently devastated the Bahamas. R.E.M. have released
the song "Fascinating" to help raise funds for relief efforts. The song
was originally recorded for the 2001 *Reveal* album. It has never been
officially released. The song does some of my favorite things that
R.E.M. was getting into in the later years like combining digital
production tricks with simple piano melodies and even string and
woodwind parts. Michael Stipe is in his sweetly vulnerable state here.
At any rate, all the proceeds go to support Mercy Corps' work helping
support the people of the Bahamas. I also excited to see R.E.M.
embracing Bandcamp. We need more bands on that platform.

[Buy R.E.M.\'s \"Fascinating\"](https://remhq.bandcamp.com/){.button
.primary}

<div>

------------------------------------------------------------------------

</div>

Try to create something this weekend. Write a poem or a song. Paint a
picture. Build a fence. Re-arrange a room. Put something new into the
world.

Your guide,

Alex

[![](https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/deeca18c-902c-4abc-9e00-78659f4e1670_4032x3024.jpeg)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fdeeca18c-902c-4abc-9e00-78659f4e1670_4032x3024.jpeg){.image-link
.image2 .image2-825-1100}

###### By Alex Ezell - Clouds Above Us
