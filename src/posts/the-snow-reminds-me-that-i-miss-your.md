Welcome to the second issue of \"I miss your smell.\" Thanks for your
continued interest. If something here intrigues you forward this to a
friend and [encourage them to sign
up](http://tinyletter.com/imissyoursmell).\
\
**My (and your) Favorite Band**\
Many of you who know me know that my favorite band is R.E.M. I\'ve been
in love with this band since I first heard \"Life\'s Rich Pageant\" at
age 9 or 10. I\'ve pored over every album since then. My greatest regret
is that I\'ve never seen an official live R.E.M. concert. I\'ve seen
them do secret shows or sets at other people\'s shows. It\'s with some
relish then that I read [Annie Zaleski\'s reappraisal of R.E.M.\'s \"Out
of Time\"
album](http://www.salon.com/2016/02/25/r_e_m_s_leap_of_faith_25_years_later_every_curveball_on_out_of_time_makes_perfect_sense/).
I applaud her assertion that, \"\...like all R.E.M. albums, peeling back
the surface revealed more questions and layers of (potential) meaning.\"
There\'s a reason I fell in love with literary criticism and it has a
lot to with trying to figure out what Michael Stipe was singing about.\
\
**Nature is a Complex System**\
Recent news out of Florida about [polluted waters flowing from the
swamps and lakes into the tourist-choked beach
areas](http://www.motherjones.com/environment/2016/02/florida-water-pollution-lake-okeechobee)
proves that we continue to expect to have what we think we need
(large-scale factory farms) and what we know we want (beautiful,
pristine beaches). We seem unable to grasp the concept that these
systems are all related. Modern medicine tends to similar thinking when
dealing with the human body. Sure, we\'re veering towards a gaian model
here and we seem to have no way to understand it.\
\
**He Writes a Great Book, Too**\
One of my favorite authors, [Alastair
Reynolds](https://twitter.com/AquilaRift), occasionally drops a few
tweets about what he\'s listening to while writing. His taste is
impeccable and is rooted in a time and place that is intriguing (20-30
years ago, England). Recently, he mentioned [New Fast Automatic
Daffodils and their album
Pigeonhole](https://open.spotify.com/album/4oNpuWnEcc6W23H4QUHd3q)
released in 1990. Ostensibly considered part of the [Madchester
scene](https://en.wikipedia.org/wiki/Madchester), New FADS has a lot
more going on than some of the other poppier bands in that scene. It
makes sense that Reynolds also turned me on to The Fall who were hugely
influential for many of the bands in the Madchester scene. It\'s worth
checking out more about [The Fall\'s leader Mark E.
Smith](http://documentaryheaven.com/the-fall-the-wonderful-and-frightening-world-of-mark-e-smith/).\
\
**The Forever Story**\
I read a lot of science fiction but I\'m not any kind of scientist. It
doesn\'t follow that I\'d be keen on the hard sci-fi of Robert Reed.
Yet, in his books,
[Marrow](https://www.goodreads.com/book/show/100208.Marrow?from_search=true&search_version=service)
and [The Well of
Stars](https://www.goodreads.com/book/show/100327.The_Well_of_Stars?from_search=true&search_version=service),
Reed builds a world that exists inside a giant world-sized spaceship and
no one knows who built it. Yeah, I\'m gonna read that. The ship houses
billions of beings from various parts of the Milky Way galaxy most of
whom are rendered nearly immortal through genetic manipulation.
Characters and stories take place over the course of thousands and
thousands of years. The books are a heady stew of physics, medicine, and
political intrigue piled up with ideas that strain the imagination.
Through it all, Reed is mostly able to build characters that are as
interesting as the ship they inhabit.\
\
**Almost as Good as the Real Thing**\
I don\'t get out to as many concerts as I used to so I find myself
regularly seeking out live albums. Bootlegs are one thing but a live
album has an intentionality about it that makes it a slightly different
form. There\'s a lot of them out there but one that I consistently
return to is [Sister Hazel\'s \"Before the
Amplifiers\"](https://open.spotify.com/album/1EHT8EoVqaKqqlXH5chR7M)
which finds the band doing an acoustic set of some of their more popular
tunes. I particularly enjoy the standout performance of [\"Your
Winter.\"](https://open.spotify.com/track/3h68W0971d6CSUID5NFWDd)\
\
**It\'s Funny and Smart and Funny**\
If you aren\'t watching [Comedy Central\'s \"Broad
City\"](http://www.cc.com/shows/broad-city) then you\'re missing out on
some ground-breaking comedy. It\'s slapstick in parts, surreal in parts,
incisive in parts, and is unafraid to be a product of its surroundings
in New York City. The two stars, Abbi Jacobson and Ilana Glazer, are
magnetic and bold while being vulnerable. It\'s an intoxicating mix that
allows them to be strong and weak and real. It says a lot that I know
nothing of the world they inhabit and experiences they have but I\'m
still able to laugh and more than anything find some of myself in their
stories. I mentioned it\'s fall off the couch funny, right?\
\
**The Tidal Wave**\
The legalization of recreational marijuana in the US and the nearly
total decriminalization in Europe are creating a tidal wave of backlash
against not just the ban on marijuana but the entire war on drugs. [Kofi
Annan deftly elucidates many of the arguments for the regulation and
legalization of many recreational
drugs](http://www.spiegel.de/international/world/kofi-annan-on-why-drug-bans-are-ineffective-a-1078402.html).
It\'s clear that when a diplomat if his stature calls out what seems
obvious to many of us that there\'s a pressure building. I was
especially struck by his pointing out the impact on Africa as a transit
station for drugs from South America through to Europe. That continent
certainly needs no more encouragement towards reinforcing the disparity
between rich and poor that a drug trade typically takes advantage of.\
\
**Miles and Miles Above Us in Space**\
As you might imagine, someone who loves music as much as I do is surely
a satellite radio subscriber. Indeed I am, and I believe the finest show
on the whole thing right now is [Mike
Marrone](http://www.mikemarrone.com)\'s \"Wake Up Sets\" which airs on
The Loft (Ch. 30) from 6-9am EST every weekday. This morning he blew me
away with a set went like this:

-   [Jackson Browne - \"Late for the
    Sky\"](https://open.spotify.com/track/2WCoRl4kKbaZ6oGN6KD4HQ)

-   [Loggins & Messina - \"Pathway to
    Glory\"](https://open.spotify.com/track/5j9VPhydiWL60b5C0ffbzY)

-   [David Bromberg - \"Get Up and Go/Fiddle
    Tunes\"](https://open.spotify.com/track/3cCaEMGddDvSNg59hp1tO8)

-   [The Byrds - \"Drug Store Truck Drivin\'
    Man\"](https://open.spotify.com/track/5PkkrqACWO9yK0h2LrDwqg)

-   [New Riders of the Purple Sage - \"Important Exportin
    Man\"](https://open.spotify.com/track/5HCAcyPoRa0oXpbMVroH67)

-   [Field Music - Don\'t You Want to Know What\'s
    Wrong?](https://open.spotify.com/track/0VqiSdHYskJnYugDxzr9sL)

Clearly, Marrone is a man who knows what he\'s doing. His taste hits me
right where it feels good. If you\'re a Sirius subscriber, check out his
show. His website has [all of his
playlists](https://drive.google.com/folderview?id=0B7Mlh0jipIeiWTRQdU5ZRk52WkE&usp=drive_web)
that you can peruse. There is some majesty in there.\
**Beauty Reigns**\
Arthur Russell was a cellist, among other pursuits, whose recordings and
performances veer from avant garde to disco to blues to classical Indian
music. [This blog post gives some context for his
work](https://blogthehum.wordpress.com/2016/02/26/62-minutes-32-seconds-of-archival-arthur-russell-performance-footage-by-phill-niblock/)
and has several experimental films of a performance of his. I recommend
checking out [the only album of his work released before he died titled
\"World of
Echo.](https://open.spotify.com/album/7qTGji0zymCBgWDSukQq1J)\" I played
this for my coworker, Parth, who is from India. He didn\'t know anything
about what it was before he said, \"This sounds like the music my
parents used to listen to.\" It\'s clear that Russell\'s influences and
training were genuine and used to great effect.\
\
**ICYMI**\
Squeeze released a [new album late last year titled Cradle to the
Grave](https://open.spotify.com/album/3070asD6o4DKm5O9xgK01s). It\'s not
the full original lineup but it\'s chock full of smart song writing that
is wry and knowing. Those delicious harmonies and hooks are still there
too.\
\
Thanks for checking out this week\'s \"I miss your smell.\" Reply to let
me know what you think and please forward to your friends who need a
dash of excitement in their inbox.
